
// QB Trickle Game
function QB_Trickle_Game(base, id) {
    this.base = base;
    this.score = 0;
    this.gid = id;
    this.allArea = "game-all-" + id;
    this.allCss = "game-all";
    this.topArea = "game-top-" +id;
    this.topCss = "game-top";
    this.rankArea = "game-rank-" + id;
    this.rankCss = "game-rank";
    this.myrankArea = "game-myrank-" + id;
    this.myrankCss = "game-myrank";
    this.topTitleArea = "game-top-title-" + id;
    this.topTitleCss = "game-top-title";
    this.topScoreArea = "game-top-score-" + id;
    this.topScoreCss = "game-top-score";
    this.topTimeArea = "game-top-time-" + id;
    this.topTimeCss = "game-top-time";
    this.mainArea = "game-main-" +id;
    this.mainCss = "game-main";
    this.progressArea = "game-progress-" + id;
    this.progressCss = "game-progress";
    this.svg;
    this.progressSvg;


    this.color = ["red", "green", "yellow"];
    this.time = 0;

    this.currentTime;


    // defense game
    this.inputTextArea;
    this.inputText;
    this.inputTextGroup;
    this.koreaFormula = ["일","십", "백", "천", "만", "억", "조"];
    this.arabFormula = [1, 10, 100, 1000, 10000, 100000000, 1000000000000];
    this.displayAnim;
    this.addAnim;
    this.endGame = false;
    this.answerMap = [];
    for(var i=0;i<5;i++){
        this.answerMap[i] = 0;
    }
    this.boundaryWidth = [150, 150, 153, 156];
    this.boundaryLine = [];
    this.life = 3;
    this.probSpeed = -0.4;
    this.probRectWidth = 80;
    this.currentCursor = 0;
    this.skillCount = 2;
}

QB_Trickle_Game.prototype = {
    constructor : QB_Trickle_Game,
    appendDiv : function (selector, idPath, cssPath) {
        $(selector).append("<div id='" + idPath + "' class='" + cssPath + "'>");
    },
    insertText : function (str, index, value) {
        return str.substr(0, index) + value + str.substr(index);
    },
    createAllArea : function () {
        var allSelector = "#" + this.allArea;

        $(allSelector).remove();

        // add game area
        this.appendDiv(this.base, this.allArea, this.allCss);
    },
    createTopArea : function () {
        var allSelector = "#" + this.allArea;
        var topSelector = "#" + this.topArea;
        var titleSelector = "#" + this.topTitleArea;
        var scoreSelector = "#" + this.topScoreArea;
        var timeSelector = "#" + this.topTimeArea;
        var rankSelector = "#" + this.rankArea;
        var myrankSelector = "#" + this.myrankArea;

        // add top area
        this.appendDiv(allSelector, this.topArea, this.topCss);
        this.appendDiv(topSelector, this.topTitleArea, this.topTitleCss);
        var text;

        text = "테스트입니다.<br>테스트 게임.";
        $(titleSelector).append("<img src='image/key.png'>");
        // $(titleSelector).append(text);

        this.appendDiv(topSelector, this.topScoreArea, this.topScoreCss);
        $(scoreSelector).append(this.score + "점");
        this.appendDiv(topSelector, this.topTimeArea, this.topTimeCss);
        $(timeSelector).append(this.time.toFixed(2) + "sec");

        this.appendDiv(allSelector, this.rankArea, this.rankCss);
        this.appendDiv(allSelector, this.myrankArea, this.myrankCss);

        // test
        $.ajax({
            type: 'GET',
            url: 'http://dev-api.quebon.tv/user/v1/games/defense',
            headers: {
                Authorization: 'Bearer ' + api_token
            },
            data: {
               count: 5
            }
        }).done(function(data) {
            console.log(data);
            var rank = data.ranking;
            var text = "";

            for(var i=0;i<rank.length;i++){
                text += (i+1) + "위 --> 닉네임 : " + rank[i].user.nickname + ", 점수 : " + rank[i].score + ", 시간 : " + rank[i].time + "<br>";
            }

            $(rankSelector).append(text);

            text = "";
            text +=  data.my.rank + "위 --> 닉네임 : " + data.my.user.nickname +  ", 점수 : " + data.my.score + ", 시간 : " + data.my.time;
            $(myrankSelector).append(text);
        });
    },
    shuffle : function(a) {
        var j, x, i;
        for (i = a.length; i; i -= 1) {
            j = Math.floor(Math.random() * i);
            x = a[i - 1];
            a[i - 1] = a[j];
            a[j] = x;
        }
    },
    checkCorrectFormula : function (input) {
        var count = 0;
        var before = "";
        for(var i=0;i<input.length;i++){
            if(input[i] === "십" || input[i] === "백" || input[i] === "천"){
                if(count > 1){
                    return false;
                }
                if(count === 1 && before ==="1"){
                    return false;
                }
                count = 0;
            }
            else if(input[i] === "억" || input[i] === "만" || input[i] === "조"){
                count = 0;
            }
            else{
                count++;
                before = input[i];
            }
        }

        return true;
    },
    addCallback : function (ms) {
        // console.log(ms);
        var selector = "#" + this.mainArea;
        var colorVal = Math.floor(Math.random() * (this.color.length));
        var component = this.svg.children();

        this.addFormula();

        var call = this.addCallback.bind(this);
        if(this.endGame === false){
            var num = Math.floor(Math.random() * 5) + 5;
            this.addAnim = setTimeout(call, 1000 * num);
        }
    },
    updateScore : function () {
        var scoreSelector = "#" + this.topScoreArea;

        $(scoreSelector).html(this.score + "점");

        if(this.score > 30){
            this.probSpeed = -0.5;
            this.probRectWidth = 100;
        }
        if(this.score > 90){
            this.probSpeed = -0.6;
        }
        if(this.score > 150){
            this.probSpeed = -0.7;
        }
        if(this.score > 180){
            this.probSpeed = -0.8;
        }
        if(this.score > 210){
            this.probSpeed = -0.9;
        }
        if(this.score > 240){
            this.probSpeed = -1.0;
        }
    },
    checkEnd : function () {
        if(this.life === 0){
            var titleSelector = "#" + this.topTitleArea;

            var text = "방벽이 무너졌습니다.<br>게임오버입니다.";

            $(titleSelector).html(text);

            var correctAudio = new Audio('fail.mp3');
            correctAudio.play();

            this.stopGame();
        }
    },
    updateFrame : function () {
        var component = this.svg.children();
        var selector = "#" + this.mainArea;
        var speed = this.probSpeed;
        var ansSpeed = 5.0;
        var prob = [];
        var ans = [];
        var width = this.probRectWidth / 2;
        var lineNumber = [0, 1, 2, 3, 4];
        var height = 100;
        var deleteGroup = [];
        var skillLine = 0;


        prob.length = 0;
        ans.length = 0;
        deleteGroup.length = 0;



        for(var i=2;i<component.length;i++){
            if(component[i].data('type') === "problem" && component[i].data('isChecked') === false){
                component[i].dmove(speed, 0);
                prob.push(component[i]);
            }

            if(component[i].data('type') === "answer" && component[i].data('isChecked') === false){
                component[i].dmove(ansSpeed, 0);
                ans.push(component[i]);
            }

            if(component[i].data('type') === "skill"){
                component[i].dmove(ansSpeed, 0);
                skillLine = component[i];
            }

            if(component[i].data('remove') === true){
                deleteGroup.push(component[i]);
            }
        }

        if(skillLine !== 0){
            for(var i=prob.length-1;i>=0;i--){
                var probWidth = prob[i].data('boundaryWidth') / 2;
                if(prob[i].cx() - probWidth <= skillLine.cx() && prob[i].data('isChecked') === false){
                    prob[i].data('isChecked', true);

                    if(prob[i].data('level') === 1){
                        this.score += 3;
                    }
                    else if(prob[i].data('level') === 2){
                        this.score += 7;
                    }
                    else if(prob[i].data('level') === 3){
                        this.score += 10;
                    }

                    prob[i].animate(300).fill({opacity: 0.0}).after(function () {
                        this.data('remove', true);
                    });

                    // prob[i].remove();
                }
            }

            this.updateScore.call(this);

            if(skillLine.cx() >= $(selector).width()){
                skillLine.remove();
            }
        }

        for(var i=ans.length-1;i>=0;i--){
            var ch = ans[i].children();
            var txt = ch[1];

            // // check correct
            var checkCorrect = this.checkCorrectFormula.call(this, txt.text());

            for(var j=prob.length-1;j>=0;j--){
                var probWidth = prob[j].data('boundaryWidth') / 2;
                if(ans[i].cx() + width >= prob[j].cx() - probWidth && ans[i].cy() === prob[j].cy()){
                    if(checkCorrect && ans[i].data('answer') === prob[j].data('answer')){
                        // 정답

                        if(prob[j].data('level') !== 2){
                            if(prob[j].data('level') === 1){
                                this.score += 3;
                            }
                            else{
                                this.score += 10;
                            }

                            for(var k=0;k<lineNumber.length;k++){
                                if(prob[j].cy() === lineNumber * height){
                                    this.answerMap[lineNumber[k]]--;
                                    break;
                                }
                            }

                            prob[j].data('isChecked', true);
                            ans[i].data('isChecked', true);

                            prob[j].animate(300).fill({opacity: 0.0}).after(function () {
                                this.data('remove', true);
                            });

                            ans[i].animate(300).fill({opacity: 0.0}).after(function () {
                                this.data('remove', true);
                            });

                            // prob[j].remove();
                            // ans[i].remove();

                            this.updateScore.call(this);
                        }
                        else if(prob[j].data('level') === 2){
                            if(prob[j].data('isDead') === true){
                                this.score += 7;
                                // prob[j].remove();

                                for(var k=0;k<lineNumber.length;k++){
                                    if(prob[j].cy() === lineNumber * height){
                                        this.answerMap[lineNumber[k]]--;
                                        break;
                                    }
                                }

                                prob[j].data('isChecked', true);

                                prob[j].animate(300).fill({opacity: 0.0}).after(function () {
                                    this.data('remove', true);
                                });

                                this.updateScore.call(this);
                            }
                            else{
                                prob[j].data('isDead', true);
                                var child = prob[j].children();

                                var rect = child[0];
                                rect.stroke({color: 'pink', width: 1});
                                var text = child[1];

                                var result = this.getFormulaText.call(this, 2);

                                var output = result[0] + " × " + result[1];

                                var first = this.convertKoreaToArabFormula(result[0]);
                                var second = this.convertKoreaToArabFormula(result[1]);
                                var answer = first * second;

                                text.text(output);

                                prob[j].data('answer', answer);
                            }


                            ans[i].data('isChecked', true);

                            ans[i].animate(300).fill({opacity: 0.0}).after(function () {
                                this.data('remove', true);
                            });
                            // ans[i].remove();
                        }
                    }
                    else{
                        // 오답
                        ans[i].data('isChecked', true);
                        ans[i].animate(300).fill({opacity: 0.0}).after(function () {
                            this.data('remove', true);
                        });
                        // ans[i].remove();
                    }
                }
            }

            if(ans[i].cx() + width >= $(selector).width()){
                ans[i].remove();
            }
        }

        // 경계 체크
        for(var i=prob.length-1;i>=0;i--){
            var probWidth = prob[i].data('boundaryWidth') / 2;
            if(prob[i].cx() - probWidth <= this.boundaryWidth[this.life]){
                // game over
                this.checkEnd.call(this);


                if(this.life > 0){
                    prob[i].remove();
                    this.boundaryLine[this.life-1].remove();
                }

                this.life--;
            }
        }

        // delete components
        for(var i=deleteGroup.length-1;i>=0;i--){
            deleteGroup[i].remove();
        }
    },
    displayCallback : function (ms) {
        // console.log(ms);
        var timeSelector = "#" + this.topTimeArea;

        // this.updateCircle();
        this.updateFrame();

        if(this.time === 0){
            this.time = new Date().getTime();
        }

        var endTime = new Date().getTime();
        // console.log(ms);
        var currentTime = (endTime - this.time)/1000;

        $(timeSelector).html(currentTime.toFixed(2) + "sec");
        this.currentTime = currentTime;

        // setTimeout(this.callback(), 1000);
        // console.log(this);
        var call = this.displayCallback.bind(this);
        if(this.endGame === false)
            this.displayAnim = requestAnimationFrame(call);
    },
    getFormulaText : function (level) {
        var fNum = Math.floor(Math.random() * 9) + 1;
        var sNum = Math.floor(Math.random() * 9) + 1;
        var fKor;
        var sKor;
        var sKorArray = [];

        var level1Kor = ["일","십", "백", "천", "만", "억"];
        var level2Kor = ["일", "십", "백", "천", "만", "억", "십만", "백만", "천만", "십억", "백억", "천억"];


        sKorArray.length = 0;

        var fText = "", sText = "", tText = "";

        // step 1
        if(this.score <= 30){
            fKor = level1Kor[Math.floor(Math.random() * level1Kor.length)];

            for(var i=0;i<level1Kor.length;i++){
                sKorArray[i] = level1Kor[i];
            }
            this.shuffle(sKorArray);

            for(var i=0;i<level1Kor.length;i++){
                var fFormula, sFormula;
                if(fKor === "일"){
                    fFormula = 1;
                }
                else{
                    fFormula = this.convertKoreaToArabFormula(fKor);
                }

                if(sKorArray[i] === "일"){
                    sFormula = 1;
                }
                else{
                    sFormula = this.convertKoreaToArabFormula(sKorArray[i]);
                }

                if(fFormula * sFormula <= this.arabFormula[6]){
                    sKor = sKorArray[i];
                    break;
                }
            }
        }
        // step 2
        else{
            fKor = level2Kor[Math.floor(Math.random() * level2Kor.length)];

            for(var i=0;i<level2Kor.length;i++){
                sKorArray[i] = level2Kor[i];
            }
            this.shuffle(sKorArray);

            for(var i=0;i<level2Kor.length;i++){
                var fFormula, sFormula;
                if(fKor === "일"){
                    fFormula = 1;
                }
                else{
                    fFormula = this.convertKoreaToArabFormula(fKor);
                }

                if(sKorArray[i] === "일"){
                    sFormula = 1;
                }
                else{
                    sFormula = this.convertKoreaToArabFormula(sKorArray[i]);
                }

                if(fFormula * sFormula <= this.arabFormula[6]){
                    sKor = sKorArray[i];
                    break;
                }
            }
        }

        if(fKor === "일"){
            if(fNum === 1){
                fNum += Math.floor(Math.random()*8)+1
            }
            fText = fNum + "";
        }
        else{
            if(fNum !== 1){
                fText = fNum + fKor;
            }
            else{
                fText = fKor;
            }
        }

        if(sKor === "일"){
            if(sNum === 1){
                sNum += Math.floor(Math.random()*8)+1
            }
            sText = sNum + "";
        }
        else{
            if(sNum !== 1){
                sText = sNum + sKor;
            }
            else{
                sText = sKor;
            }
        }

        if(level === 3){
            if(sKor === "일"){
                var kor = ["십", "백"];

                var tKor = kor[Math.floor(Math.random() * kor.length)];

                tText = tKor;
            }
            else{
                sText = sKor;

                var kor = ["일","십", "백"];

                var tKor = kor[Math.floor(Math.random() * kor.length)];
                var tNum =  Math.floor(Math.random() * 9) + 1;

                if(tKor === "일"){
                    if(tNum === 1){
                        tNum += Math.floor(Math.random()*8)+1
                    }
                    tText = tNum + "";
                }
                else{
                    if(tNum !== 1){
                        tText = tNum + tKor;
                    }
                    else{
                        tText = tKor;
                    }
                }
            }

        }

        if(level !== 3){
            return [fText, sText];
        }
        else{
            return [fText, sText, tText];
        }
    },
    addFormula : function () {
        var svg = this.svg;
        var selector = "#" + this.mainArea;
        var lineNum = Math.floor(Math.random() * 5);
        var sKorArray = [];
        var height = 100;
        var boundaryWidth = this.probRectWidth;
        var lineNumber = [0, 1, 2, 3, 4];


        var bugType = [];
        var output;
        var answer;
        var first;
        var second;
        var third;

        bugType.length = 0;
        sKorArray.length = 0;
        var fText = "", sText = "", tText = "";

        bugType.push(1);


        var component = svg.children();
        if(this.score > 60){
            var bugCount = 0;
            for(var i=0;i<component.length;i++){
                if(component[i].data('level') === 2){
                    bugCount++;
                }
            }
            if(bugCount < 2){
                bugType.push(2);
            }
        }
        if(this.score > 120){
            var bugCount = 0;
            for(var i=0;i<component.length;i++){
                if(component[i].data('level') === 3){
                    bugCount++;
                }
            }
            if(bugCount < 2) {
                bugType.push(3);
            }
        }

        var level = bugType[Math.floor(Math.random() * bugType.length)];

        var result = this.getFormulaText.call(this, level);

        this.shuffle(result);

        fText = result[0];
        sText = result[1];
        if(level === 3){
            tText = result[2];

            output = fText + " × " + sText + " × " + tText;

            first = this.convertKoreaToArabFormula(fText);
            second = this.convertKoreaToArabFormula(sText);
            third = this.convertKoreaToArabFormula(tText);

            answer = first * second * third;
        }
        else{
            output = fText + " × " + sText;

            first = this.convertKoreaToArabFormula(fText);
            second = this.convertKoreaToArabFormula(sText);

            answer = first * second;
        }

        var count = 0;
        this.shuffle(lineNumber);
        var isOk = false;
        while(true){
            for(var i=0;i<lineNumber.length;i++){
                if(this.answerMap[lineNumber[i]] === count){
                    lineNum = lineNumber[i];
                    this.answerMap[lineNumber[i]]++;
                    isOk = true;
                    break;
                }
            }
            count++;
            if(isOk === true){
                break;
            }
        }

        // add svg
        var group = svg.group();
        var text;
        var rect;
        if(level === 1){
            text = svg.text(output).fill("red").font({family: 'Helvetica', size: 15, anchor: 'middle', leading: '1.5em'}).move(boundaryWidth/2, 40);
            rect = svg.rect(boundaryWidth, 50).fill({color: "white", opacity: 1.0}).stroke({color: 'pink', width: 1}).move(0, 25);
        }
        else if(level ===2){
            text = svg.text(output).fill("red").font({family: 'Helvetica', size: 15, anchor: 'middle', leading: '1.5em'}).move(boundaryWidth/2, 40);
            rect = svg.rect(boundaryWidth, 50).fill({color: "white", opacity: 1.0}).stroke({color: 'blue', width: 1}).move(0, 25);
        }
        else if(level ===3){
            boundaryWidth += 30;
            text = svg.text(output).fill("red").font({family: 'Helvetica', size: 15, anchor: 'middle', leading: '1.5em'}).move(boundaryWidth/2, 40);
            rect = svg.rect(boundaryWidth, 50).fill({color: "white", opacity: 1.0}).stroke({color: 'green', width: 1}).move(0, 25);
        }

        group.add(rect);
        group.add(text);
        group.data('answer', answer);
        group.data('type', "problem");
        group.data('level', level);
        if(level !== 2){
            group.data('isDead', true);
        }
        else{
            group.data('isDead', false);
        }
        group.data('boundaryWidth', boundaryWidth);
        group.data('isChecked', false);
        group.data('remove', false);

        // console.log(group.children());


        group.move($(selector).width(), height * lineNum);
    },
    createInputArea : function () {
        var svg = this.svg;
        var selector = "#" + this.mainArea;
        var boundaryWidth = 150;

        this.boundaryLine.push(svg.line(boundaryWidth, 0, boundaryWidth, $(selector).height()).stroke({color: "blue", width: 2}));
        this.boundaryLine.push(svg.line(boundaryWidth+3, 0, boundaryWidth+3, $(selector).height()).stroke({color: "blue", width: 2}));
        this.boundaryLine.push(svg.line(boundaryWidth+6, 0, boundaryWidth+6, $(selector).height()).stroke({color: "blue", width: 2}));

        this.inputTextGroup = svg.group();

        this.inputText = "정답을 입력해주세요";
        this.inputTextArea = svg.text(this.inputText).fill("blue").font({family: 'Helvetica', size: 15, anchor: 'middle', leading: '1.5em'}).move(boundaryWidth/2, 15);
        var inputRect = svg.rect(boundaryWidth-10, 50).fill({color: 'white', opacity: 1.0}).stroke({color: 'pink', width: 1}).move(5, 0);

        this.inputTextGroup.add(inputRect);
        this.inputTextGroup.add(this.inputTextArea);

        this.inputTextGroup.move(0, 25);
    },
    createGameArea : function () {
        var svg = this.svg;
        var selector = "#" + this.mainArea;
        var boundaryWidth = 150;

        var lineCount = 5;
        var height = $(selector).height() / lineCount;

        for(var i=1;i<lineCount;i++){
            svg.line(boundaryWidth, height * i, $(selector).width(), height * i).stroke({color: "blue", width: 2});
        }
    },
    addAnswerMissile : function (txt) {
        var svg = this.svg;
        var component = svg.children();
        var textArea = this.inputTextArea;
        var answer = this.convertKoreaToArabFormula(txt);
        var boundaryWidth = this.probRectWidth;



        var group = svg.group();
        var text = svg.text(txt + "").fill("red").font({family: 'Helvetica', size: 15, anchor: 'middle', leading: '1.5em'}).move(boundaryWidth/2, 40);
        var rect = svg.rect(boundaryWidth, 50).fill({opacity: 0.0}).stroke({color: 'pink', width: 1}).move(0, 25);

        group.add(rect);
        group.add(text);
        group.data('answer', answer);
        group.data('type', "answer");
        group.data('isChecked', false);
        group.data('remove', false);

        group.move(boundaryWidth, this.inputTextGroup.cy() - 50);
    },
    addSkillMissile : function () {
        var svg = this.svg;
        var selector = "#" + this.mainArea;
        var boundaryWidth = 150;

        var group = svg.group();
        var line = svg.line(boundaryWidth, 0, boundaryWidth, $(selector).height()).stroke({color: "red", width: 4});

        group.add(line);
        group.data('type', 'skill');
    },
    convertKoreaToArabFormula : function (k_formula) {
        var start = 0;
        var output = 0;
        var tempOutput = 0;

        for(var i=0;i<k_formula.length;i++){
            if(k_formula[i] === "십" || k_formula[i] === "백" || k_formula[i] === "천" || k_formula[i] === "만" || k_formula[i] === "억" || k_formula[i] === "조"){
                var subNum;
                if(start !== i){
                    subNum = Number(k_formula.substring(start, i));
                }
                else{
                    subNum = 1;
                }


                if(k_formula[i] === "십" || k_formula[i] === "백" || k_formula[i] === "천"){
                    for(var j=0;j<this.koreaFormula.length;j++){
                        if(this.koreaFormula[j] === k_formula[i]){
                            subNum *= this.arabFormula[j];
                            break;
                        }
                    }
                    tempOutput += subNum;
                }
                else{
                    for(var j=0;j<this.koreaFormula.length;j++){
                        if(this.koreaFormula[j] === k_formula[i]){
                            subNum *= this.arabFormula[j];
                            tempOutput *= this.arabFormula[j];
                            break;
                        }
                    }

                    // tempOutput *= subNum;
                    output += tempOutput;
                    if(tempOutput === 0 || start !== i){
                        output += subNum;
                    }

                    tempOutput = 0;
                }

                start = i + 1;
            }

            if(i === (k_formula.length - 1)){
                var subNum;

                subNum = Number(k_formula.substring(start, i+1));

                output += subNum;

                if(tempOutput !== 0){
                    output += tempOutput;
                }
            }
        }

        return output;
    },
    createMainArea : function () {
        var allSelector = "#" + this.allArea;
        var mainSelector = "#" + this.mainArea;

        // add top area
        this.appendDiv(allSelector, this.mainArea, this.mainCss);

        this.svg = SVG(this.mainArea);

        this.createInputArea.call(this);
        this.createGameArea.call(this);

        var group = this.inputTextGroup;
        var text =  this.inputText;
        var textArea = this.inputTextArea;
        var svg = this.svg;
        var lineCount = 5;
        var convertKtoA = this.convertKoreaToArabFormula.bind(this);
        var addMissile = this.addAnswerMissile.bind(this);
        var cursor = this.currentCursor;
        var insert = this.insertText.bind(this);
        var skill = this.skillCount;
        var addSkill = this.addSkillMissile.bind(this);

        $("body").keydown(function (d) {
            d.preventDefault();
            var height = $(mainSelector).height() / lineCount;
            if(d.originalEvent.key === "ArrowUp"){
                if(group.cy() - height > 0){
                    group.dmove(0, -height);
                }
            }
            else if(d.originalEvent.key === "ArrowDown"){
                if(group.cy() + height < $(mainSelector).height()){
                    group.dmove(0, height);
                }
            }
            else if(d.originalEvent.key === "ArrowLeft"){
                if(text !== "정답을 입력해주세요" && cursor > 0){
                    text = text.replace("_", "");
                    cursor--;
                    text = insert(text, cursor, "_");
                    textArea.text(text);
                }
            }
            else if(d.originalEvent.key === "ArrowRight"){
                if(text !== "정답을 입력해주세요" && cursor < text.length-1){
                    text = text.replace("_", "");
                    cursor++;
                    text = insert(text, cursor, "_");
                    textArea.text(text);
                }
            }
            else if(d.originalEvent.key === "Backspace"){
                if(text !== "정답을 입력해주세요"){
                    text = text.replace("_", "");
                    text = text.substring(0, cursor-1) + text.substring(cursor, text.length);

                    if(cursor>0){
                        cursor--;
                    }
                    // text = text.substring(0, text.length-1);

                    if(text.length === 0){
                        text = "정답을 입력해주세요";
                    }
                    else{
                        text = insert(text, cursor, "_");
                    }
                }

                textArea.text(text);
            }
            else if(d.originalEvent.key === "0" || d.originalEvent.key === "1" || d.originalEvent.key === "2"
            || d.originalEvent.key === "3" || d.originalEvent.key === "4" || d.originalEvent.key === "5"
            || d.originalEvent.key === "6" || d.originalEvent.key === "7" || d.originalEvent.key === "8"
            || d.originalEvent.key === "9"){
                if(text === "정답을 입력해주세요"){
                    text = d.originalEvent.key;
                    text += "_";
                    cursor = 1;
                }
                else{
                    text = text.replace("_", "");
                    text = insert(text, cursor, d.originalEvent.key);
                    cursor++;
                    text = insert(text, cursor, "_");
                    // text += d.originalEvent.key;
                }

                textArea.text(text);
            }
            else if( d.originalEvent.key === "q" || d.originalEvent.key === "Q"){
                if(text === "정답을 입력해주세요"){
                    text = "십";
                    text += "_";
                    cursor = 1;
                }
                else{
                    text = text.replace("_", "");
                    text = insert(text, cursor, "십");
                    cursor++;
                    text = insert(text, cursor, "_");
                    // text += "십";
                }

                textArea.text(text);
            }
            else if(d.originalEvent.key === "w" || d.originalEvent.key === "W"){
                if(text === "정답을 입력해주세요"){
                    text = "백";
                    text += "_";
                    cursor = 1;
                }
                else{
                    text = text.replace("_", "");
                    text = insert(text, cursor, "백");
                    cursor++;
                    text = insert(text, cursor, "_");
                    // text += "백";
                }

                textArea.text(text);
            }
            else if(d.originalEvent.key === "e" || d.originalEvent.key === "E"){
                if(text === "정답을 입력해주세요"){
                    text = "천";
                    text += "_";
                    cursor = 1;
                }
                else{
                    text = text.replace("_", "");
                    text = insert(text, cursor, "천");
                    cursor++;
                    text = insert(text, cursor, "_");
                    // text += "천";
                }

                textArea.text(text);
            }
            else if(d.originalEvent.key === "a" || d.originalEvent.key === "A"){
                if(text === "정답을 입력해주세요"){
                    text = "만";
                    text += "_";
                    cursor = 1;
                }
                else{
                    text = text.replace("_", "");
                    text = insert(text, cursor, "만");
                    cursor++;
                    text = insert(text, cursor, "_");
                    // text += "만";
                }

                textArea.text(text);
            }
            else if(d.originalEvent.key === "s" || d.originalEvent.key === "S"){
                if(text === "정답을 입력해주세요"){
                    text = "억";
                    text += "_";
                    cursor = 1;
                }
                else{
                    text = text.replace("_", "");
                    text = insert(text, cursor, "억");
                    cursor++;
                    text = insert(text, cursor, "_");
                    // text += "억";
                }

                textArea.text(text);
            }
            else if(d.originalEvent.key === "d" || d.originalEvent.key === "D"){
                if(text === "정답을 입력해주세요"){
                    text = "조";
                    text += "_";
                    cursor = 1;
                }
                else{
                    text = text.replace("_", "");
                    text = insert(text, cursor, "조");
                    cursor++;
                    text = insert(text, cursor, "_");
                    // text += "조";
                }

                textArea.text(text);
            }
            else if(d.originalEvent.key === "r" || d.originalEvent.key === "R"){
                text = "정답을 입력해주세요";

                cursor = 0;

                textArea.text(text);
            }
            else if(d.originalEvent.key === "f" || d.originalEvent.key === "F"){
                if(skill > 0){

                    skill--;
                    addSkill();
                }
            }
            else if(d.originalEvent.key ==="Enter"){
                if(text !== "정답을 입력해주세요"){
                    text = text.replace("_", "");
                    addMissile(text);

                    var output = convertKtoA(text);
                    console.log(output);

                    text = "정답을 입력해주세요";
                    textArea.text(text);
                }
            }
        });

        this.addFormula.call(this);
        this.displayCallback.call(this);
        var call = this.addCallback.bind(this);
        this.addAnim = setTimeout(call, 3000);
    },
    createBotArea : function () {
        var allSelector = "#" + this.allArea;
        var progressSelector = "#" + this.progressArea;

        this.appendDiv(allSelector, this.progressArea, this.progressCss);

        this.progressSvg = SVG(this.progressArea);

        this.progressSvg.rect($(progressSelector).width(), $(progressSelector).height()).fill("red");
    },
    drawGame : function () {
        this.createAllArea();
        this.createTopArea();
        this.createMainArea();
        // this.createBotArea();
    },
    stopGame : function () {
        // var selector = "#" + this.allArea;
        this.endGame = true;
        var mainSelector = "#" + this.mainArea;

        cancelAnimationFrame(this.displayAnim);
        // clearInterval(this.addAnim);
        clearTimeout(this.addAnim);
        // clearTimeout(this.checkCountCallback);
        $(mainSelector).css('pointer-events', 'none');
        // $(selector).remove();
        $("body").unbind();

        gameStop();
    }
};