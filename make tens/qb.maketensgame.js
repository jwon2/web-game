
// QB Trickle Game
function QB_Trickle_Game(type, fill, level, base, id) {
    this.type = type;
    base = typeof base !== 'undefined' ? base : "body";
    this.base = base;
    this.level = level;
    this.defaultLevel = level;
    this.fill = fill;
    this.score = 0;
    this.gid = id;
    this.allArea = "game-all-" + id;
    this.allCss = "game-all";
    this.topArea = "game-top-" +id;
    this.topCss = "game-top";
    this.rankArea = "game-rank-" + id;
    this.rankCss = "game-rank";
    this.myrankArea = "game-myrank-" + id;
    this.myrankCss = "game-myrank";
    this.topTitleArea = "game-top-title-" + id;
    this.topTitleCss = "game-top-title";
    this.topScoreArea = "game-top-score-" + id;
    this.topScoreCss = "game-top-score";
    this.topTimeArea = "game-top-time-" + id;
    this.topTimeCss = "game-top-time";
    this.mainArea = "game-main-" +id;
    this.mainCss = "game-main";
    this.progressArea = "game-progress-" + id;
    this.progressCss = "game-progress";
    this.checkCountCallback = false;
    this.svg;
    this.progressSvg;
    this.circleAnim;
    this.addAnim;
    this.color = ["B", "G", "R"];
    this.maxCount = 27;
    this.time = 0;
    this.endGame = false;
    this.answerCount = fill;
    this.counter;
    this.isCheckAnswer = false;
    this.addable = true;
    this.currentTime;
    this.ansZoneCount;
    if(this.fill > 0){
        this.ansZoneCount = 2;
    }
    else{
        this.ansZoneCount = 1;
    }
    this.ans = [];
    this.ansColor = [];
    for(var i=0;i<this.ansZoneCount;i++){
        this.ans[i] = [];
        this.ansColor[i] = [];
    }
    this.currentAnsZone = 0;
    this.ansZonePosX = [];
    this.ansZonePosY = [];
    this.ansZoneMargin = 5;
    this.ansZonePadding = 5;
    this.answerMap = [];
    for(var i=0;i<30;i++){
        if(i%10 === 0){
            this.answerMap[i] = true;
        }
        else{
            this.answerMap[i] = false;
        }
    }
    this.beforeAns = 0;
    this.beforeId = [];

    // parameter
    this.maxAnswer;
    if(type === 'two'){
        this.maxAnswer = 2;
    }
    else if(type === 'three'){
        this.maxAnswer = 3;
    }
    this.radius = 28;
}

QB_Trickle_Game.prototype = {
    constructor : QB_Trickle_Game,
    appendDiv : function (selector, idPath, cssPath) {
        $(selector).append("<div id='" + idPath + "' class='" + cssPath + "'>");
    },
    createAllArea : function () {
        var allSelector = "#" + this.allArea;

        $(allSelector).remove();

        // add game area
        this.appendDiv(this.base, this.allArea, this.allCss);
    },
    createTopArea : function () {
        var allSelector = "#" + this.allArea;
        var topSelector = "#" + this.topArea;
        var titleSelector = "#" + this.topTitleArea;
        var scoreSelector = "#" + this.topScoreArea;
        var timeSelector = "#" + this.topTimeArea;
        var rankSelector = "#" + this.rankArea;
        var myrankSelector = "#" + this.myrankArea;

        // add top area
        this.appendDiv(allSelector, this.topArea, this.topCss);
        this.appendDiv(topSelector, this.topTitleArea, this.topTitleCss);
        var text;
        if(this.type === "two"){
            text = "두 개의 숫자 합의 일의 자리가 0이 되게 선택하세요.<br>개수가 30개 되면 게임오버입니다.";
            $(titleSelector).append(text);
        }
        else if(this.type === "three"){
            text = "세 개의 숫자 합의 일의 자리가 0이 되게 선택하세요.<br>개수가 30개 되면 게임오버입니다.";
            $(titleSelector).append(text);
        }

        this.appendDiv(topSelector, this.topScoreArea, this.topScoreCss);
        $(scoreSelector).append(this.score + "점");
        this.appendDiv(topSelector, this.topTimeArea, this.topTimeCss);
        $(timeSelector).append(this.time.toFixed(2) + "sec");

        this.appendDiv(allSelector, this.rankArea, this.rankCss);
        this.appendDiv(allSelector, this.myrankArea, this.myrankCss);

        $.ajax({
            type: 'GET',
            url: 'http://dev-api.quebon.tv/user/v1/games/makeTens' + this.type + this.fill,
            headers: {
                Authorization: 'Bearer ' + api_token
            },
            data: {
               count: 5
            }
        }).done(function(data) {
            console.log(data);
            var rank = data.ranking;
            var text = "";

            for(var i=0;i<rank.length;i++){
                text += (i+1) + "위 --> 닉네임 : " + rank[i].user.nickname + ", 점수 : " + rank[i].score + ", 시간 : " + rank[i].time + "<br>";
            }

            $(rankSelector).append(text);

            text = "";
            text +=  data.my.rank + "위 --> 닉네임 : " + data.my.user.nickname +  ", 점수 : " + data.my.score + ", 시간 : " + data.my.time;
            $(myrankSelector).append(text);
        });
    },
    checkCallback : function () {
        var titleSelector = "#" + this.topTitleArea;
        var component = this.svg.children();
        var mainSelector = "#" + this.mainArea;

        var count = 0;

        for(var i=1+this.ansZoneCount;i<component.length;i++){
            if(component[i].data('isChecked') === false){
                count++;
            }
        }

        if(count >= this.maxCount){
            // 게임 종료 문구 삽입
            var text = "개수가 꽉 찼습니다.<br>게임오버입니다.";

            console.log(component.length);

            $(titleSelector).html(text);

            var correctAudio = new Audio('fail.mp3');
            correctAudio.play();

            this.stopGame();
        }
        else{
            this.checkCountCallback = false;
        }
    },
    checkCount : function () {
        var titleSelector = "#" + this.topTitleArea;
        var component = this.svg.children();
        var mainSelector = "#" + this.mainArea;

        // console.log(component.length);

        var count = 0;

        for(var i=1+this.ansZoneCount;i<component.length;i++){
            if(component[i].data('isChecked') === false){
                count++;
            }
        }

        if(count >= this.maxCount){
            this.checkCountCallback = true;
            var call = this.checkCallback.bind(this);
            setTimeout(call, 1000);
            // // 게임 종료 문구 삽입
            // var text = "개수가 꽉 찼습니다.<br>게임오버입니다.";
            //
            // console.log(component.length);
            //
            // $(titleSelector).html(text);
            //
            // var correctAudio = new Audio('fail.mp3');
            // correctAudio.play();
            //
            // this.stopGame();
        }
    },
    shuffle : function(a) {
        var j, x, i;
        for (i = a.length; i; i -= 1) {
            j = Math.floor(Math.random() * i);
            x = a[i - 1];
            a[i - 1] = a[j];
            a[j] = x;
        }
    },
    isPointInCircle : function(cx, cy, cr, px, py){
        var deltaX = cx - px;
        var deltaY = cy - py;
        var crr = cr * cr;

        return (deltaX * deltaX + deltaY * deltaY) < crr;
    },
    isCollisionRect : function (cx, cy, cr, x, y, width, height) {
        if( (x <= cx && cx <= width) || (y <= cy && cy <= height)){
            var left = x - cr;
            var top = y - cr;
            var right = width + cr;
            var bottom = height + cr;

            if((left < cx && cx < right) && (top < cy && cy < bottom)){
                return true;
            }
        }
        else{
            if(this.isPointInCircle(cx, cy, cr, x, y)){
                return true;
            }

            if(this.isPointInCircle(cx, cy, cr, x, height)){
                return true;
            }

            if(this.isPointInCircle(cx, cy, cr, width, y)){
                return true;
            }

            if(this.isPointInCircle(cx, cy, cr, width, height)){
                return true;
            }
        }

        return false;
    },
    isCollision : function (c1x, c1y, c1r, c2x, c2y, c2r) {
        var deltaX = c1x - c2x;
        var deltaY = c1y - c2y;

        return ((deltaX * deltaX + deltaY * deltaY) < ((c1r + c2r) * (c1r + c2r)));
    },
    getDxDy : function () {
        var t = Math.random() * 4.0 - 2.0;
        if(t === 0)
            t += 0.5;
        return t;
    },
    updateCircle : function () {
        var component = this.svg.children();
        var selector = "#" + this.mainArea;

        var inter = [];

        var checkAnswers = this.checkAnswers.bind(this);
        var answer = this.ans[this.currentAnsZone];
        var ansColor = this.ansColor[this.currentAnsZone];
        var ansX = this.ansZonePosX[this.currentAnsZone];
        var ansY = this.ansZonePosY[this.currentAnsZone];
        var ansMargin = this.ansZoneMargin;
        var padding = this.ansZonePadding;
        var ansZone = this.currentAnsZone;


        var groups = this.svg.children();
        var deleteGroups = [];
        deleteGroups.length = 0;
        var endGroups = [];
        endGroups.length = 0;
        var isDelete = false;
        var isRemoveAni = false;

        var ansCountMinus = this.counterMinus.bind(this);
        var colorArray = this.color;

        for(var i=0;i<groups.length;i++){
            if(groups[i].data('moved') === true){
                var number = groups[i].data('number');
                var color = groups[i].data('color');
                var first = groups[i].data('first');
                answer[answer.length] = number;
                ansColor[ansColor.length] = color;
                groups[i].data('moved', false);
                if(this.fill > 0){
                    if(answer.length === this.maxAnswer){
                        this.currentAnsZone = 1 - this.currentAnsZone;
                    }
                }
                var colorNum;

                for(var j=0;j<colorArray.length;j++){
                    if(colorArray[j] === color){
                        colorNum = j;
                        break;
                    }
                }
                var index = number + colorNum * 10;

                if(first === false)
                    this.answerMap[index] = false;

                groups[i].animate(300).move((answer.length - 1) * (this.radius*2 + padding) + ansX + ansMargin, ansY + ansMargin).after(function () {
                    checkAnswers(number, ansZone);
                });
            }
            if(groups[i].data('remove') === true && groups[i].data('current') === true){
                isRemoveAni = true;
                deleteGroups.push(groups[i]);
            }
            if(groups[i].data('end') === true && groups[i].data('current') === true){
                isDelete = true;
                endGroups.push(groups[i]);
            }
        }

        if(isRemoveAni){
            for(var i=1;i<component.length;i++){
                for(var j=0;j<this.beforeId.length;j++){
                    if(component[i].id() === this.beforeId[j]){
                        component[i].data('current', true);
                    }
                }
            }
            this.beforeId.length = 0;

            for(var i=deleteGroups.length-1;i>=0;i--){
                deleteGroups[i].data('remove', false);
                deleteGroups[i].animate(200).fill({opacity: 0.0}).after(function () {
                    this.data('end', true);
                });
            }
        }



        if(isDelete){
            this.answerCount = deleteGroups.length;

            for(var i=endGroups.length-1;i>=0;i--){
                var child = endGroups[i].children();
                for(var j=0;j<child.length;j++){
                    child[j].remove();
                }
                endGroups[i].remove();
                ansCountMinus();
            }

            for(var i=1;i<this.ansZoneCount+1;i++){
                var rect = component[i];

                var child = rect.children();

                if((i-1) === this.currentAnsZone){
                    child[0].fill({opacity: 1.0});
                }
                else{
                    child[0].fill({opacity: 0.5});
                }
            }
        }

        var bx = 9999, by = 9999, bx2 = 0, by2 = 0;

        for(var i=1;i<this.ansZoneCount+1;i++){
            var rect = component[i];
            var bbox = rect.bbox();
            var bboxX = rect.cx() - bbox.width / 2;
            var bboxX2 = rect.cx() + bbox.width / 2;
            var bboxY = rect.cy() - bbox.height / 2;
            var bboxY2 = rect.cy() + bbox.height / 2;

            // console.log(bbox.width, rect.cy());
            //
            // console.log(bbox);

            if(bboxX < bx)
                bx = bboxX;
            if(bboxY < by)
                by = bboxY;
            if(bboxX2 > bx2)
                bx2 = bboxX2;
            if(bboxY2 > by2)
                by2 = bboxY2;
        }

        for(var i=1+this.ansZoneCount;i<component.length;i++){
            if(component[i].data('isChecked') === true)
                continue;
            if(inter[i] === true){
                continue;
            }
            var x = component[i].cx(), y = component[i].cy();
            var speed, angle;
            var dx, dy, vx, vy;
            var weight = component[i].data('weight');

            dx = component[i].data('dx');
            dy = component[i].data('dy');

            var cx = x + dx;
            var cy = y + dy;

            var temp = component[i].children();

            if(cy <= this.radius){
                dy = 0;

                // component[i].data('dx', dx);
                component[i].data('dy', dy);

                continue;
            }


            if(this.isCollisionRect.call(this, cx, cy, this.radius, bx, by, bx2, by2)){
                dy = 0;

                if(cy + this.radius < by2){
                    dx = -dx;
                    component[i].data('dx', dx);
                }

                component[i].data('dy', dy);

                continue;
            }


            // check if we hit top/bottom borders
            if ((dy < 0 && cy <= this.radius) || (dy > 0 && cy >= $(selector).height() - this.radius)) {
                dx = this.getDxDy();
                dy = 0;

                component[i].data('dx', dx);
                component[i].data('dy', dy);
                continue;
            }

            if(cx <= temp[0].attr('r')){
                dx = Math.random() * 2.0;
                dy = -1;
                component[i].data('dx', dx);
                component[i].data('dy', dy);
                continue;
            }

            if(cx >= $(selector).width() - this.radius){
                dx = Math.random() * 2.0 - 2.0;
                dy = -1;
                component[i].data('dx', dx);
                component[i].data('dy', dy);
                continue;
            }

            // collision
            var intersection = false;
            var isInter = false;
            for(var j=1+this.ansZoneCount;j<component.length;j++){
                if(i===j){
                    continue;
                }
                // if(done[i][j] === true || inter[i] === true || inter[j] === true){
                //     continue;
                // }

                var t = component[j].children();

                var c2x = component[j].cx();
                var c2y = component[j].cy();


                if(Math.abs(cx - c2x) > this.radius * 2.5 && Math.abs(cy - c2y) > this.radius * 2.5)
                    continue;



                intersection = this.isCollision(cx, cy, this.radius, c2x, c2y, this.radius);

                // if(this.gameMap[cx][cy] !== component[i].id() && this.gameMap[cx][cy] !== 0){
                if(intersection === true){
                    isInter = true;

                    inter[i] = true;
                    inter[j] = true;



                    dx = this.getDxDy();
                    dy = -1;

                    component[i].data('dx', dx);
                    component[i].data('dy', dy);
                    component[i].data('weight', weight+1);


                    dx = this.getDxDy();
                    dy = -1;

                    component[j].data('dx', dx);
                    component[j].data('dy', dy);
                    var w2 = component[j].data('weight');
                    component[j].data('weight', w2+1);

                    break;
                }
            }

            if(cy > $(selector).height()){
                dy -= 1;
            }

            if(isInter === false){
                component[i].dmove(dx, dy);
                dy = -1.5;
                component[i].data('dy', dy);
                component[i].data('weight', 1);
            }
        }
    },
    getNumber : function () {
        // check number
        var numberArray = [];
        var numberCount = [];


        numberCount[0] = 3;
        for(var i=1;i<10;i++){
            numberCount[i] = 0;
        }

        for(var i=0;i<this.answerMap.length;i++){
            if(this.answerMap[i] === false){
                numberArray.push(i);
            }
            else{
                numberCount[i%10]++;
            }
        }

        this.shuffle(numberArray);
        if(numberArray.length === 0){
            console.log("error");
            return Math.random() * 30;
        }

        var count =0;
        while(true){
            for(var i=0;i<numberArray.length;i++){
                if(numberCount[numberArray[i]%10] === count && this.answerMap[numberArray[i]] === false){
                    this.answerMap[numberArray[i]] = true;

                    return numberArray[i];
                }
            }

            count++;
        }

        // return numberArray[Math.floor(Math.random() * numberArray.length)];
    },
    addCallback : function (ms) {
        // console.log(ms);
        var selector = "#" + this.mainArea;
        var colorVal = Math.floor(Math.random() * (this.color.length));
        var component = this.svg.children();

        var count = 0;

        for(var i=1+this.ansZoneCount;i<component.length;i++){
            if(component[i].data('isChecked') === false){
                count++;
            }
        }

        if(count >= this.maxCount){
            var checkCount = this.checkCount.bind(this);
            if(this.checkCountCallback === false){
                this.checkCountCallback = true;
                checkCount();
            }

            var call = this.addCallback.bind(this);

            if(this.endGame === false)
                this.addAnim = setTimeout(call, 5000/this.level);

            return;
        }


        var number = this.getNumber.call(this);

        colorVal = Math.floor(number/10);
        number = number % 10;

        var rect = component[1];
        var bbox = rect.bbox();

        var posXX = [];

        for(var i=0;i<$(selector).width() - (this.radius*4);i++){
            posXX[i] = i + this.radius*2;
        }
        this.shuffle(posXX);
        // var posYY = [];
        //
        var isOK = true;

        var cy = $(selector).height() + this.radius;
        var cx;

        for(var i=0;i<posXX.length;i++){
            cx = posXX[i];

            isOK = true;

            var intersection = false;

            for(var k=2;k<component.length;k++){

                var c2x = component[k].cx();
                var c2y = component[k].cy();

                if(Math.abs(cx - c2x) > this.radius*2 && Math.abs(cy - c2y) > this.radius*2){
                    continue;
                }

                intersection = this.isCollision(cx, cy, this.radius, c2x, c2y, this.radius);

                if(intersection === true){
                    isOK = false;
                    break;
                }
            }

            if(isOK === true)
                break;
        }

        this.level = Math.min(Number(this.defaultLevel) + this.currentTime/10, 20);

        if(isOK === false){
            var call = this.addCallback.bind(this);
            if(this.endGame === false)
                this.addAnim = setTimeout(call, 5000/this.level);
            // return;
        }
        else{
            if(this.endGame === false){
                if(this.addAble === true)
                    this.addNumberComponent.call(this, this.radius*2, this.color[colorVal], number, cx, cy, false, true);

                var checkCount = this.checkCount.bind(this);
                checkCount();

                var call = this.addCallback.bind(this);

                if(this.endGame === false)
                    this.addAnim = setTimeout(call, 5000/this.level);
            }
        }
    },
    displayCallback : function (ms) {
        // console.log(ms);
        var timeSelector = "#" + this.topTimeArea;

        this.updateCircle();

        if(this.time === 0){
            this.time = new Date().getTime();
        }

        var endTime = new Date().getTime();
        // console.log(ms);
        var currentTime = (endTime - this.time)/1000;

        $(timeSelector).html(currentTime.toFixed(2) + "sec");
        this.currentTime = currentTime;

        // setTimeout(this.callback(), 1000);
        // console.log(this);
        var call = this.displayCallback.bind(this);
        this.circleAnim = requestAnimationFrame(call);
    },
    addFillNumber : function () {
        // this.ans.length = 0;
        for(var i=0;i<this.fill;i++){
            var colorVal = Math.floor(Math.random() * (this.color.length));
            var number = Math.floor(Math.random() * 9 + 1);
            var diameter = this.radius * 2;

            if(this.fill > 1 && i > 0){
                while(true){
                    if((this.ans[this.beforeAns][0] + number) % 10 === 0){
                        number = Math.floor(Math.random() * 9 + 1);
                    }
                    else{
                        break;
                    }
                }
            }

            // 수정
            this.addNumberComponent.call(this, diameter, this.color[colorVal], number, (diameter + this.ansZonePadding) * i + this.ansZoneMargin + this.ansZonePosX[this.beforeAns], this.ansZoneMargin + this.ansZonePosY[this.beforeAns], true, false);
            this.ans[this.beforeAns][i] = number;
            this.ansColor[this.beforeAns][i] = this.color[colorVal];
        }

        this.beforeAns = 1 - this.beforeAns;
    },
    counterMinus : function () {
        this.answerCount--;
        // if(this.answerCount < 0){
        //     this.answerCount = 0;
        // }
    },
    countCallback : function () {
        // console.log(this.answerCount);
        var addFill = this.addFillNumber.bind(this);

        if(this.answerCount <= 0){
            addFill();
            clearTimeout(this.counter);
            this.addAble = true;
        }
        else{
            var call = this.countCallback.bind(this);

            if(this.endGame === false)
                this.counter = setTimeout(call, 100);
        }
    },
    checkAnswers : function (ans, ansZone) {
        // this.ans.push(ans);
        // console.log("answer : " + this.ans[ansZone]);

        var isFail = false;
        var titleSelector = "#" + this.topTitleArea;
        var scoreSelector = "#" + this.topScoreArea;
        var mainSelector = "#" + this.mainArea;

        // clearTimeout(this.addAnim);

        if(this.ans[ansZone].length === this.maxAnswer){
            var sum = 0;
            var sumColor = [];
            sumColor.length = 0;
            for(var i=0;i<this.color.length;i++){
                sumColor[i] = 0;
            }
            for(var i=0;i<this.ans[ansZone].length;i++){
                sum += this.ans[ansZone][i];
            }
            for(var i=0;i<this.ansColor[ansZone].length;i++){
                for(var j=0;j<this.color.length;j++){
                    if(this.color[j] === this.ansColor[ansZone][i]){
                        sumColor[j]++;
                        break;
                    }
                }
            }
            this.ans[ansZone].length = this.fill;
            this.ansColor[ansZone].length = this.fill;
            // this.currentAnsZone = 1 - this.currentAnsZone;

            if(sum % 10 === 0){
                var maxColor = 0;
                for(var i=0;i<sumColor.length;i++){
                    if(maxColor < sumColor[i]){
                        maxColor = sumColor[i];
                    }
                }

                if(this.type === "two"){
                    if(maxColor === 2){
                        this.score = this.score + 3;
                    }
                    else{
                        this.score = this.score + 1;
                    }
                }
                else if(this.type === "three"){
                    if(maxColor === 3){
                        this.score = this.score + 5;
                    }
                    else if(maxColor === 1){
                        this.score = this.score + 2;
                    }
                    else{
                        this.score = this.score + 1;
                    }
                }
                // this.score++;
                // this.level = Math.min(Number(this.defaultLevel) + this.currentTime/10, 20);

                $(scoreSelector).html(this.score + "점");
                // this.time = 0;

                var groups = this.svg.children();

                for(var i=0;i<groups.length;i++){
                    if(groups[i].data('isChecked') === true && groups[i].data('current') === true){
                        groups[i].data('remove', true);
                    }
                }

                // this.beforeAns = ansZone;
                this.answerCount = this.fill;
                this.counter = setTimeout(this.countCallback(), 100);

                var correctAudio = new Audio('correct.mp3');
                correctAudio.play();
            }
            else{
                isFail = true;
            }
        }

        if(isFail){
            // 게임 오버 처리
            var text = "틀렸습니다.<br>게임오버입니다.";

            var correctAudio = new Audio('fail.mp3');
            correctAudio.play();

            $(titleSelector).html(text);

            this.stopGame();
        }

        // this.isCheckAnswer = false;

        var addCall = this.addCallback.bind(this);
        // this.addAnim = setTimeout(addCall, 5000/this.level);
    },
    setCheckAnswer : function (bool) {
        this.isCheckAnswer = bool;
    },
    getCheckedAnswer : function () {
        return this.isCheckAnswer;
    },
    addCircleEvent : function (circle) {
        var svg = this.svg;
        var answer = this.ans[this.currentAnsZone];
        var max = this.maxAnswer;
        var isChecked = this.getCheckedAnswer.bind(this);
        var radius = this.radius;

        circle.mouseover(function (e) {
            // e.preventDefault();
            // this.radius(40);
            // var svg = this.parent();

            var groups = svg.children();
            var target = this.parent();


            for(var i=1;i<groups.length;i++){
                var temp = groups[i].children();

                if(target.id() === groups[i].id() && groups[i].data('isChecked') === false){
                    groups[i].fill({opacity: 0.7});
                    // temp[0].radius(radius * 1.2);
                    // var b = temp[0].bbox();
                    // temp[1].attr('width', b.width);
                    // temp[1].attr('height', b.height);
                    // temp[1].fill(svg.image('image/' + groups[i].data('color') + "_" + groups[i].data('number') + '.png', b.width, b.height));
                }
            }
        });
        circle.mouseout(function (e) {
            // e.preventDefault();
            // this.radius(25);

            var groups = svg.children();
            var target = this.parent();


            for(var i=1;i<groups.length;i++){
                var temp = groups[i].children();

                if(target.id() === groups[i].id() && groups[i].data('isChecked') === false){
                    groups[i].fill({opacity: 1.0});
                    // temp[0].radius(radius);
                    // var b = temp[0].bbox();
                    // temp[1].attr('width', b.width);
                    // temp[1].attr('height', b.height);
                    // temp[1].fill(svg.image('image/' + groups[i].data('color') + "_" + groups[i].data('number') + '.png', b.width, b.height));
                }
            }
        });
        circle.mousedown(function (e) {
            // e.preventDefault();
            var groups = svg.children();
            var target = this.parent();

            if(isChecked() === false){
                for(var i=1;i<groups.length;i++){
                    var temp = groups[i].children();

                    if(target.id() === groups[i].id() && groups[i].data('isChecked') === false){
                        if(answer.length === max){
                            console.log(answer.length);
                            return;
                        }

                        groups[i].data('isChecked', true);
                        groups[i].data('moved', true);
                        temp[0].radius(radius);
                        groups[i].fill({opacity: 1.0});
                        // var b = temp[0].bbox();
                        // temp[1].attr('width', b.width);
                        // temp[1].attr('height', b.height);
                        // temp[1].fill(svg.image('image/' + groups[i].data('color') + "_" + groups[i].data('number') + '.png', b.width, b.height));

                        return;
                    }
                }
            }
        });
    },
    addAnswerZone : function (number) {
        var svg = this.svg;
        var max = this.maxAnswer;
        var group = svg.group();

        var radius = this.radius * 2;
        var width = radius * max + this.ansZonePadding * (max - 1) + this.ansZoneMargin * 2;
        var height = radius + this.ansZoneMargin * 2;

        var selector = "#" + this.mainArea;



        this.ansZonePosX[number] = $(selector).width()/2 - width/2;
        this.ansZonePosY[number] = number * height;

        var imgTxt = "image/zone_";

        if(this.type === "two"){
            imgTxt += "02_";
        }
        else{
            imgTxt += "03_";
        }

        // if(number !== this.currentAnsZone){
        //     imgTxt += "02.png";
        // }
        // else{
            imgTxt += "01.png";
        // }

        var rect = svg.rect(width, height).fill(svg.image(imgTxt, width, height));

        if(number !== this.currentAnsZone) {
            rect.fill({opacity: 0.5});
        }


        // var rect = svg.rect(width, height).fill({opacity: 0.0}).stroke({color: 'black', width: 1}).move(this.ansZonePosX[number], this.ansZonePosY[number]);
        //
        // if(number !== this.currentAnsZone){
        //     rect.fill({color: "grey", opacity: 0.5});
        // }

        group.add(rect);

        group.move(this.ansZonePosX[number], this.ansZonePosY[number]);

        // for(var j=0;j<max;j++){
        //     var circle = svg.circle(radius).fill({opacity: 0.0}).stroke({color: 'black', width: 1}).move(this.ansZoneMargin + (radius + this.ansZonePadding) * j + this.ansZonePosX[number], this.ansZonePosY[number] + this.ansZoneMargin);
        //     group.add(circle);
        // }
    },
    addNumberComponent : function (radius, color, number, posX, posY, isChecked, current) {
        var svg = this.svg;
        var group = svg.group();
        var selector = "#" + this.mainArea;

        var colorNum;
        for(var i=0;i<this.color.length;i++){
            if(color === this.color[i]){
                colorNum = i;
                break;
            }
        }

        // this.answerMap[colorNum*10 + number] = !isChecked;
        // console.log(colorNum * 10 + number);

        var circle = svg.circle(radius).fill({color: color, opacity: 0.0});
        var b = circle.bbox();
        // var image = svg.image('image/' + color + "_" + number + '.png', b.width, b.height);
        var rect = svg.rect(b.width, b.height).move(b.x, b.y).fill(svg.image('image/' + color + "_" + number + '.png', b.width, b.height));
        // var rect = svg.rect(b.width, b.height).move(b.x, b.y).fill(svg.image('image/number_B/' + number + '.png', b.width, b.height));

        var addCircleEvent = this.addCircleEvent.bind(this);
        addCircleEvent(rect);

        group.add(circle);
        group.add(rect);
        group.move(posX, posY);
        group.data('type', 'circle');
        group.data('number', number);
        group.data('color', color);
        group.data('isChecked', isChecked);
        group.data('first', isChecked);
        group.data('remove', false);
        group.data('moved', false);
        group.data('weight', 1);
        group.data('end', false);
        group.data('current', current);

        var dx = this.getDxDy();
        if(posX > 400){
            dx = Math.random() * 2.0 - 2.0;
        }
        else{
            dx = Math.random() * 2.0;
        }

        var dy = -2.5;

        group.data('dx', dx);
        group.data('dy', dy);

        if(current === false){
            this.beforeId[this.beforeId.length] = group.id();
        }
    },
    createMainArea : function () {
        var allSelector = "#" + this.allArea;
        var mainSelector = "#" + this.mainArea;

        // add top area
        this.appendDiv(allSelector, this.mainArea, this.mainCss);

        this.svg = SVG(this.mainArea);


        // this.svg.image('image/bg.png');

        for(var i=0;i<this.ansZoneCount;i++){
            this.addAnswerZone.call(this, i);
        }

        for(var j=0;j<this.ansZoneCount;j++){
            for(var i=0;i<this.fill;i++){
                var colorVal = Math.floor(Math.random() * (this.color.length));
                var number = Math.floor(Math.random() * 9 + 1);

                var diameter = this.radius * 2;

                if(this.fill > 1 && i > 0){
                    while(true){
                        if((this.ans[j][0] + number) % 10 === 0){
                            number = Math.floor(Math.random() * 9 + 1);
                        }
                        else{
                            break;
                        }
                    }
                }

                if(j === this.currentAnsZone){
                    this.addNumberComponent.call(this, diameter, this.color[colorVal], number, (diameter + this.ansZonePadding) * i + this.ansZonePosX[j] + this.ansZoneMargin, this.ansZonePosY[j] + this.ansZoneMargin, true, true);
                }
                else{
                    this.addNumberComponent.call(this, diameter, this.color[colorVal], number, (diameter + this.ansZonePadding) * i + this.ansZonePosX[j] + this.ansZoneMargin, this.ansZonePosY[j] + this.ansZoneMargin, true, false);
                }
                this.ans[j].push(number);
                this.ansColor[j].push(this.color[colorVal]);
            }
        }



        // console.log(this.gameMap);

        // this.svg.rect(200,100);

        var start = 5;
        for(var i=0;i<start;i++){
            var colorVal = Math.floor(Math.random() * (this.color.length));
            var min = this.radius * 2;
            var wMax = $(mainSelector).width() - min;
            var hMax = $(mainSelector).height() - min;
            var posX = Math.random() * (wMax - min) + min;
            var posY = Math.random() * (hMax - min) + min;
            var number = this.getNumber.call(this);
            // var number = 3;
            var component = this.svg.children();

            var rect = component[1];
            var bbox = rect.bbox();

            // console.log(number);

            colorVal = Math.floor(number/10);
            number = number % 10;
            // number = 3;

            while(true){
                wMax = $(mainSelector).width() - min;
                hMax = $(mainSelector).height() - min;
                posX = Math.random() * (wMax - min) + min;
                posY = Math.random() * (hMax - min) + min;

                var isIntersection = false;

                // collision
                var cx = posX;
                var cy = posY;
                // console.log(transform.x);

                // collision detect answer zone
                if(this.isCollisionRect.call(this, cx, cy, this.radius*2, bbox.x, bbox.y, bbox.x2, bbox.y2)){
                    continue;
                }

                var intersection = false;
                for(var j=2;j<component.length;j++){

                    var c2x = component[j].cx();
                    var c2y = component[j].cy();

                    intersection = this.isCollision(cx, cy, this.radius*2, c2x, c2y, this.radius*2);

                    if(intersection === true){
                        isIntersection = true;
                        break;
                    }
                }

                if(isIntersection === false){
                    break;
                }
            }

            this.addNumberComponent.call(this, this.radius*2, this.color[colorVal], number, posX, posY, false, true);
        }


        // obj = this;
        this.displayCallback.call(this);
        var call = this.addCallback.bind(this);
        // this.addCallback.call(this);

        this.addAble = true;

        this.addAnim = setTimeout(call, 5000 / this.level);
        // var group = svg.group();
        //
        // var circle = svg.circle(50).fill({color: "red", opacity: 0.7});
        // var b = circle.bbox();
        // var rect = svg.rect(b.width, b.height).move(b.x, b.y).fill(svg.image('image/number_B/0.png', b.width, b.height));
        //
        // group.add(circle);
        // group.add(rect);
        // group.move(20, 20);

        // var text = svg.plain("1").attr({x: circle.attr('cx'), y: circle.attr('cy'), 'text-anchor': 'middle', fill: 'white'}).font({family: 'Roboto', size: '30px'});
        // var testG = svg.group();
        // testG.add(circle);
        // testG.add(text);
    },
    createBotArea : function () {
        var allSelector = "#" + this.allArea;
        var progressSelector = "#" + this.progressArea;

        this.appendDiv(allSelector, this.progressArea, this.progressCss);

        this.progressSvg = SVG(this.progressArea);

        this.progressSvg.rect($(progressSelector).width(), $(progressSelector).height()).fill("red");
    },
    drawGame : function () {
        this.createAllArea();
        this.createTopArea();
        this.createMainArea();
        // this.createBotArea();
    },
    stopGame : function () {
        // var selector = "#" + this.allArea;
        this.endGame = true;
        var mainSelector = "#" + this.mainArea;

        cancelAnimationFrame(this.circleAnim);
        // clearInterval(this.addAnim);
        clearTimeout(this.addAnim);
        clearTimeout(this.counter);
        // clearTimeout(this.checkCountCallback);
        $(mainSelector).css('pointer-events', 'none');
        // $(selector).remove();

        gameStop();
    }
};