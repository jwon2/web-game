﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Oven_Button : MonoBehaviour
{
    public GameObject Pizza;
    public Pizza Pizzacs;

    // Start is called before the first frame update
    private void Awake()
    {
        Pizzacs = Pizza.GetComponent<Pizza>();
    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Pizzacs.Has_Cheese() && Pizzacs.Has_Dough() && Pizzacs.Has_Sauce())
        {
            GetComponent<Button>().interactable = true;
        }
        else
        {
            GetComponent<Button>().interactable = false;
        }
    }
}
