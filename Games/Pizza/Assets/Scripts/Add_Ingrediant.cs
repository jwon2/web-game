﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Add_Ingrediant : MonoBehaviour
{
    public int index;
    public GameObject Ingrediant;

    public GameObject GMGO;
    private GameManager GM;

    public GameObject Kitchen;

    public GameObject Problem1, Problem2, Problem3;
    

    private void Awake()
    {
        GM = GMGO.GetComponent<GameManager>();
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Add()
    {
        GM.Lost_Money(GM.price_topping[index]);
        OpenProblem();
    }
    public void Get()
    {

        GM.Now_ingredient[index] += 5;
        Ingrediant.GetComponent<Ingrediant>().Change_Bucket();
    }
    public struct problem1
    {
        public int ansIdx;
        public string question;
        public string[] choice;
        public problem1(int ansIdx, string question, string[] choice)
        {
            this.ansIdx = ansIdx;
            this.question = question;
            this.choice = choice;
        }
        public string ToString()
        {
            return "ansIdx: " + ansIdx.ToString() + "\n" +
                "question: " + question + "\n" +
                "choices: " + choice[0] + " " + choice[1] + " " + choice[2] + " " + choice[3];
        }
    }
    public struct problem2
    {
        public int ansIdx;
        public string question;
        public problem2(int ansIdx, string question)
        {
            this.ansIdx = ansIdx;
            this.question = question;
        }
        public string ToString()
        {
            return "ansIdx: " + ansIdx.ToString() + "\n" +
                "question: " + question;
        }
    }
    public struct problem3
    {
        public int ansIdx;
        public int edge, deltaV, deltaH;
        public string question;
        public string[] choice;
        public problem3(int ansIdx, int edge, int deltaV, int deltaH, string question, string[] choice)
        {
            this.ansIdx = ansIdx;
            this.edge = edge;
            this.deltaV = deltaV;
            this.deltaH = deltaH;
            this.question = question;
            this.choice = choice;
        }
        public string ToString()
        {
            return "ansIdx: " + ansIdx.ToString() + "\n" +
                "edge: " + edge.ToString() + "\n" +
                "deltaV: " + deltaV.ToString() + "\n" +
                "deltaH: " + deltaH.ToString() + "\n" +
                "question: " + question + "\n" +
                "choices: " + choice[0] + " " + choice[1] + " " + choice[2] + " " + choice[3];
        }
    }

    public void OpenProblem()
    {
        if(index == 0 || index == 1){
            GameObject newProblem = Instantiate(Problem2, Kitchen.transform);
            problem2 newPro = Problem.SetProblem2();
            newProblem.GetComponent<Problem2>().Problem.text = newPro.question;
            newProblem.GetComponent<Problem2>().ansIdx = newPro.ansIdx;
            newProblem.GetComponent<Problem2>().this_num = index;
            for(int i=0;i< newProblem.GetComponent<Problem2>().Ingrediants.Length; i++)
            {
                newProblem.GetComponent<Problem2>().Ingrediants[i].SetActive(false);
            }
            newProblem.GetComponent<Problem2>().Ingrediants[index].SetActive(true);
        }
        else if(index ==2 || index ==4 || index == 6)
        {

            GameObject newProblem = Instantiate(Problem3, Kitchen.transform);
            problem3 newPro = Problem.SetProblem3();
            newProblem.GetComponent<Problem3>().Problem.text = newPro.question;
            for (int i = 0; i < 4; i++)
            {
                newProblem.GetComponent<Problem3>().Choice[i].text = newPro.choice[i];
            }
            newProblem.GetComponent<Problem3>().ansIdx = newPro.ansIdx;

            newProblem.GetComponent<Problem3>().this_num = index;
            newProblem.GetComponent<Problem3>().edge = newPro.edge;
            for (int i = 0; i < newProblem.GetComponent<Problem3>().Ingrediants.Length; i++)
            {
                newProblem.GetComponent<Problem3>().Ingrediants[i].SetActive(false);
            }
            newProblem.GetComponent<Problem3>().Ingrediants[index].SetActive(true);
        }
        else
        {
            GameObject newProblem = Instantiate(Problem1, Kitchen.transform);
            problem1 newPro = Problem.SetProblem1();
            newProblem.GetComponent<Problem1>().Problem.text = newPro.question;
            for(int i = 0; i < 4; i++)
            {
                newProblem.GetComponent<Problem1>().Choice[i].text = newPro.choice[i];
            }
            newProblem.GetComponent<Problem1>().ansIdx = newPro.ansIdx;
            newProblem.GetComponent<Problem1>().this_num = index;
            for (int i = 0; i < newProblem.GetComponent<Problem1>().Ingrediants.Length; i++)
            {
                newProblem.GetComponent<Problem1>().Ingrediants[i].SetActive(false);
            }
            newProblem.GetComponent<Problem1>().Ingrediants[index].SetActive(true);
        }
    }
}
