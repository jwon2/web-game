﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Dough : MonoBehaviour, IPointerClickHandler, IPointerDownHandler, IPointerEnterHandler, IDragHandler, IPointerExitHandler, IPointerUpHandler
{
    public GameObject[] Doughs = new GameObject[6];
    public GameObject[] Doughs_pos = new GameObject[6];
    public GameObject Dough_Prefab;

    public GameObject Pizza;

    int count;

    private void Awake()
    {
        count = 6;
        
        
        StartCoroutine( First_Set());
    }
    IEnumerator First_Set()
    {
        yield return new WaitForSeconds(0.1f);
        for (int i = 0; i < 6; i++)
        {
            Doughs[i] = Instantiate(Dough_Prefab, transform);
            Doughs[i].transform.position = Doughs_pos[i].transform.position;
        }
    }
    void Initalize()
    {

        count = 6;
        StartCoroutine(Charge_Dough());

    }
    IEnumerator Charge_Dough()
    {
        yield return new WaitForSeconds(0.1f);
        for (int i = 0; i < 6; i++)
        {
            Doughs[i] = Instantiate(Dough_Prefab, transform);
            Doughs[i].transform.position = Doughs_pos[i].transform.position;
            yield return new WaitForSeconds(0.2f);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        Doughs[count - 1].transform.position = Doughs_pos[count - 1].transform.position;
    }

    public void OnDrag(PointerEventData eventData)
    {
        Doughs[count - 1].transform.position = Input.mousePosition;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        Doughs[count - 1].transform.position += new Vector3(0f, 10f, 0f);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
    }

    public void OnPointerClick(PointerEventData eventData)
    {
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if(Vector3.Distance(Doughs[count-1].transform.position,Pizza.transform.position) < 200f && !Pizza.GetComponent<Pizza>().Has_Dough())
        {
            Pizza.GetComponent<Pizza>().Get_Dough();
            Destroy(Doughs[count - 1].gameObject);
            count--;
            if (count == 0)
            {
                Initalize();
            }
        }
        Doughs[count - 1].transform.position = Doughs_pos[count - 1].transform.position;
    }
}
