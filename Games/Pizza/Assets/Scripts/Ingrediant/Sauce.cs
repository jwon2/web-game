﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Sauce : MonoBehaviour, IPointerClickHandler, IPointerDownHandler, IPointerEnterHandler, IDragHandler, IPointerExitHandler, IPointerUpHandler
{
    public GameObject Sauce_pos;
    public GameObject Pizza;

    public void OnDrag(PointerEventData eventData)
    {
        transform.position = Input.mousePosition;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
    }

    public void OnPointerDown(PointerEventData eventData)
    {
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        transform.position += new Vector3(0f, 10f, 0f);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        transform.position = Sauce_pos.transform.position;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (Vector3.Distance(transform.position, Pizza.transform.position) < 200f && !Pizza.GetComponent<Pizza>().Has_Sauce() && Pizza.GetComponent<Pizza>().Has_Dough())
        {
            Pizza.GetComponent<Pizza>().Get_Sauce();
            transform.position = Sauce_pos.transform.position;
        }
        else
        {
            transform.position = Sauce_pos.transform.position;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
