﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Cheese : MonoBehaviour, IPointerClickHandler, IPointerDownHandler, IPointerEnterHandler, IDragHandler, IPointerExitHandler, IPointerUpHandler
{
    public GameObject Cheese_Prefab;
    public GameObject Cheese_NOscript_Prefab;

    public GameObject Pizza;

    GameObject Cheese_Instance;
    GameObject Cheese_NOscript_Instance;

    public void OnDrag(PointerEventData eventData)
    {
        Cheese_Instance.transform.position = Input.mousePosition;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        Destroy(Cheese_NOscript_Instance.gameObject);
        Cheese_Instance = Instantiate(Cheese_Prefab, transform);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if(Cheese_Instance==null)
            Cheese_NOscript_Instance = Instantiate(Cheese_NOscript_Prefab, transform);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        Destroy(Cheese_NOscript_Instance.gameObject);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if(Vector3.Distance(Cheese_Instance.transform.position, Pizza.transform.position)<200f && !Pizza.GetComponent<Pizza>().Has_Cheese() && Pizza.GetComponent<Pizza>().Has_Dough())
        {
            Pizza.GetComponent<Pizza>().Get_Cheese();
            Destroy(Cheese_Instance.gameObject);
        }
        else{
            Destroy(Cheese_Instance.gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
