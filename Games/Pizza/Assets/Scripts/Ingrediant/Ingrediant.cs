﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Ingrediant : MonoBehaviour, IPointerClickHandler, IPointerDownHandler, IPointerEnterHandler, IDragHandler, IPointerExitHandler, IPointerUpHandler
{
    public  int index;

    public GameObject Pizza;
    public GameObject GMGO;
    private GameManager GM;

    public GameObject Instance_Prefab;
    public GameObject Instance_Noscript_Prefab;

    public GameObject[] Buckets = new GameObject[4];

    public Text Count;

    GameObject Instance;
    GameObject Instance_Noscript;

    public void OnDrag(PointerEventData eventData)
    {
        Instance.transform.position = Input.mousePosition;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (GM.Now_ingredient[index] > 0)
        {
            Destroy(Instance_Noscript.gameObject);
            Instance = Instantiate(Instance_Prefab, transform);
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (GM.Now_ingredient[index] > 0)
        {
            if(Instance==null)
                Instance_Noscript = Instantiate(Instance_Noscript_Prefab, transform);
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if(Instance_Noscript!=null)
            Destroy(Instance_Noscript.gameObject);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (GM.Now_ingredient[index] > 0)
        {
            if (Vector3.Distance(Instance.transform.position, Pizza.transform.position) < 200f && Pizza.GetComponent<Pizza>().Has_Dough())
            {
                Pizza.GetComponent<Pizza>().Put_Topping(index);
                Destroy(Instance.gameObject);
            }
            else
            {
                Destroy(Instance.gameObject);
            }
        }
    }

    public void Change_Bucket()
    {
        Count.text = GM.Now_ingredient[index].ToString();

        for (int i = 0; i < 4; i++)
            Buckets[i].SetActive(false);
        if (GM.Now_ingredient[index] >= 5)
        {
            Buckets[3].SetActive(true);
        }
        else if(GM.Now_ingredient[index]>=3)
        {
            Buckets[2].SetActive(true);
        }
        else if (GM.Now_ingredient[index] >= 1)
        {
            Buckets[1].SetActive(true);
        }
        else
        {
            Buckets[0].SetActive(true);
        }
    }

    private void Awake()
    {
        GM = GMGO.GetComponent<GameManager>();
        
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
