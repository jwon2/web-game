﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Problem : Add_Ingrediant {
    static string[] text = {
        "A가 일어날 확률",
        "A가 일어나지 않을 확률",
        "A가 일어났을 때 B가 일어날 확률",
        "A가 일어났을 때 B가 일어나지 않을 확률",
        "A가 일어나지 않았을 때 B가 일어날 확률",
        "A가 일어나지 않았을 때 B가 일어나지 않을 확률",
        "A가 일어나고 B도 일어날 확률",
        "A가 일어나고 B가 일어나지 않을 확률",
        "A가 일어나지 않고 B가 일어날 확률",
        "A가 일어나지 않고 B도 일어나지 않을 확률",
        "B가 일어날 확률",
        "B가 일어나지 않을 확률"
    };

    static int[,] ProblemCases = {
        { 0, 1, 2 },
        { 0, 1, 4 },
        { 0, 1, 5 },
        { 0, 2, 3 },
        { 0, 2, 5 },
        { 0, 3, 4 },
        { 0, 3, 5 },
        { 0, 4, 5 },
        { 1, 2, 3 },
        { 1, 2, 4 },
        { 1, 2, 5 },
        { 1, 3, 4 },
        { 1, 3, 5 },
        { 1, 4, 5 },
        { 2, 3, 4 },
        { 2, 3, 5 },
        { 2, 4, 5 }
    };

    void Start() {
        Debug.Log(SetProblem1().ToString());
        Debug.Log(SetProblem2().ToString());
        Debug.Log(SetProblem3().ToString());
    }



    public static problem1 SetProblem1() {
        // 문제 경우
        int ProblemCase = Random.Range(0, 18);

        // 문제에서 주는 확률 idx[3]: 0 ~ 12
        int[] ProbIdx = new int[3];
        if (ProblemCase == 17) {
            int r = Random.Range(0, 4);
            if (r == 0)
                ProbIdx = new int[] { 6, 9, 10 };
            else if (r == 1)
                ProbIdx = new int[] { 6, 9, 11 };
            else if (r == 2)
                ProbIdx = new int[] { 7, 8, 10 };
            else
                ProbIdx = new int[] { 7, 8, 11 };
        }
        else
            ProbIdx = new int[] { 2 * ProblemCases[ProblemCase, 0] + Random.Range(0, 2), 2 * ProblemCases[ProblemCase, 1] + Random.Range(0, 2), 2 * ProblemCases[ProblemCase, 2] + Random.Range(0, 2) };

        // 정답 확률 idx
        int ansProbIdx;
        do { ansProbIdx = Random.Range(0, 6); } while (ansProbIdx == ProbIdx[0] / 2 || ansProbIdx == ProbIdx[1] / 2 || ansProbIdx == ProbIdx[2] / 2);
        ansProbIdx = 2 * ansProbIdx + Random.Range(0, 2);

        // 값
        float[] p = new float[3];
        do {
            for (int i = 0; i < 3; i++)
                p[i] = Random.Range(1, 10) / 10f;
        } while (p[0] == 0.5f && p[1] == 0.5f && p[2] == 0.5f);

        // 보기 만들기
        float[] probList = {
            p[0],
            1f - p[0],
            p[1],
            1f - p[1],
            p[2],
            1f - p[2],
            p[0] * p[1],
            p[0] * (1f - p[1]),
            (1f - p[0]) * p[2],
            (1f - p[0]) * (1f - p[2]),
            p[0] * p[1] + (1f - p[0]) * p[2],
            p[0] * (1f - p[1]) + (1f - p[0]) * (1f - p[2])
        };

        // 문제 보기
        float[] orderedChoice = new float[4];
        orderedChoice[0] = probList[ansProbIdx];
        do { orderedChoice[1] = probList[Random.Range(0, 12)]; } while(orderedChoice[1] == orderedChoice[0]);
        do { orderedChoice[2] = probList[Random.Range(0, 12)]; } while (orderedChoice[2] == orderedChoice[0] || orderedChoice[2] == orderedChoice[1]);
        do { orderedChoice[3] = probList[Random.Range(0, 12)]; } while (orderedChoice[3] == orderedChoice[0] || orderedChoice[3] == orderedChoice[1] || orderedChoice[3] == orderedChoice[2]);

        // 랜덤 idx
        int ansIdx = -1;
        int[] shuffle = { -1, -1, -1, -1 };
        for (int i = 0; i < 4; i++) {
            int tmp;
            do { tmp = Random.Range(0, 4); } while (shuffle[tmp] != -1);
            if (i == 0)
                ansIdx = tmp;
            shuffle[tmp] = i;
        }

        // 문제
        string question = "";
        for (int i = 0; i < 3; i++)
            question += text[ProbIdx[i]] + ": " + probList[ProbIdx[i]].ToString("0.##") + "\n";
        question += text[ansProbIdx] + "은?";


        string[] choice = {
            orderedChoice[shuffle[0]].ToString("0.##"),
            orderedChoice[shuffle[1]].ToString("0.##"),
            orderedChoice[shuffle[2]].ToString("0.##"),
            orderedChoice[shuffle[3]].ToString("0.##")
        };
        
        return new problem1(ansIdx, question, choice);
    }



    public static problem2 SetProblem2() {
        int ansIdx = Random.Range(0, 12);
        return new problem2(ansIdx, text[ansIdx]);
    }


    public static problem3 SetProblem3() {
        int edge = Random.Range(3, 7);
        int deltaV = Random.Range(1, 10);
        int deltaH = Random.Range(1, 10);
        int ansIdx = -1;
        int ans = ((int)Mathf.Pow(10f, (float)(edge+deltaV))) * ((int)Mathf.Pow(10f, (float)(edge + deltaH)));
        string question = "(" + KMGtoSTR(edge) + " + " + deltaV.ToString() + ") x (" + KMGtoSTR(edge) + " + " + deltaH.ToString() + ")=?";

        int[] shuffle = { -1, -1, -1, -1 };
        for (int i = 0; i < 4; i++) {
            int idx;
            do { idx = Random.Range(0, 4); } while (shuffle[idx] != -1);
            if (i == 0)
                ansIdx = idx;
            shuffle[idx] = i;
        }

        string[] orderedChoice = new string[4];
        orderedChoice[0] = KMGtoSTR(2 * edge) + (deltaV + deltaH).ToString() + KMGtoSTR(edge) + (deltaV * deltaH).ToString();
        orderedChoice[1] = KMGtoSTR(2 * edge) + (deltaV * deltaH).ToString();
        orderedChoice[2] = KMGtoSTR(2 * edge) + (deltaV + deltaH).ToString() + KMGtoSTR(edge);
        orderedChoice[3] = KMGtoSTR(2 * edge) + (deltaV * deltaH).ToString() + KMGtoSTR(edge) + ((deltaV + deltaH) == (deltaV * deltaH) ? deltaH : (deltaV + deltaH)).ToString();

        string[] choice = new string[4];
        for (int i = 0; i < 4; i++)
            choice[i] = orderedChoice[shuffle[i]];

        return new problem3(ansIdx, edge, deltaV, deltaH, question, choice);
    }

    // 10^p 한글로 display
    static public string KMGtoSTR(int p) {
        string[] THT = { "", "십", "백", "천" };
        string[] KMG = { "", "만", "억", "조" };
        if (p % 4 == 0 && p / 4 != 1)
            return '1' + KMG[p / 4];
        return THT[p % 4] + KMG[p / 4];
    }

    // 숫자 n 한글로 10보다 큰 수
    static public string wingToStr(int p,int n = 1) {
        string[] THT = { "", "십", "백", "천" };
        string[] KMG = { "", "만", "억", "조" };
        if (p % 4 == 0 && p / 4 != 1)
            return '1' + KMG[p / 4];
        return n.ToString() + THT[p % 4] + KMG[p / 4];
    }
}
