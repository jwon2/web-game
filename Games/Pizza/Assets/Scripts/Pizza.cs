﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pizza : MonoBehaviour
{
    public GameObject GMGO;
    private GameManager GM;

    bool HasDough, HasCheese, HasSauce;

    int[] Ingredients = new int[8];

    /* Layers */
    public GameObject Dough_layer, Sauce_layer, BaseCheese_layer, Toppings_layer, ToppingCheese_layer;

    /* Prefabs */
    public GameObject Dough;
    public GameObject Cheese_base;
    public GameObject Cheese_topping;
    public GameObject Sauce;
    public GameObject[] Toppings = new GameObject[8];
    public GameObject[] Toppings2 = new GameObject[8];

    public AudioSource Topping_AS, Dough_AS, Sauce_AS, Cheese_AS;

    class Grid
    {
        public bool[,] by3;

        public Grid()
        {
            by3 = new bool[,] { { false, false, false }, { false, false, false }, { false, false, false } };
        }
        public bool Isfull()
        {
            bool flag = true;
            foreach(bool i in by3)
            {
                if (!i)
                    flag = false;
            }
            return flag;
        }
    }
    Grid[] ToppingGrid = new Grid[8];

    public void Initialize()
    {
        HasDough = false;
        HasCheese = false;
        HasSauce = false;
        Ingredients = new int[8];
        Topping_Initialize();
    }
    public int[] Get_Ingrediant()
    {
        return Ingredients;
    }
    
    public void Get_Dough()
    {
        Dough_AS.Play();
        HasDough = true;
        GM.Lost_Money(GM.dough);
        GameObject newDough = Instantiate(Dough, Dough_layer.transform);
        newDough.GetComponent<Animator>().SetTrigger("IsFirst");
    }
    public bool Has_Dough()
    {
        return HasDough;
    }
    public void Get_Cheese()
    {
        Cheese_AS.Play();
        HasCheese = true;
        GM.Lost_Money(GM.cheese);
        Instantiate(Cheese_base, BaseCheese_layer.transform);
        Instantiate(Cheese_topping, ToppingCheese_layer.transform);
    }
    public bool Has_Cheese()
    {
        return HasCheese;
    }
    public void Get_Sauce()
    {
        Sauce_AS.Play();
        HasSauce = true;
        GM.Lost_Money(GM.sauce);
        GameObject newSauce = Instantiate(Sauce, Sauce_layer.transform);
        newSauce.GetComponent<Animator>().SetTrigger("IsFirst");
    }
    public bool Has_Sauce()
    {
        return HasSauce;
    }
    public void Put_Topping(int index)
    {
        GM.Lost_Ingrediant(index);
        StartCoroutine(Topping(index));
    }
    IEnumerator Topping(int index)
    {
        int cnt = 0;

        Ingredients[index]++;
        while (cnt < 5) { 
            if(Random.value<0.5f)
            {

                int h = Random.Range(0, 3), v=Random.Range(0,3);
                if (!ToppingGrid[index].by3[h, v])
                {

                    GameObject tops = Instantiate(Toppings[index], Toppings_layer.transform);
                    tops.transform.Rotate(new Vector3(0f, 0f, 1f), Random.Range(0f, 360f));
                    tops.transform.position += new Vector3((-1 + h) * 70 + Random.Range(-30f, 30f), (-1 + v) * 70 + Random.Range(-30f, 30f), 0f);
                    ToppingGrid[index].by3[h, v] = true;
                    if (ToppingGrid[index].Isfull())
                        ToppingGrid[index] = new Grid();
                    cnt++;
                    Topping_AS.Stop();
                    Topping_AS.Play();
                    yield return new WaitForSeconds(0.2f);
                }
            }
            else
            {


                int h = Random.Range(0, 3), v = Random.Range(0, 3);
                if (!ToppingGrid[index].by3[h, v])
                {

                    GameObject tops = Instantiate(Toppings2[index], Toppings_layer.transform);
                    tops.transform.Rotate(new Vector3(0f, 0f, 1f), Random.Range(0f, 360f));
                    tops.transform.position += new Vector3((-1 + h) * 70 + Random.Range(-30f, 30f), (-1 + v) * 70 + Random.Range(-30f, 30f), 0f);
                    ToppingGrid[index].by3[h, v] = true;
                    if (ToppingGrid[index].Isfull())
                        ToppingGrid[index] = new Grid();
                    
                    cnt++;

                    Topping_AS.Stop();
                    Topping_AS.Play();
                    yield return new WaitForSeconds(0.2f);
                }
            }
        }
    }
    public void Topping_Initialize()
    {
        for (int i = 0; i < ToppingGrid.Length; i++)
        {
            ToppingGrid[i] = new Grid();
        }
    }

    public void Go_Trash()
    {
        for (int i = 0; i < Dough_layer.transform.childCount; i++)
            Destroy(Dough_layer.transform.GetChild(i).gameObject);
        for (int i = 0; i < Sauce_layer.transform.childCount; i++)
            Destroy(Sauce_layer.transform.GetChild(i).gameObject);
        for (int i = 0; i < BaseCheese_layer.transform.childCount; i++)
            Destroy(BaseCheese_layer.transform.GetChild(i).gameObject);
        for (int i = 0; i < Toppings_layer.transform.childCount; i++)
            Destroy(Toppings_layer.transform.GetChild(i).gameObject);
        for (int i = 0; i < ToppingCheese_layer.transform.childCount; i++)
            Destroy(ToppingCheese_layer.transform.GetChild(i).gameObject);
        Initialize();
    }

    private void Awake()
    {
        GM = GMGO.GetComponent<GameManager>();
    }
    // Start is called before the first frame update
    void Start()
    {
        Initialize();

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
