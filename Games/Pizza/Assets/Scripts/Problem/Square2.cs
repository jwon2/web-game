﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Square2 : MonoBehaviour
{
    public GameObject ProblemGO;

    public Text Base_Left, Base_Bottom, Up_Left, Right_Bottom, Base_area, Right_area, Up_area, RightUp_area;
    public Text Sum;

    public GameObject BaseLT, BaseLB, BaseRB, BaseRT;

    public GameObject Left_Button, Bottom_Button;
    private Square2_Left SL;
    private Square2_Bottom SB;
    

    // Start is called before the first frame update
    void Start()
    {
        SL = Left_Button.GetComponent<Square2_Left>();
        SB = Bottom_Button.GetComponent<Square2_Bottom>();
    }

    // Update is called once per frame
    void Update()
    {
        Sum.text = "총 넓이 :\n" + Problem.KMGtoSTR(2 * ProblemGO.GetComponent<Problem3>().edge) + ((SL.state + SB.state==0)? "":(SL.state + SB.state).ToString() + Problem.KMGtoSTR(ProblemGO.GetComponent<Problem3>().edge)) + ((SL.state * SB.state==0)?"": (SL.state * SB.state).ToString());
        Base_Left.text = Problem.wingToStr(ProblemGO.GetComponent<Problem3>().edge);
        Base_Bottom.text = Problem.wingToStr(ProblemGO.GetComponent<Problem3>().edge);
        Base_area.text = Problem.wingToStr(ProblemGO.GetComponent<Problem3>().edge+ ProblemGO.GetComponent<Problem3>().edge);
        if (SL.state == 0)
        {
            Up_Left.gameObject.SetActive(false);
            Up_area.gameObject.SetActive(false);
        }
        else
        {
            Up_Left.gameObject.SetActive(true);
            Up_area.gameObject.SetActive(true);
            RightUp_area.gameObject.SetActive(true);
        }
        if (SB.state == 0)
        {
            Right_Bottom.gameObject.SetActive(false);
            Right_area.gameObject.SetActive(false);
        }
        else
        {
            Right_Bottom.gameObject.SetActive(true);
            Right_area.gameObject.SetActive(true);
            RightUp_area.gameObject.SetActive(true);
        }
        if(SL.state ==0 || SB.state == 0)
        {
            RightUp_area.gameObject.SetActive(false);
        }
        Up_Left.text = SL.state.ToString();
        Right_Bottom.text = SB.state.ToString();

        Up_area.text = Problem.wingToStr(ProblemGO.GetComponent<Problem3>().edge,SL.state);
        Right_area.text = Problem.wingToStr(ProblemGO.GetComponent<Problem3>().edge,SB.state);
        RightUp_area.text = (SL.state * SB.state).ToString();

        Base_Left.transform.position = new Vector3(BaseLT.transform.position.x - 20f, (BaseLT.transform.position.y + BaseLB.transform.position.y) / 2f, 0f);
        Base_Bottom.transform.position = new Vector3((BaseLB.transform.position.x + BaseRB.transform.position.x) / 2f, BaseRB.transform.position.y - 20f, 0f);
        Up_Left.transform.position = new Vector3(BaseLT.transform.position.x - 20f, (BaseLT.transform.position.y + Left_Button.transform.position.y) / 2f, 0f);
        Right_Bottom.transform.position = new Vector3((Bottom_Button.transform.position.x + BaseRB.transform.position.x) / 2f, BaseRB.transform.position.y - 20f, 0f);
        Base_area.transform.position = new Vector3((BaseLB.transform.position.x + BaseRB.transform.position.x) / 2f, (BaseLT.transform.position.y + BaseLB.transform.position.y) / 2f, 0f);
        Right_area.transform.position = new Vector3((Bottom_Button.transform.position.x + BaseRB.transform.position.x) / 2f, (BaseLT.transform.position.y + BaseLB.transform.position.y) / 2f, 0f);
        Up_area.transform.position = new Vector3((BaseLB.transform.position.x + BaseRB.transform.position.x) / 2f, (Left_Button.transform.position.y + BaseLT.transform.position.y) / 2f, 0f);
        RightUp_area.transform.position = new Vector3((Bottom_Button.transform.position.x + BaseRB.transform.position.x) / 2f, (Left_Button.transform.position.y + BaseLT.transform.position.y) / 2f, 0f);
    }

    public void Destroy_This()
    {
        Destroy(ProblemGO.gameObject);
    }
}
