﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Problem_Help : MonoBehaviour
{
    public GameObject HelpWindow;
    public GameObject[] HelpContents;
    public GameObject LeftButton,RightButton;

    int state;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void Help_Open()
    {
        HelpWindow.SetActive(true);
        for(int i = 0; i < HelpContents.Length; i++)
        {
            HelpContents[i].SetActive(false);
        }
        state = 0;
        HelpContents[0].SetActive(true);
        LeftButton.SetActive(false);
        RightButton.SetActive(true);
    }
    public void Left()
    {
        HelpContents[state--].SetActive(false);
        HelpContents[state].SetActive(true);
        if (state == 0)
            LeftButton.SetActive(false);
        else
            LeftButton.SetActive(true);

        if (state == HelpContents.Length - 1)
            RightButton.SetActive(false);
        else
            RightButton.SetActive(true);
    }
    public void Right()
    {
        HelpContents[state++].SetActive(false);
        HelpContents[state].SetActive(true);
        if (state == HelpContents.Length - 1)
            RightButton.SetActive(false);
        else
            RightButton.SetActive(true);

        if (state == 0)
            LeftButton.SetActive(false);
        else
            LeftButton.SetActive(true);
    }
}
