﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Square2_Bottom : MonoBehaviour, IDragHandler
{
    public int state = 0;

    public GameObject RB, Extend_RB;
    public GameObject Right_area, RightUp_area;

    float gap;

    public void OnDrag(PointerEventData eventData)
    {
        for (int i = 0; i <= 9; i++)
        {
            if (Input.mousePosition.x < RB.transform.position.x + (2 * i + 1) / 2f * gap && Input.mousePosition.x > RB.transform.position.x)
            {
                transform.position = new Vector3(Input.mousePosition.x, transform.position.y, transform.position.z);
                Right_area.transform.localScale = new Vector3(1f * i / 10f,1f, 1f);
                RightUp_area.transform.localScale = new Vector3(1f * i / 10f, RightUp_area.transform.localScale.y, 1f);
                state = i;
                break;
            }

        }
    }

    // Start is called before the first frame update
    void Start()
    {
        gap = (Extend_RB.transform.position.x - RB.transform.position.x) / 10f;
    }

    // Update is called once per frame
    void Update()
    {

    }
}
