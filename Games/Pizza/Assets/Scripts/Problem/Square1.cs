﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Square1 : MonoBehaviour
{
    public GameObject Problem;

    public GameObject LT, LB, RT, RB;
    public GameObject Left, Right, Top;

    public GameObject STGO, SLGO, SRGO;
    private Square1_Left SL;
    private Square1_Right SR;
    private Square1_Top ST;

    public Text A, A_C, BA, B_CA, BA_C, B_CA_C, AB, A_CB, AB_C, A_CB_C; 

    // Start is called before the first frame update
    void Start()
    {
        SL = SLGO.GetComponent<Square1_Left>();
        SR = SRGO.GetComponent<Square1_Right>();
        ST = STGO.GetComponent<Square1_Top>();
    }

    // Update is called once per frame
    void Update()
    {
        A.text = (ST.state / 10f).ToString("0.#");
        A_C.text = ((10-ST.state) / 10f).ToString("0.#");
        BA.text = (SL.state / 10f).ToString("0.#");
        B_CA.text = ((10-SL.state) / 10f).ToString("0.#");
        BA_C.text = (SR.state / 10f).ToString("0.#");
        B_CA_C.text = ((10 - SR.state) / 10f).ToString("0.#");
        AB.text = ((ST.state / 10f) * (SL.state / 10f)).ToString("0.##");
        A_CB.text = (((10-ST.state) / 10f) * (SR.state / 10f)).ToString("0.##");
        AB_C.text = ((ST.state / 10f) * ((10-SL.state) / 10f)).ToString("0.##");
        A_CB_C.text = (((10 - ST.state) / 10f) * ((10-SR.state) / 10f)).ToString("0.##");

        A.transform.position = new Vector3((LT.transform.position.x + Top.transform.position.x) / 2f, LT.transform.position.y + 20f, 0f);
        A_C.transform.position = new Vector3((RT.transform.position.x + Top.transform.position.x) / 2f, LT.transform.position.y + 20f, 0f);
        BA.transform.position = new Vector3(LT.transform.position.x - 20f, (LT.transform.position.y + Left.transform.position.y) / 2f, 0f);
        B_CA.transform.position = new Vector3(LT.transform.position.x - 20f, (LB.transform.position.y + Left.transform.position.y) / 2f, 0f);
        BA_C.transform.position = new Vector3(RT.transform.position.x + 20f, (LT.transform.position.y + Right.transform.position.y) / 2f, 0f);
        B_CA_C.transform.position = new Vector3(RT.transform.position.x + 20f, (LB.transform.position.y + Right.transform.position.y) / 2f, 0f);
        AB.transform.position = new Vector3((LT.transform.position.x + Top.transform.position.x) / 2f, (LT.transform.position.y + Left.transform.position.y) / 2f, 0f);
        A_CB.transform.position = new Vector3((RT.transform.position.x + Top.transform.position.x) / 2f, (LT.transform.position.y + Right.transform.position.y) / 2f, 0f);
        AB_C.transform.position = new Vector3((LT.transform.position.x + Top.transform.position.x) / 2f, (LB.transform.position.y + Left.transform.position.y) / 2f, 0f);
        A_CB_C.transform.position = new Vector3((RT.transform.position.x + Top.transform.position.x) / 2f, (LB.transform.position.y + Right.transform.position.y) / 2f, 0f);
    }

    public void Destroy_This()
    {
        Destroy(Problem.gameObject);
    }
}
