﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Problem2 : MonoBehaviour
{
    public Text Problem;
    public int ansIdx;

    public int this_num;
    public GameObject[] Ingrediants;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Click_Button(string index)
    {
        if(int.Parse(index) == ansIdx)
        {
            GameObject.Find("GameManager").GetComponent<GameManager>().Answer_AS.Play();
            GameObject.Find("GameManager").GetComponent<GameManager>().addButtons[this_num].GetComponent<Add_Ingrediant>().Get();
            Destroy(this.gameObject);
        }
        else
        {
            GameObject.Find("GameManager").GetComponent<GameManager>().Wrong_AS.Play();
            Destroy(this.gameObject);
        }
    }

    public void Destroy_This()
    {
        Destroy(gameObject);
    }
}
