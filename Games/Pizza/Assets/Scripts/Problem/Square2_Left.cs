﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Square2_Left : MonoBehaviour, IDragHandler
{
    public int state = 0;

    public GameObject LT, Extend_LT;
    public GameObject Up_area, RightUp_area;

    float gap;

    public void OnDrag(PointerEventData eventData)
    {
        for (int i = 0; i <= 9; i++)
        {
            if (Input.mousePosition.y < LT.transform.position.y + (2 * i + 1) / 2f * gap && Input.mousePosition.y >LT.transform.position.y)
            {
                transform.position = new Vector3(transform.position.x, Input.mousePosition.y, transform.position.z);
                Up_area.transform.localScale = new Vector3(1f, 1f * i/10f, 1f);
                RightUp_area.transform.localScale = new Vector3(RightUp_area.transform.localScale.x, 1f * i / 10f, 1f);
                state = i;
                break;
            }

        }
    }

    // Start is called before the first frame update
    void Start()
    {
        gap = (Extend_LT.transform.position.y - LT.transform.position.y) / 10f;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
