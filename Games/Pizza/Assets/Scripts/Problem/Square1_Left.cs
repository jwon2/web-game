﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Square1_Left : MonoBehaviour, IDragHandler
{
    public int state = 5;

    public GameObject LT, LB;
    public GameObject Left, Centor;
    public GameObject Top;
    public GameObject Line;
    float gap;

    public void OnDrag(PointerEventData eventData)
    {
        if (Input.mousePosition.y > LB.transform.position.y && Input.mousePosition.y < LT.transform.position.y)
            transform.position = new Vector3(transform.position.x, Input.mousePosition.y, transform.position.z);
        for (int i = 1; i <= 9; i++)
        {
            if (Input.mousePosition.y < LB.transform.position.y + (2 * i + 1) / 2f * gap)
            {
                Left.transform.position = new Vector3(Left.transform.position.x, LB.transform.position.y + i * gap, Left.transform.position.z);
                Centor.transform.position = new Vector3(Top.transform.position.x, LB.transform.position.y + i * gap, Centor.transform.position.z);
                Line.transform.position = new Vector3(Line.transform.position.x, LB.transform.position.y + i * gap, Line.transform.position.z);
                state = 10-i;
                break;
            }

        }
    }
    // Start is called before the first frame update
    void Start()
    {
        gap = (LT.transform.position.y - LB.transform.position.y) / 10f;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
