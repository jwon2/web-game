﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Help_Display : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    string[] str = {
        "A가 일어날 확률",
        "A가 일어나지 않을 확률",
        "A가 일어났을 때 B가 일어날 확률",
        "A가 일어났을 때 B가 일어나지 않을 확률",
        "A가 일어나지 않았을 때 B가 일어날 확률",
        "A가 일어나지 않았을 때 B가 일어나지 않을 확률",
        "A가 일어나고 B도 일어날 확률",
        "A가 일어나고 B가 일어나지 않을 확률",
        "A가 일어나지 않고 B가 일어날 확률",
        "A가 일어나지 않고 B도 일어나지 않을 확률",
        "B가 일어날 확률",
        "B가 일어나지 않을 확률"
    };

    public Text ExplainText;

    public void OnPointerEnter(PointerEventData eventData)
    {
        ExplainText.text = str[int.Parse(name)];
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        ExplainText.text = "";
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
