﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Square1_Top : MonoBehaviour, IDragHandler
{
    public int state = 5;
    public GameObject LT, RT;
    public GameObject Top, Bottom;
    public GameObject Left_Line, Right_Line;
    public GameObject Left_Centor, Right_Centor;
    float gap;

    public void OnDrag(PointerEventData eventData)
    {
        if(Input.mousePosition.x>LT.transform.position.x && Input.mousePosition.x < RT.transform.position.x)
            transform.position = new Vector3( Input.mousePosition.x , transform.position.y, transform.position.z);
        for(int i = 1; i <= 9; i++)
        {
            if(Input.mousePosition.x< LT.transform.position.x + (2*i + 1)/2f * gap)
            {
                Top.transform.position = new Vector3(LT.transform.position.x + i * gap, Top.transform.position.y, Top.transform.position.z);
                Bottom.transform.position = new Vector3(LT.transform.position.x + i * gap, Bottom.transform.position.y, Bottom.transform.position.z);

                Left_Line.transform.localScale = new Vector3(1f * i / 10f, 1f,1f);
                Left_Centor.transform.position = new Vector3(LT.transform.position.x + i * gap, Left_Centor.transform.position.y, Left_Centor.transform.position.z);

                Right_Line.transform.localScale = new Vector3(1f * (10-i) / 10f, 1f, 1f);
                Right_Centor.transform.position = new Vector3(LT.transform.position.x + i * gap, Right_Centor.transform.position.y, Right_Centor.transform.position.z);
                state = i;
                break;
            }

        }
    }

    // Start is called before the first frame update
    void Start()
    {
        gap = (RT.transform.position.x - LT.transform.position.x)/10f;
        Left_Line.transform.localScale = new Vector3(1f * 0.5f, 1f, 1f);
        Right_Line.transform.localScale = new Vector3(1f * 0.5f , 1f, 1f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
