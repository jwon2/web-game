﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Customer : MonoBehaviour
{
    /* 캐싱 */
    public GameObject[] Order_pos = new GameObject[6];
    public GameObject[] Character_Prefab = new GameObject[4];

    public GameObject[] Ingredient_Prefabs = new GameObject[8];
    public GameObject Order;

    public GameObject gauge;
    public GameObject gauge_fill;

    public GameObject Happy;
    public GameObject Angry;

    public AudioSource[] Order_AS;
    public AudioSource[] Happy_AS;
    public AudioSource[] Angry_AS;


    /* 값 */
    int Character;
    float Init_time;
    public float time;
    int ToppingNum;
    public int[] Ingredient;
    int index;

    public void Set_Customer(int Character, float Init_time, int ToppingNum, int[] Ingredient, GameObject pos,int index)
    {
        this.Character = Character;
        Character_Prefab[Character].SetActive(true);
        this.Init_time = Init_time;
        time = this.Init_time;
        this.ToppingNum = ToppingNum;
        this.Ingredient = Ingredient;
        transform.position = pos.transform.position;
        this.index = index;

        StartCoroutine(Set_Menu_Delay());
    }
    IEnumerator Set_Menu_Delay()
    {
        yield return new WaitForSeconds(0.4f);
        Set_Menu();
    }

    void Set_Menu()
    {
        int cnt = 0;
        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < Ingredient[i]; j++)
            {
                GameObject newOrder = Instantiate(Ingredient_Prefabs[i], Order.transform);
                newOrder.transform.position = Order_pos[cnt++].transform.position;
                newOrder.transform.localScale *= 0.6f;
            }
        }
    }



    private void Awake()
    {
        StartCoroutine(Waiting());
        Order.GetComponent<Animator>().SetTrigger("IsFirst");
        Order_AS[Random.Range(0, 2)].Play();
    }

    IEnumerator Waiting()
    {
        float timeScale = 0.1f;

        yield return new WaitForSeconds(timeScale);
        time -= timeScale;
        if (time > 0f)
        {
            StartCoroutine(Waiting());
        }
        else
        {
            // 대기시간 끝!
            GameObject.Find("GameManager").GetComponent<GameManager>().Sell_InCorrect_Pizza(index);
        }

    }




    // Update is called once per frame
    void Update()
    {
        gauge_fill.GetComponent<Image>().fillAmount = time / Init_time;
    }
}
