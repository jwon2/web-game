﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Receipt : MonoBehaviour
{
    public GameObject[] Order_pos = new GameObject[6];
    public GameObject[] Ingredient_Prefabs = new GameObject[8];
    public GameObject Order;
    public GameObject Angry_stamp;

    public GameObject gauge;
    public GameObject gauge_fill;

    float Init_time;
    float time;
    int ToppingNum;
    int[] Ingredient;
    int index;

    public void Set_Receipt(float Init_time, int ToppingNum, int[] Ingredient, GameObject pos, int index)
    {
        this.Init_time = Init_time;
        time = this.Init_time;
        this.ToppingNum = ToppingNum;
        this.Ingredient = Ingredient;
        transform.position = pos.transform.position;
        this.index = index;

        Set_Menu();
    }

    void Set_Menu()
    {
        int cnt = 0;
        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < Ingredient[i]; j++)
            {
                GameObject newOrder = Instantiate(Ingredient_Prefabs[i], Order.transform);
                newOrder.transform.position = Order_pos[cnt++].transform.position;
                newOrder.transform.localScale *= 0.3f;
            }
        }
    }
    
    

    // Update is called once per frame
    void Update()
    {
        gauge_fill.GetComponent<Image>().fillAmount = GameObject.Find("GameManager").GetComponent<GameManager>().Customers[index].GetComponent<Customer>().time / Init_time;
    }
}
