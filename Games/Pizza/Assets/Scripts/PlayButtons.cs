﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayButtons : MonoBehaviour
{

    public GameObject SoundOnButton, SoundOffButton;
    // Start is called before the first frame update
    void Start()
    {
        if (!PlayerPrefs.HasKey("Sound"))
        {
            SoundOn();
        }
        else
        {
            if (PlayerPrefs.GetFloat("Sound") == 1f)
            {
                SoundOnButton.SetActive(false);
                SoundOffButton.SetActive(true);
                AudioListener.volume = 1f;
            }
            else
            {
                SoundOnButton.SetActive(true);
                SoundOffButton.SetActive(false);
                AudioListener.volume = 0f;
            }
        }
    }
    public void SoundOn()
    {
        PlayerPrefs.SetFloat("Sound", 1);
        AudioListener.volume = 1f;
        SoundOnButton.SetActive(false);
        SoundOffButton.SetActive(true);
    }
    public void SoundOff()
    {
        PlayerPrefs.SetFloat("Sound", 0);
        AudioListener.volume = 0f;
        SoundOnButton.SetActive(true);
        SoundOffButton.SetActive(false);
    }
    public void Go_Title()
    {
        SceneManager.LoadScene("Title");
    }
    public void RePlay()
    {
        SceneManager.LoadScene("Play");
    }
    public void GameClose()
    {

    }
}
