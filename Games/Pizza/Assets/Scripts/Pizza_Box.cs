﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Pizza_Box : MonoBehaviour, IPointerClickHandler, IPointerDownHandler, IPointerEnterHandler, IDragHandler, IPointerExitHandler, IPointerUpHandler
{
    GameObject GMGO;
    private GameManager GM;

    public int[] topping;

    public void OnDrag(PointerEventData eventData)
    {
        transform.position = Input.mousePosition;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
    }

    public void OnPointerDown(PointerEventData eventData)
    {
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
    }

    public void OnPointerExit(PointerEventData eventData)
    {
    }

    public void OnPointerUp(PointerEventData eventData)
    {
       
        if (GM.Customers[0]!=null && (Vector3.Distance(GM.Customers[0].transform.position , transform.position ) < 100f|| Vector3.Distance(GM.Customers[0].transform.position+new Vector3(0f,250f,0f), transform.position) < 100f) && GM.Customers[0].GetComponent<Customer>().Order.activeSelf)
        {
            bool flag = true;
            for(int i = 0; i < 8; i++)
            {
                if (topping[i] != GM.Customers[0].GetComponent<Customer>().Ingredient[i])
                    flag = false;
            }
            if (flag)
            {
                GM.Sell_Correct_Pizza(0);

            }
            else
            {
                GM.Sell_InCorrect_Pizza(0);
            }

            Destroy(gameObject);
        }
        else if (GM.Customers[1] != null && (Vector3.Distance(GM.Customers[1].transform.position, transform.position) < 100f || Vector3.Distance(GM.Customers[1].transform.position + new Vector3(0f, 250f, 0f), transform.position) < 100f) && GM.Customers[1].GetComponent<Customer>().Order.activeSelf)
        {
            bool flag = true;
            for (int i = 0; i < 8; i++)
            {
                if (topping[i] != GM.Customers[1].GetComponent<Customer>().Ingredient[i])
                    flag = false;
            }
            if (flag)
            {
                GM.Sell_Correct_Pizza(1);

            }
            else
            {
                GM.Sell_InCorrect_Pizza(1);
            }
            Destroy(gameObject);
        }
        else if (GM.Customers[2] != null && (Vector3.Distance(GM.Customers[2].transform.position, transform.position) < 100f || Vector3.Distance(GM.Customers[2].transform.position + new Vector3(0f, 250f, 0f), transform.position) < 100f) && GM.Customers[2].GetComponent<Customer>().Order.activeSelf)
        {
            bool flag = true;
            for (int i = 0; i < 8; i++)
            {
                if (topping[i] != GM.Customers[2].GetComponent<Customer>().Ingredient[i])
                    flag = false;
            }
            if (flag)
            {
                GM.Sell_Correct_Pizza(2);

            }
            else
            {
                GM.Sell_InCorrect_Pizza(2);
            }
            Destroy(gameObject);
        }
        else
        {
            transform.position = GM.Pizza_p.transform.position;
        }
    }

    public void Set_Info(int[] topping)
    {
        this.topping = topping;
    }
    private void Awake()
    {
        GMGO = GameObject.Find("GameManager");
        GM = GMGO.GetComponent<GameManager>();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
