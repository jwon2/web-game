﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    // 캐싱

    public GameObject Counter;
    public GameObject Kitchen;
    public GameObject UI_Counter;
    public GameObject UI_Kitchen;

    public GameObject OvenWindow;

    public GameObject Customer_Prefab;
    public GameObject Receipt_Prefab;
    public GameObject Pizza_Prefab;

    public GameObject Receipt;
    public GameObject Customers_p;
    public GameObject Pizza_p;

    public GameObject Plate;

    public GameObject GameoverWindow;

    public GameObject[] Customer_pos = new GameObject[3];
    public GameObject[] Receipt_pos = new GameObject[3];

    public GameObject[] Ingrediants = new GameObject[8];
    public GameObject[] addButtons = new GameObject[8];

    public Text Money_text;

    public GameObject Go_Kitchen_Button;


    public AudioSource Ovening_AS, Oven_DDing_AS;
    public AudioSource Answer_AS, Wrong_AS;

    public GameObject Money_Prefab_Pos;
    public GameObject LostMoney_Prefab, GetMoney_Prefab;
    
    // 값들

    public int Money;
    public int[] Now_ingredient;

    float time;

    int level; // 10단계까지 구성
    int Able_customer;

    public GameObject[] Customers = new GameObject[3];
    public GameObject[] Receipts = new GameObject[3];


    // 읽기 전용
    readonly int[] level_limit = { 10, 20, 30, 50, 70, 100, 200,350, 500, 800 };
    readonly float[,] level_topping = { { 1, 1, 1, 1, 1, 1, 1 }, { 0, 1, 1, 1, 1, 1, 1 }, { 0.5f, 1, 1, 1, 1, 1, 1 }, { 0.2f, 0.8f, 1, 1, 1, 1, 1 }, { 0.1f, 0.4f, 0.8f, 1, 1, 1, 1 },
        { 0.1f, 0.2f, 0.4f, 0.8f, 1, 1, 1 }, { 0.1f, 0.2f, 0.4f, 0.8f, 0.9f, 1, 1 }, { 0.1f, 0.2f, 0.3f, 0.5f, 0.7f, 0.9f, 1 }, { 0.05f, 0.1f, 0.2f, 0.4f, 0.6f, 0.8f, 1 }, { 0.05f, 0.1f, 0.2f, 0.4f, 0.6f, 0.8f, 1 } };
    readonly float[] time_level = { 40, 115/3f, 110/3f, 35, 100/3f, 30, 80/3f, 70/3f, 20, 15, 10f };


    public readonly int dough = 200, sauce = 200, cheese = 200;
    public readonly int[] price_topping = { 200, 500, 300, 500, 500, 800, 700, 1000 };
   
     

    // 수치


    private void Awake()
    {
        Money = 10000;
        level = 0;
        Now_ingredient = new int[] {1,1,1,1,1,1,1,1};
        Able_customer = 3;
        time = 0;
    }

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Customer_Coming());
        StartCoroutine(Timer());

    }
    IEnumerator Timer()
    {
        float timeScale = 0.1f;

        yield return new WaitForSeconds(timeScale);
        time += timeScale;
        for (int i = 0; i < 10; i++)
        {
            if (time > level_limit[i])
                level = i;
        }
        StartCoroutine(Timer());
    }

    IEnumerator Customer_Coming()
    {
        for (int i = 0; i < 3; i++)
        {
            if (Customers[i] == null)
            {
                if (Random.value < 0.2f)
                {
                    Customers[i] = Instantiate(Customer_Prefab,Customers_p.transform);
                    int topping_num=Get_topping_num();
                    int[] topping = Get_topping(topping_num);
                    Customers[i].GetComponent<Customer>().Set_Customer(Random.Range(0, 4), time_level[level]*Able_customer, topping_num, topping,Customer_pos[i],i);

                    Receipts[i] = Instantiate(Receipt_Prefab, Receipt.transform);
                    Receipts[i].GetComponent<Receipt>().Set_Receipt(time_level[level]*Able_customer, topping_num, topping, Receipt_pos[i],i);
                        
                    break;
                }
            }
        }

        yield return new WaitForSeconds(1f);
        StartCoroutine(Customer_Coming());
    }

    int Get_topping_num()
    {
        float rnd_value = Random.value;

        for(int i = 0; i < 7; i++)
        {
            if(rnd_value < level_topping[level,i])
            {
                if (i == 0)
                    return 1;
                return i;
            }
        }
        return 0;
    }
    int[] Get_topping(int num)
    {
        int[] result = new int[8];
        

        for (int i = 0; i < num; i++)
        {
            result[Random.Range(0, 8)]++;
        }
        
        return result;
    }
    
    public void Go_Kitchen()
    {
        Kitchen.SetActive(true);
        UI_Kitchen.SetActive(true);
        Counter.SetActive(false);
        UI_Counter.SetActive(false);
        for(int i = 0; i < Receipt.transform.childCount; i++)
        {
            Receipt.transform.GetChild(i).gameObject.SetActive(true);
        }

        Receipt.SetActive(true);
    }
    public void Go_Counter()
    {

        Kitchen.SetActive(false);
        UI_Kitchen.SetActive(false);
        Counter.SetActive(true);
        UI_Counter.SetActive(true);

        Receipt.SetActive(false);
        Customers_p.SetActive(true);
    }

    public void Go_Oven()
    {
        OvenWindow.SetActive(true);
        StartCoroutine(Ovening());
        Ovening_AS.Play();
    }
    IEnumerator Ovening()
    {

        yield return new WaitForSeconds(3f);
        Ovening_AS.Stop();
        Oven_DDing_AS.Play();
        yield return new WaitForSeconds(1f);
        OvenWindow.SetActive(false);
        GameObject newPizza = Instantiate(Pizza_Prefab, Pizza_p.transform);
        newPizza.GetComponent<Pizza_Box>().Set_Info(Plate.GetComponent<Pizza>().Get_Ingrediant());
        Plate.GetComponent<Pizza>().Go_Trash();
        Go_Counter();
    }

    public void Sell_Correct_Pizza(int index)
    {
        Customers[index].GetComponent<Customer>().Happy_AS[Random.Range(0, 2)].Play();
        int profit = 2000;
        for(int i = 0; i < 8; i++)
        {
            profit += price_topping[i] / 2 * Customers[index].GetComponent<Customer>().Ingredient[i];
           
        }
        Get_Money(profit);

        Customers[index].GetComponent<Customer>().Happy.SetActive(true);
        Destroy(Receipts[index].gameObject);
        Destroy(Customers[index].gameObject,1.5f);
    }

    public void Sell_InCorrect_Pizza(int index)
    {
        Customers[index].GetComponent<Customer>().StopAllCoroutines();
        Customers[index].GetComponent<Customer>().Angry_AS[Random.Range(0, 2)].Play();
        Customers[index].GetComponent<Customer>().Order.SetActive(false);
        Customers[index].GetComponent<Customer>().gauge.SetActive(false);
        Customers[index].GetComponent<Customer>().Angry.SetActive(true);
        Receipts[index].GetComponent<Receipt>().gauge.SetActive(false);
        Receipts[index].GetComponent<Receipt>().Order.SetActive(false);
        Receipts[index].GetComponent<Receipt>().Angry_stamp.SetActive(true);
        Able_customer--;
        if (Able_customer == 0)
            GameOver();

    }

    public void Lost_Money(int price)
    {
        Money -= price;

        /*
         돈을 잃는 애니메이션
         */
        GameObject newText = Instantiate(LostMoney_Prefab, Money_Prefab_Pos.transform);
        newText.GetComponent<Text>().text = "-" + price.ToString();
        Destroy(newText, 0.5f);

        Money_text.text = Money.ToString() + " QP";
    }
    public void Get_Money(int price)
    {
        Money += price;

        /*
         돈을 얻는 애니메이션
         */

        GameObject newText = Instantiate(GetMoney_Prefab, Money_Prefab_Pos.transform);
        newText.GetComponent<Text>().text = "+" + price.ToString();
        Destroy(newText, 0.7f);

        Money_text.text = Money.ToString() + " QP";
    }
    public void Lost_Ingrediant(int index)
    {
        Money -= price_topping[index];
        Now_ingredient[index]--;
        Ingrediants[index].GetComponent<Ingrediant>().Change_Bucket();
    }

    public void GameOver()
    {
        GameoverWindow.SetActive(true);
        GetComponent<RankManager>().PutAndGetRankInfo(Money, time);
    }

    public void Go_Trash_Pizza()
    {
        Destroy(Pizza_p.transform.GetChild(0).gameObject);
    }
    // Update is called once per frame
    void Update()
    {
        if (Pizza_p.transform.childCount == 0)
        {
            Go_Kitchen_Button.GetComponent<Button>().interactable = true;
        }
        else
        {
            Go_Kitchen_Button.GetComponent<Button>().interactable = false;
        }
    }
}

