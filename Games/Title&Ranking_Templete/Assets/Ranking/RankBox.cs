﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RankBox : MonoBehaviour {

    // 등수, 점수, 시간, 닉네임, 레벨
    public GameObject N, S, T, NN, L;
    public GameObject NumBackGroundPar;

    public void SetRankBox(int num, int score, float time, string nickname, int level)
    {
        for (int i = 0; i < NumBackGroundPar.transform.childCount; i++) NumBackGroundPar.transform.GetChild(i).gameObject.SetActive(false);
        if (num == 1) NumBackGroundPar.transform.GetChild(0).gameObject.SetActive(true);
        else if (num == 2) NumBackGroundPar.transform.GetChild(1).gameObject.SetActive(true);
        else if (num == 3) NumBackGroundPar.transform.GetChild(2).gameObject.SetActive(true);
        else NumBackGroundPar.transform.GetChild(3).gameObject.SetActive(true);

        if (num == 0) N.SetActive(false);
        else
        {
            if (num < 100) SetText(N, num.ToString());
            else SetText(N, "99+");
            N.SetActive(true);
        }

        if (nickname == null)
        {
            SetText(N, "");
            SetText(S, "");
            SetText(T, "");
            SetText(NN, "");
            SetText(L, "");
        } else {
            SetText(S, score.ToString());
            SetText(T, time.ToString("##0.00"));
            if (nickname.Length <= 10)
                SetText(NN, nickname);
            else
                SetText(NN, nickname.Substring(0, 9) + "...");
            SetText(L, "lv" + level.ToString());
        }
    }

    void SetText(GameObject GO, string str) {
        GO.transform.GetComponent<Text>().text = str;
    }
}
