﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fever : MonoBehaviour
{

    GameObject[] Multiple = new GameObject[9];
    // Start is called before the first frame update
    private void Awake()
    {
        for (int i = 0; i < 9; i++)
            Multiple[i] = this.transform.GetChild(i).gameObject;
    }
    public void Set_Fever(int n)
    {
        for (int i = 0; i < 9; i++)
            Multiple[i].SetActive(false);
        Multiple[n].SetActive(true);
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
