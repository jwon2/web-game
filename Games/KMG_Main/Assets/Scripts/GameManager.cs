﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    // 캐싱하는 오브젝트
    public GameObject PlayerGO;
    private Player playercs;
    public GameObject Shield;

    public GameObject Effect;


    private Create_Problem create_problemcs;

    public GameObject[] Road = new GameObject[2];

    public GameObject[] Trees = new GameObject[3];
    public GameObject Road_Lamp;


    public GameObject ItemShield,ItemStar,ItmeTime;


    public GameObject GameFinish;
    public GameObject MyLoading;
    public GameObject GameOverRankBox;
    public GameObject GameOverWindow;
    public GameObject RedWindow;
    public GameObject DarkWindow;
    public GameObject PoliceCar;

    public AudioSource PoliceSound;
    public AudioSource FeverSound;

    //UI
    //Score
    public GameObject ScoreGO;
    public GameObject Score_Display_100000, Score_Display_10000, Score_Display_1000, Score_Display_100, Score_Display_10, Score_Display_1;

    public GameObject Velocity_Display_Hand;
    public GameObject Velocity_Display_100, Velocity_Display_10, Velocity_Display_1;
    public Text Count_Display;
        //Fever
    public GameObject Fever_Display;
    public GameObject Fever_Multiple;
    public GameObject Fever_Bar;
        //Star
    public GameObject Star_Display;
    public GameObject Star_Bar;

        //Slow
    public GameObject Slow_Display;
    public GameObject Slow_Bar;

        //Break;
    public GameObject Break_Display;
    public GameObject Break_Bar;

    //Sound
    public GameObject SoundOn_btn;
    public GameObject SoundOff_btn;

    public AudioSource[] BGMs = new AudioSource[7];


    // 전반적인 게임 관련 수치
    public int Score;
    public int Counts;
    float Play_Time;
        //Fever
    public int Fever;// 0 : default,  1: x2,  2: x3,  3:  x4,  ...... 9: x10
    int Fever_Stack;
    public float Fever_Time;

    public float Star_Time;
    public float Slow_Time;


    int Level;

    public bool isDash;
    public bool isStar;
    public bool isSlow;
    public bool isShield;
    public bool isFever;

    readonly float Velocity_Dash = 50f;
    public readonly float[] Velocity = { 2f, 3f, 4f, 6f, 8f, 10f, 12f, 15f, 20f };
    public readonly float Velocity_Display_weight = 3.6f;
    readonly int[] level_max = { 10, 20, 35, 50, 70, 90, 115, 140, 170 };
    readonly float[,] probability = { { 1f, 1f, 1f }, { 0.8f, 1f, 1f }, { 0.6f, 1f, 1f }, { 0.4f, 0.8f, 1f }, { 0.4f, 0.6f, 1f }, { 0.3f, 0.6f, 1f }, { 0.2f, 0.6f, 1f }, { 0.2f, 0.5f, 1f }, { 0.1f, 0.4f, 1f } };
    readonly int[,] Fever_Data = { { 1, 0 }, { 2, 26 }, { 3, 21 }, { 4,16 }, { 5, 14 }, { 6,12 }, { 7, 10 }, { 8, 8 }, { 9, 6 }, { 10, 4 } };
    readonly float[] line = { -3f, 0f, 3f };

    public void Initialize()
    {
        if (!PlayerPrefs.HasKey("BGM"))
        {
            PlayerPrefs.SetInt("BGM", 0);
            BGMs[PlayerPrefs.GetInt("BGM")].Play();
        }
        else
        {
            BGMs[PlayerPrefs.GetInt("BGM")].Play();
        }
        Play_Time = 0f;
        Score = 0;
        Counts = 0;
        Level = 0;
        Fever = 0;
        Fever_Stack = 0;
        create_problemcs.Make_Problem(new Vector3(0f, 0f, 20f), 0);

        create_problemcs.Make_Problem(new Vector3(0f, 0f, 40f), 0);

        create_problemcs.Make_Problem(new Vector3(0f, 0f, 60f), 0);

        for (int i = 0; i < 5; i++)
        {
            for (int j = 0; j < 2; j++)
            {

                float a = 0f;
                if (j == 1) a = 100f;
                for (int k = 0; k < 3; k++) {
                    Instantiate(Trees[k], new Vector3(-11f, Trees[k].transform.position.y, -50f+i*20f+6.66f*k + a), Trees[k].transform.rotation, Road[j].transform);
                    Instantiate(Trees[k], new Vector3(11f, Trees[k].transform.position.y, -50f+i*20f+6.66f*k + a), Trees[k].transform.rotation, Road[j].transform);
                }
                //Instantiate(Road_Lamp, new Vector3(Random.Range(-8f, -12f), Road_Lamp.transform.position.y, Random.Range(-50f, 50f) + a), Road_Lamp.transform.rotation, Road[j].transform);
                //Instantiate(Road_Lamp, new Vector3(Random.Range(8f, 12f), Road_Lamp.transform.position.y, Random.Range(-50f, 50f) + a), Road_Lamp.transform.rotation, Road[j].transform);
            }
        }
        StartCoroutine(Timer());
    }
    IEnumerator Timer()
    {
        float timescale = 0.001f;
        yield return new WaitForSeconds(timescale);

        Play_Time += timescale;

        StartCoroutine(Timer());
    }
    public void Instantiate_Problem()
    {
        create_problemcs.Make_Problem(new Vector3(0f, 0f, Counts * 20f + 60), get_difficulty());
        float item = Random.value;
        if (item < 0.05f)
        {
            Instantiate(ItemShield, new Vector3(line[Random.Range(0,3)], 1f, Counts * 20f + 65), new Quaternion(0f, 0f, 0f, 1f));
        }
        else if (item < 0.08f)
        {

            Instantiate(ItemStar, new Vector3(line[Random.Range(0, 3)], 1f, Counts * 20f + 65), new Quaternion(0f, 0f, 0f, 1f));
        }
        else if (item < 0.13f)
        {

            Instantiate(ItmeTime, new Vector3(line[Random.Range(0, 3)], 1f, Counts * 20f + 65), new Quaternion(0f, 0f, 0f, 1f));
        }
    }
    int get_difficulty()
    {
        int result=0;
        float Random_Num = Random.value;
        if (Random_Num <= probability[Level, 0])
            result = 0;
        else if (Random_Num <= probability[Level, 1])
            result = 1;
        else
            result = 2;

        return result;
    }

    public void ChangeVelocity()
    {
        playercs.Player_Velocity = Get_StandardVelocity();
    }
    public void ChangeVelocity(string str)
    {
        if(str == "Dash" || str == "Star")
            playercs.Player_Velocity = Velocity_Dash;
;
    }

    public float Get_StandardVelocity()
    {

        return Velocity[Level];
    }
    public void SolveProblem()
    {
        Score += 1 * Fever_Data[Fever, 0] * (Level + 1);
        Set_Score();
        Counts++;

        if (Counts < level_max[0])
            Level = 0;
        else if (Counts < level_max[1])
            Level = 1;
        else if (Counts < level_max[2])
            Level = 2;
        else if (Counts < level_max[3])
            Level = 3;
        else if (Counts < level_max[4])
            Level = 4;
        else if (Counts < level_max[5])
            Level = 5;
        else if (Counts < level_max[6])
            Level = 6;
        else if (Counts < level_max[7])
            Level = 7;
        else
            Level = 8;

        if ((++Fever_Stack) % 10 == 0)
            FeverManager();

        isDash = false;
        if (!isStar)
        {
            ChangeVelocity();
        }Count_Display.text = Counts.ToString() + " 개";

        Instantiate_Problem();

      

    }
    void Set_Score()
    {
        Score_Display_1.GetComponent<Number>().Set_Num((Score / 1) % 10);
        if (Score >= 10)
            Score_Display_10.GetComponent<Number>().Set_Num((Score / 10) % 10);
        if (Score >= 100)
            Score_Display_100.GetComponent<Number>().Set_Num((Score / 100) % 10);
        if (Score >= 1000)
            Score_Display_1000.GetComponent<Number>().Set_Num((Score / 1000) % 10);
        if (Score >= 10000)
            Score_Display_10000.GetComponent<Number>().Set_Num((Score / 10000) % 10);
        if (Score >= 100000)
            Score_Display_100000.GetComponent<Number>().Set_Num((Score / 100000) % 10);
    }

    void FeverManager()
    {
        if (!isFever)
        {
            Fever = 1;
            Fever_Time = Fever_Data[Fever, 1];
            StartCoroutine(Fevering());
        }
        else
        {
            Fever += 1;
            Fever = Mathf.Min(9, Fever);
            Fever_Time += Fever_Data[Fever, 1];
        }

        FeverSound.Play();
    }
    IEnumerator Fevering()
    {
        float timescale = 0.1f;

        isFever = true;
        Fever_Display.SetActive(true);
        Fever_Multiple.GetComponent<Fever>().Set_Fever(Fever-1);

        if (Fever_Time > 30f)
            Fever_Bar.GetComponent<Image>().fillAmount = 1f;
        else
            Fever_Bar.GetComponent<Image>().fillAmount = Fever_Time / 50f;

        yield return new WaitForSeconds(timescale);
        isFever = false;
        Fever_Display.SetActive(false);

        Fever_Time -= timescale;

        if (Fever_Time > 0)
            StartCoroutine(Fevering());
        else
            Fever_Stack = 0;
    }


    public void get_Shield()
    {
        if (!isShield)
        {
            isShield = true;
            Shield.SetActive(true);

            StartCoroutine( Effect.GetComponent<Effect>().Start_eff(0));
        }
        else
        {

        }
    }
    public void Lost_Shield()
    {
        isShield = false;
        Shield.SetActive(false);
    }
    public void get_Star()
    {
        if (!isStar)
        {
            isStar = true;
            Star_Time = 3f;
            StartCoroutine(Star_ing());
            ChangeVelocity("Dash");
        }
        else
        {
            Star_Time = 3f;
        }
        StartCoroutine(Effect.GetComponent<Effect>().Start_eff(1));
    }
    IEnumerator Star_ing()
    {
        isStar = true;
        float timescale = 0.1f;

        Star_Bar.GetComponent<Image>().fillAmount = Star_Time / 3f;

        yield return new WaitForSeconds(timescale);
        Star_Time -= timescale;
        if (Star_Time > 0f)
            StartCoroutine(Star_ing());
    }
    public void get_Time()
    {
        if (!isSlow)
        {
            isSlow = true;
            Slow_Time = 8f;
            StartCoroutine(Slow_ing());
        }
        else
        {

            Slow_Time = 8f;
        }
        StartCoroutine(Effect.GetComponent<Effect>().Start_eff(2));
    }
    IEnumerator Slow_ing()
    {
        isSlow = true;
        float timescale = 0.1f;
        if(!isStar && !isDash)
            playercs.Player_Velocity = 1f;

        Slow_Bar.GetComponent<Image>().fillAmount = Slow_Time / 8f;

        yield return new WaitForSeconds(timescale);

        if(!isStar && !isDash)
            ChangeVelocity();
        Slow_Time -= timescale;
        isSlow = false;
        if (Slow_Time > 0f)
            StartCoroutine(Slow_ing());
    }


    public void GameOver()
    {
        playercs.Player_Velocity = 0f;
        StartCoroutine(Delay());
    }
    bool isRedWindow = false;
    IEnumerator Delay()
    {
        GameOverWindow.SetActive(true);
        StartCoroutine(Police());
        isRedWindow = true;
        StartCoroutine(RedWindowFlash());
        yield return new WaitForSeconds(2.5f);
        DarkWindow.SetActive(true);
        PoliceSound.Stop();
        isRedWindow = false;
        GameFinish.SetActive(true);
        MyLoading.SetActive(true);
        GameOverRankBox.SetActive(false);
        transform.GetComponent<RankManager>().PutAndGetRankInfo(Score, Play_Time);
    }
    IEnumerator Police()
    {

        yield return new WaitForSeconds(0.01f);
        GameOverWindow.transform.Translate(new Vector3(0f, 700f, 0f) * Time.deltaTime);

        if (GameOverWindow.transform.position.y < 0) 
            StartCoroutine(Police());
        else

            StartCoroutine(PoliceCarIE());
    }
    IEnumerator RedWindowFlash()
    {
        RedWindow.SetActive(!RedWindow.activeSelf);
        yield return new WaitForSeconds(0.1f);
        if(isRedWindow)
            StartCoroutine(RedWindowFlash());
    }
    IEnumerator PoliceCarIE()
    {
        for (int i = 0; i < 100; i++)
        {
            PoliceCar.transform.Translate(new Vector3(0f, 700f, 0f) * Time.deltaTime);

            yield return new WaitForSeconds(0.01f);
        }
    }


    // Start is called before the first frame update
    void Awake()
    {
        Time.timeScale = 1f;
        playercs = PlayerGO.GetComponent<Player>();
        Debug.Log("playercs has set");
        create_problemcs = this.GetComponent<Create_Problem>();
        Debug.Log("create_problemcs has set");
    }
    private void Start()
    {
        if (!PlayerPrefs.HasKey("isSoundOn")) PlayerPrefs.SetFloat("isSoundOn", 1f);
        if (PlayerPrefs.GetFloat("isSoundOn") == 0f)
        {
            AudioListener.volume = 0f;
            SoundOff_btn.SetActive(false);
            SoundOn_btn.SetActive(true);
        }
        else
        {
            AudioListener.volume = 1f;
            SoundOff_btn.SetActive(true);
            SoundOn_btn.SetActive(false);
        }
        Initialize();
    }

    public void SoundOff()
    {
        SoundOn_btn.SetActive(true);
        SoundOff_btn.SetActive(false);
        PlayerPrefs.SetFloat("isSoundOn", 0f);
        AudioListener.volume = 0f;
    }

    public void SoundOn()
    {
        SoundOn_btn.SetActive(false);
        SoundOff_btn.SetActive(true);
        PlayerPrefs.SetFloat("isSoundOn", 1f);
        AudioListener.volume = 1f;
    }
    public void BGM_Change()
    {
        if (PlayerPrefs.GetInt("BGM") == 6)
            PlayerPrefs.SetInt("BGM", 0);
        else
            PlayerPrefs.SetInt("BGM", PlayerPrefs.GetInt("BGM") + 1);

        for (int i = 0; i < BGMs.Length; i++)
        {
            BGMs[i].Stop();
        }
        BGMs[PlayerPrefs.GetInt("BGM")].Play();
    }

    // Update is called once per frame
    void Update()
    {

        Road[0].transform.position = new Vector3(Road[0].transform.position.x, Road[0].transform.position.y, (((int)PlayerGO.transform.position.z + 150) / 200) * 200);
        Road[1].transform.position = new Vector3(Road[1].transform.position.x, Road[1].transform.position.y, (((int)PlayerGO.transform.position.z + 50) / 200) * 200 +100);
    }
    public void ToTitle()
    {
        SceneManager.LoadScene("Title");
    }

    public void Restart()
    {
        SceneManager.LoadScene("Play");
    }
}
