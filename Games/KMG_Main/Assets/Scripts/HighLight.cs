﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HighLight : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject HiLi1, HiLi2;
    public GameObject[] Dash = new GameObject[3];
    public GameObject GM;
    GameManager GameMgr;

    private void Awake()
    {
        GameMgr = GM.GetComponent<GameManager>();
    }

    bool is1 = true;

    IEnumerator Hilief()
    {

        if(GameMgr.isDash || GameMgr.isStar)
        {
            for(int i = 0; i < 3; i++)
            {
                Dash[i].SetActive(true);
            }
            if (is1)
            {
                HiLi1.SetActive(true);
                HiLi2.SetActive(false);
                is1 = false;
            }
            else
            {
                HiLi1.SetActive(false);
                HiLi2.SetActive(true);
                is1 = true;
            }

        }
        else
        {
            HiLi1.SetActive(false);
            HiLi2.SetActive(false);
            for (int i = 0; i < 3; i++)
            {
                Dash[i].SetActive(false);
            }
        }

        yield return new WaitForSeconds(0.1f);
        StartCoroutine(Hilief());

    }

    IEnumerator Dashef()
    {
        if (GameMgr.isDash || GameMgr.isStar)
        {
            for (int i = 0; i < 3; i++)
            {
                Dash[i].SetActive(true);
            }
        }
        else
        {
            for (int i = 0; i < 3; i++)
            {
                Dash[i].SetActive(false);
            }
        }
        yield return new WaitForSeconds(0.01f);
        StartCoroutine(Dashef());
    }

    void Start()
    {
        StartCoroutine(Hilief());
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
