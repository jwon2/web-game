﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Title : MonoBehaviour
{

    public GameObject SoundOn_btn;
    public GameObject SoundOff_btn;

    // Start is called before the first frame update
    void Start()
    {
        if (!PlayerPrefs.HasKey("Sound")) PlayerPrefs.SetFloat("Sound", 1f);
        if (PlayerPrefs.GetFloat("Sound") == 0f)
        {
            AudioListener.volume = 0f;
            SoundOff_btn.SetActive(false);
            SoundOn_btn.SetActive(true);
        }
        else
        {
            AudioListener.volume = 1f;
            SoundOff_btn.SetActive(true);
            SoundOn_btn.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SoundOff()
    {
        SoundOn_btn.SetActive(true);
        SoundOff_btn.SetActive(false);
        PlayerPrefs.SetFloat("Sound", 0f);
        AudioListener.volume = 0f;
    }

    public void SoundOn()
    {
        SoundOn_btn.SetActive(false);
        SoundOff_btn.SetActive(true);
        PlayerPrefs.SetFloat("Sound", 1f);
        AudioListener.volume = 1f;
    }

    public void GameClose()
    {
        //  Application.OpenURL(.user.closeUrl);
    }
}
