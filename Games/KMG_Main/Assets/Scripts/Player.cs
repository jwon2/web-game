﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{

    public float Player_Velocity;


    private readonly float horizental_velocity_weight = 8f;

    readonly float Velocity_weight = 10f;

    public int State; // 0 : 왼쪽, 1 : 가운데, 2 : 오른쪽
    private readonly float[] State_x = { -3f, 0f, 3f }; // 왼쪽-가운데-오른쪽
    public int location;

    public GameObject Main_Camera;

    public GameObject Car_P;

    public GameObject Car_C, Car_L, Car_R;

    public GameObject GM;
    GameManager GameMgr;

    public AudioSource Break_Sound;
    bool break_sound_isplayed;
    public AudioSource accel_Sound;
    bool accel_sound_isplayed;

    float break_weight;
    public float break_gauge;

    // Start is called before the first frame update
    private void Awake()
    {

        GameMgr = GM.GetComponent<GameManager>();
    }
    void Start()
    {
        Initialize();
    }
    public void Initialize()
    {
        State = 1;
        GameMgr.ChangeVelocity();
        GameMgr.isDash = false;

        break_gauge = 1f;

        StartCoroutine(Gauge_bar());
    }

    // Update is called once per frame
    void Update()
    {


        if (Input.GetKeyDown(KeyCode.Space) && !GameMgr.isDash && !GameMgr.isStar)  // 최고속도로 Dash
        {
            GameMgr.isDash = true;
            GameMgr.ChangeVelocity("Dash");
            accel_Sound.Play();
        }
        if (Input.GetKeyDown(KeyCode.LeftArrow ) && Player_Velocity != 0/* && !GameMgr.isDash*/) // 왼쪽으로 이동.
        {
            if (State != 0)
            {
                State--;
            }
        }
        if (Input.GetKeyDown(KeyCode.RightArrow) && Player_Velocity != 0/* && !GameMgr.isDash*/)
        {
            if (State != 2)
            {
                State++;
            }

        }
        break_weight = 1f;
        if (Input.GetKey(KeyCode.DownArrow))
        {
            if (break_gauge > 0f)
            {
                break_weight = 0.4f;
                break_gauge -= 0.005f;
                if (!Break_Sound.isPlaying && !break_sound_isplayed)
                {
                    Break_Sound.Play();
                    break_sound_isplayed = true;
                }
            }
            
        }
        else
        {
            break_gauge += 0.001f;
            Break_Sound.Stop();
            break_sound_isplayed = false;
            if (break_gauge > 1f) break_gauge = 1f;
        }
        GameMgr.Break_Bar.GetComponent<Image>().fillAmount = break_gauge;

        if (State == 0)
        {
            if (Car_P.transform.position.x > State_x[0])
            {
                if (Car_P.transform.position.x - State_x[0] < 0.2f * GameMgr.Get_StandardVelocity() / GameMgr.Velocity[0])
                {
                    Car_P.transform.position = new Vector3(State_x[0], Car_P.transform.position.y, Car_P.transform.position.z);
                }
                else
                {
                    Car_P.transform.Translate(new Vector3(-1f, 0f, 0f) * GameMgr.Get_StandardVelocity() * horizental_velocity_weight * Time.deltaTime);
                }
            }
            if (Car_P.transform.position.x < State_x[0])
                Car_P.transform.position = new Vector3(State_x[0], Car_P.transform.position.y, Car_P.transform.position.z);
        }
        else if (State == 1)
        {
            if (Car_P.transform.position.x > State_x[1])
            {
                if (Car_P.transform.position.x - State_x[1] < 0.2f * GameMgr.Get_StandardVelocity() / GameMgr.Velocity[0])
                {
                    Car_P.transform.position = new Vector3(State_x[1], Car_P.transform.position.y, Car_P.transform.position.z);
                }
                else
                {
                    Car_P.transform.Translate(new Vector3(-1f, 0f, 0f) * GameMgr.Get_StandardVelocity() * horizental_velocity_weight * Time.deltaTime);
                }
            }
            else if (Car_P.transform.position.x < State_x[1])
            {
                if (State_x[1] - Car_P.transform.position.x < 0.2f * GameMgr.Get_StandardVelocity() / GameMgr.Velocity[0])
                {
                    Car_P.transform.position = new Vector3(State_x[1], Car_P.transform.position.y, Car_P.transform.position.z);
                }
                else
                {
                    Car_P.transform.Translate(new Vector3(1f, 0f, 0f) * GameMgr.Get_StandardVelocity() * horizental_velocity_weight * Time.deltaTime);
                }
            }
        }
        else if (State == 2)
        {
            if (Car_P.transform.position.x < State_x[2])
            {
                if (State_x[2] - transform.position.x < 0.2f * GameMgr.Get_StandardVelocity() / GameMgr.Velocity[0])
                {
                    Car_P.transform.position = new Vector3(State_x[2], Car_P.transform.position.y, Car_P.transform.position.z);
                }
                else
                {
                    Car_P.transform.Translate(new Vector3(1f, 0f, 0f) * GameMgr.Get_StandardVelocity() * horizental_velocity_weight * Time.deltaTime);
                }
            }
            if (Car_P.transform.position.x > State_x[2])
                Car_P.transform.position = new Vector3(State_x[2], Car_P.transform.position.y, Car_P.transform.position.z);

        }
        else
            Debug.Log("State value Error!!");


        Car_L.SetActive(false);
        Car_C.SetActive(false);
        Car_R.SetActive(false);
        if (Car_P.transform.position.x < -1.5f)
        {
            location = 0;
            Car_L.SetActive(true);
        }
        else if (Car_P.transform.position.x < 1.5f)
        {
            location = 1;
            Car_C.SetActive(true);
        }
        else
        {
            location = 2;
            Car_R.SetActive(true);
        }
        
        if(GameMgr.isStar)
        {
            if (!accel_sound_isplayed)
            {
                accel_sound_isplayed = true;
                accel_Sound.Play();
            }

        }
        else if(!GameMgr.isDash)
        {
            accel_sound_isplayed = false;

        }
        


        transform.Translate(new Vector3(0f, 0f, Player_Velocity * break_weight) * Time.deltaTime);

    }


    float past_velocity=0f;

    IEnumerator Gauge_bar()
    {
        past_velocity = past_velocity + (Player_Velocity * break_weight - past_velocity) * 0.1f;
        GameMgr.Velocity_Display_Hand.transform.localRotation = Quaternion.Euler(new Vector3(0f, 0f, +145 - 280f * (past_velocity) / 50f));
        GameMgr.Velocity_Display_100.GetComponent<Number>().Set_Num((int)(past_velocity*Velocity_weight / 100)%10);

        GameMgr.Velocity_Display_10.GetComponent<Number>().Set_Num((int)((past_velocity * Velocity_weight / 10)%10));

        GameMgr.Velocity_Display_1.GetComponent<Number>().Set_Num((int)(past_velocity * Velocity_weight % 10));

        yield return new WaitForSeconds(0.01f);
        StartCoroutine(Gauge_bar());
    }
}
