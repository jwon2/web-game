﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarCollision : MonoBehaviour
{

    public GameObject GM;
    GameManager GameMgr;

    public AudioSource Correct_Sound;
    public AudioSource Get_Item_Sound;
    public AudioSource Shield_Hit_Sound;

    public GameObject PlayerGO;
    Player Playercs;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "0" || other.tag == "1" || other.tag == "2")
        {
            if (other.tag == Playercs.location.ToString())
            {
                Destroy(other.transform.parent.gameObject);
                GameMgr.SolveProblem();

                if (GameMgr.isStar)
                {
                    if (GameMgr.Star_Time <= 0f)
                    {
                        GameMgr.isStar = false;
                        GameMgr.ChangeVelocity();
                    }
                }
                
                Correct_Sound.Play();

            }
            else
            {

                if (GameMgr.isStar || GameMgr.isShield)
                {

                    Destroy(other.transform.parent.gameObject);

                    GameMgr.SolveProblem();
                    if (GameMgr.isShield && !GameMgr.isStar)
                    {
                        GameMgr.Lost_Shield();
                        Shield_Hit_Sound.Play();
                    }
                    else
                    {
                        Correct_Sound.Play();
                    }
                    if (GameMgr.isStar)
                    {
                        if (GameMgr.Star_Time <= 0f)
                        {
                            GameMgr.isStar = false;
                            GameMgr.ChangeVelocity();
                        }
                    }
                }

                else
                {

                    GameMgr.GameOver();
                }
            }
           
        }
        else if (other.tag == "Item_Shield")
        {
            Destroy(other.transform.gameObject);
            GameMgr.get_Shield();
            Get_Item_Sound.Play();

        }
        else if (other.tag == "Item_Star")
        {
            Destroy(other.transform.gameObject);
            GameMgr.get_Star();
            Get_Item_Sound.Play();
        }
        else if (other.tag == "Item_Time")
        {
            Destroy(other.transform.gameObject);
            GameMgr.get_Time();
            Get_Item_Sound.Play();
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        GameMgr = GM.GetComponent<GameManager>();
        Playercs = PlayerGO.GetComponent<Player>();
        StartCoroutine(dulkung());
    }
    bool isUp=true;
    IEnumerator dulkung()
    {
        if (isUp)
        {
            transform.Translate(0f, 0.01f, 0f);

        }
        else
        {
            transform.Translate(0f, -0.01f, 0f);
        }
        if (transform.position.y > 0.01f)
            isUp = false;
        if (transform.position.y < -0.01f)
            isUp = true;
        yield return new WaitForSeconds(0.01f);
        StartCoroutine(dulkung());
    }
    // Update is called once per frame
    void Update()
    {

    }
}
