﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Problem : MonoBehaviour
{
    
    public GameObject Problem_text;
    public GameObject[] Ans_text =new GameObject[3];
    public GameObject Ans_coll;

    public GameObject PlayerGO;
    private Player Playercs;

    public GameObject[] Panel = new GameObject[3];
    private void Awake()
    {

        PlayerGO = GameObject.Find("Player");
        Playercs = PlayerGO.GetComponent<Player>();
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < 3; i++)
            Panel[i].SetActive(false);
        Panel[Playercs.location].SetActive(true);
    }
}
