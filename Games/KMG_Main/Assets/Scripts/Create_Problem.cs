﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Create_Problem : MonoBehaviour
{

    public GameObject Problem_Prefab;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void Make_Problem(Vector3 position,int level)
    {
        GameObject PP = Instantiate(Problem_Prefab, position, Quaternion.Euler(new Vector3(0f,0f,0f)));
        int ans = Set(PP.GetComponent<Problem>().Problem_text, PP.GetComponent<Problem>().Ans_text, level);
       
        PP.GetComponent<Problem>().Ans_coll.tag = ans.ToString();
    }


    private int Set(GameObject pro, GameObject[] ans, int d)
    {
        // 랜덤 문제 생성
        // ac, a: 문제의 왼쪽 숫자 계수, 지수
        // bc, b: 문제의 오른쪽 숫자 계수, 지수
        int a = Random.Range(1, 15), b = Random.Range(1, 16 - a);

        // 난이도별 계수 결정
        int ac, bc;
        switch (d)
        {
            case 0: // 난이도 0 일 때: 항상 곱한계수가 1
                ac = 1; bc = 1;
                break;
            case 1: // 난이도 1 일 때: 곱한계수가 1이 아닌 한자리수
                do
                {
                    ac = Random.Range(1, 10);
                    bc = Random.Range(1, 10);
                } while (ac * bc >= 10 || (ac * bc) == 1);
                break;
            case 2: // 난이도 2 일 때: 곱한계수가 10의 배수가 아닌 두자리수
                do
                {
                    ac = Random.Range(1, 10);
                    bc = Random.Range(1, 10);
                } while (ac * bc <= 10 || (ac * bc) % 10 == 0);

                // 25천조 이런거 안댐, 이런 경우 한자리 내림
                if (a + b == 15)
                {
                    if (a == 1)      // 5십 * 5백조 이런 경우 오른쪽 수를 한자리 내림
                        b--;
                    else if (b == 1) // 5백조 * 5십 이런 경우 왼쪽 수를 한자리 내림
                        a--;
                    else
                    {           // 양쪽 다 내려도 문제없는 경우 랜덤으로 골라서 내림
                        if (Random.Range(0, 2) == 0)
                            a--;
                        else
                            b--;
                    }
                }
                break;
            default: // 예외처리
                Debug.Log("Difficulty out of range");
                return -1;
        }

        // 문제 Text 표시
        pro.GetComponent<TextMesh>().text = intToStr(ac, a) + " x " + intToStr(bc, b);

        // p는 자릿수, c는 계수: c*10^p
        int p = a + b, c = ac * bc;

        // 랜덤 정답 생성
        int ansIdx;
        if (p == 2)                           // 정답이 3백 이런 경우 항상 ansIdx는 0, 1
            ansIdx = Random.Range(0, 2);
        else if (p + (c >= 10 ? 1 : 0) == 15) // 정답이 천조인 경우 항상 ansIdx는 2
            ansIdx = 2;
        else if (p + (c >= 10 ? 1 : 0) == 14) // 정답이 백조인 경우 항상 ansIdx는 1, 2
            ansIdx = Random.Range(1, 3);
        else                                  // 일반적인 경우 ansIdx는 0, 1, 2
            ansIdx = Random.Range(0, 3);

        int[] r = new int[3];
        r[0] = Random.Range(0, 3);
        do
        {
            r[1] = Random.Range(0, 3);
        } while (r[0] == r[1]);
        r[2] = 3 - r[0] - r[1];

        int rr = Random.Range(0, 2);
        for (int i = 0; i < 3; i++) ans[r[i]].GetComponent<TextMesh>().text = intToStr(c, p + i - ansIdx, rr);

        // 정답 index 반환
        return r[ansIdx];
    }

    // 계수와 지수 받아서 글로 반환
    private string intToStr(int c, int p, int isNumDisplay = 0)
    {
        if (p + (c >= 10 ? 1 : 0) > 15)
        {
            Debug.Log("Num out of range");
            return "ERROR";
        }
        string[] THT = { "", "십", "백", "천" };
        string[] KMG = { "", "만", "억", "조" };
        if (c == 1) // 계수가 1인 경우
            return THT[p % 4] + KMG[p / 4];
        else if (c < 10) // 계수가 1이 아닌 한자리 수인 경우
            return c.ToString() + THT[p % 4] + KMG[p / 4];
        else if (10 < c || c < 100) { // 계수가 두자리 수인 경우 (앞의 문제 생성 과정에서 10의 배수는 들어오지 않음)
            if (isNumDisplay == 0) return (c / 10).ToString() + (p % 4 == 3 ? KMG[p / 4 + 1] : THT[p % 4 + 1]) + (c % 10).ToString() + THT[p % 4] + KMG[p / 4];
            else return c.ToString() + THT[p % 4] + KMG[p / 4];
        }
        else { // 예외처리
            Debug.Log("Coefficient out of range");
            return "ERROR";
        }
    }
}
