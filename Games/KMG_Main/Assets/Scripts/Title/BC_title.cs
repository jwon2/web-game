﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BC_title : MonoBehaviour {
    private const int MaxPage = 9;
    public GameObject HelpContentsPar;
    public GameObject[] HelpContents = new GameObject[9];
    public GameObject LeftButton;
    public GameObject RightButton;
    public GameObject RankData;
    public GameObject SoundOnButton;
    public GameObject SoundOffButton;
    private int Page;

    void Start() {
        if(!PlayerPrefs.HasKey("isSoundOn")) PlayerPrefs.SetFloat("isSoundOn", 1f);
        if(PlayerPrefs.GetFloat("isSoundOn") == 1f)
        {
            AudioListener.volume = 1f;
            SoundOffButton.SetActive(true);
            SoundOnButton.SetActive(false);
        } else
        {
            AudioListener.volume = 0f;
            SoundOffButton.SetActive(false);
            SoundOnButton.SetActive(true);
        }
    }

    public void GameStart()
    {
        SceneManager.LoadScene("Play");
    }

    public void SetFirstPage()
    {
        for (int i = 0; i < MaxPage; i++) HelpContentsPar.transform.GetChild(i).gameObject.SetActive(false);
        Page = 0;
        HelpContents[Page].SetActive(true);
        LeftButton.SetActive(false);
        RightButton.SetActive(true);
    }

    public void RightPage()
    {
        if (Page == 0) LeftButton.SetActive(true);
        HelpContents[Page].SetActive(false);
        Page++;
        HelpContents[Page].SetActive(true);
        if (Page == MaxPage-1) RightButton.SetActive(false);
    }

    public void LeftPage()
    {
        if (Page == MaxPage-1) RightButton.SetActive(true);
        HelpContents[Page].SetActive(false);
        Page--;
        HelpContents[Page].SetActive(true);
        if (Page == 0) LeftButton.SetActive(false);
    }

    public void HideRankData()
    {
        Vector3 pos = RankData.transform.position;
        RankData.transform.position = new Vector3(pos.x, pos.y - 500f, 0);
    }

    public void SoundOn()
    {
        PlayerPrefs.SetFloat("isSoundOn", 1f);
        AudioListener.volume = 1f;
    }

    public void SoundOff()
    {
        PlayerPrefs.SetFloat("isSoundOn", 0f);
        AudioListener.volume = 0f;
    }
}
