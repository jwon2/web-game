﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Number : MonoBehaviour
{

    GameObject[] NUM = new GameObject [10];
    // Start is called before the first frame update
    public void Set_Num(int n)
    {
        for (int i = 0; i < 10; i++)
            NUM[i].SetActive(false);
        NUM[n].SetActive(true);
    }
    private void Awake()
    {
        for (int i = 0; i < 10; i++)
            NUM[i] = this.transform.GetChild(i).gameObject;
    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
