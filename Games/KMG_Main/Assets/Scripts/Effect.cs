﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Effect : MonoBehaviour
{
    public GameObject[] Eff = new GameObject[3];

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Effect_IE());
    }
    public IEnumerator Start_eff(int n)
    {
        Eff[n].SetActive(true);

        yield return new WaitForSeconds(0.5f);
        Eff[n].SetActive(false);
    }

    IEnumerator Effect_IE()
    {
        for (int i = 0; i < 3; i++) {
            if (Eff[i].transform.GetChild(0).gameObject.activeSelf)
            {
                Eff[i].transform.GetChild(1).gameObject.SetActive(true);
                Eff[i].transform.GetChild(0).gameObject.SetActive(false);

            }
            else
            {
                Eff[i].transform.GetChild(1).gameObject.SetActive(false);
                Eff[i].transform.GetChild(0).gameObject.SetActive(true);
            }
        }
        yield return new WaitForSeconds(0.1f);

        StartCoroutine(Effect_IE());
    }


    // Update is called once per frame
    void Update()
    {
        
    }
}
