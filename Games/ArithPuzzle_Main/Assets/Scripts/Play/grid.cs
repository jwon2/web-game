﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class grid : MonoBehaviour {

    // 상수값들
    const float width = 2208;
    const float height = 1242;

    public GameObject EC;
    private ec eccs;

    public GameObject[] LeftTop = new GameObject[3];  // 0 : 4*4 1: 6*6 2: 8*8
    public GameObject[] RightBottom = new GameObject[3];
    public GameObject[] LeftTopView = new GameObject[3];
    public GameObject[] RightBottomView= new GameObject[3];

    public GameObject[] book = new GameObject[3];
    public GameObject[] AnswerGrid = new GameObject[3];

    public GameObject BlockPref;
    public GameObject Blocks; // 0: 흰색 1: 빨간색 2: 노란색 3: 파란색 4: 초록색 5: 검은색
    public GameObject[] Bounce_effect = new GameObject[6];
    public GameObject[] Bounce_effect_c = new GameObject[6];

    public AudioSource CreateBlock;
    public bool isfinish_setting = false;

    private void Awake()
    {
        eccs = EC.GetComponent<ec>();
    }
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void MakeGrid(int num)
    {
        int index;
        if (eccs.gridSize == 4)
            index = 0;
        else if (eccs.gridSize == 6)
            index = 1;
        else
            index = 2;

        book[index].SetActive(true);
        AnswerGrid[index].SetActive(true);
        float dis_x = (RightBottom[index].transform.position.x - LeftTop[index].transform.position.x) / (num - 1);
        float dis_y = (LeftTop[index].transform.position.y - RightBottom[index].transform.position.y) / (num - 1);
        float disView_x = (RightBottomView[index].transform.position.x - LeftTopView[index].transform.position.x) / (num - 1);
        float disView_y = (LeftTopView[index].transform.position.y - RightBottomView[index].transform.position.y) / (num - 1);
        
        for (int i = 0; i <= num; i++)
        {
            for(int j = 0; j <= num; j++)
            {
                
                eccs.gridArr[i, j] = new Vector2(LeftTop[index].transform.position.x + (dis_x * i), LeftTop[index].transform.position.y - (dis_y * j));
                eccs.gridViewArr[i,j]= new Vector2(LeftTopView[index].transform.position.x + (disView_x * i), LeftTopView[index].transform.position.y - (disView_y * j));
                eccs.gridViewArr[i, j].x *= Screen.height / height;
                eccs.gridViewArr[i, j].y = (eccs.gridViewArr[i, j].y - Screen.height / 2f + height/2f ) * (Screen.height / height);
                if (i != num && j != num)
                {
                    GameObject newblock = Instantiate(BlockPref, eccs.gridArr[i, j], new Quaternion(0f, 0f, 0f, 1f));
                    newblock.name = "Block(" + i + "," + j + ")";
                    newblock.transform.SetParent(Blocks.transform,false);
                    newblock.transform.position = eccs.gridArr[i, j];
                    ChangeBlockColor(i, j, eccs.gridInit[i, j],0f,0);
                    eccs.gridNow[i, j] = eccs.gridInit[i, j];
                }
            }
        }
    }
    public void ChangeBlockColor(int i,int j,int color,float delay,int dir)
    {
        if(isfinish_setting)
            CreateBlockAudioPlay();
        StartCoroutine(ChangeBlockColorIE(i, j, color, delay, dir));
        eccs.gridNow[i, j] = color;
    }
    IEnumerator  ChangeBlockColorIE(int i, int j, int color, float delay, int dir)
    {
        yield return new WaitForSeconds(delay);
        GameObject GO = GameObject.Find("Block(" + i + "," + j + ")");

        int a = GO.GetComponent<WhatIsMyColor>().color;
        if (color != 0)
        {
            GameObject BE = Instantiate(Bounce_effect_c[color], GO.transform.position, new Quaternion(0f, 0f, 0f, 1f));
            BE.transform.SetParent(Bounce_effect_c[0].transform,false);
            BE.transform.position = GO.transform.position;
            //BE.transform.position +=new Vector3(-20f, -15f,0f);
        }
        else
        {
            if (a != 0)
            {
                GameObject BE = Instantiate(Bounce_effect[a], GO.transform.position, new Quaternion(0f, 0f, 0f, 1f));
                BE.transform.SetParent(Bounce_effect[0].transform, false);
                BE.transform.position = GO.transform.position;
                //BE.transform.position += new Vector3(-20f, -15f, 0f);
            }
            
        }

        if (dir==0)
        {
            if (a == 0)
            {
                GO.GetComponent<Animator>().SetTrigger("0" + color.ToString() + "UP");
                yield return new WaitForSeconds(0.4f);
                GO.GetComponent<Animator>().SetTrigger("gotocolor");
            }
            else
            {
                if (color == 0)
                {
                    GO.GetComponent<Animator>().SetTrigger("UP");
                    yield return new WaitForSeconds(0.4f);
                    GO.GetComponent<Animator>().SetTrigger("gotoempty");
                }
                else
                {
                    GO.GetComponent<Animator>().SetTrigger("go" + color.ToString());
                }
            }
        }
        else if(dir==1)
        {
            if (a == 0)
            {
                GO.GetComponent<Animator>().SetTrigger("0" + color.ToString() + "DOWN");
                yield return new WaitForSeconds(0.4f);
                GO.GetComponent<Animator>().SetTrigger("gotocolor");
            }
            else
            {
                if (color == 0)
                {
                    GO.GetComponent<Animator>().SetTrigger("DOWN");
                    yield return new WaitForSeconds(0.4f);
                    GO.GetComponent<Animator>().SetTrigger("gotoempty");
                }
                else
                {
                    GO.GetComponent<Animator>().SetTrigger("go" + color.ToString());
                }
            }
        }
        else if (dir == 2)
        {
            if (a == 0)
            {
                GO.GetComponent<Animator>().SetTrigger("0" + color.ToString() + "LEFT");
                yield return new WaitForSeconds(0.4f);
                GO.GetComponent<Animator>().SetTrigger("gotocolor");
            }
            else
            {
                if (color == 0)
                {
                    GO.GetComponent<Animator>().SetTrigger("LEFT");
                    yield return new WaitForSeconds(0.4f);
                    GO.GetComponent<Animator>().SetTrigger("gotoempty");
                }
                else
                {
                    GO.GetComponent<Animator>().SetTrigger("go" + color.ToString());
                }
            }
        }
        else if (dir == 3)
        {
            if (a == 0)
            {
                GO.GetComponent<Animator>().SetTrigger("0" + color.ToString() + "RIGHT");
                yield return new WaitForSeconds(0.4f);
                GO.GetComponent<Animator>().SetTrigger("gotocolor");
            }
            else
            {
                if (color == 0)
                {
                    GO.GetComponent<Animator>().SetTrigger("RIGHT");
                    yield return new WaitForSeconds(0.4f);
                    GO.GetComponent<Animator>().SetTrigger("gotoempty");
                }
                else
                {
                    GO.GetComponent<Animator>().SetTrigger("go" + color.ToString());
                }
            }
        }
        GO.GetComponent<WhatIsMyColor>().color = color;
    }

    public void GridInit()
    {

    }

    public void CheckAnswer()
    {
        if (eccs.gridNow == eccs.gridAnswer)
            eccs.StageEnd();
    }
    public void CreateBlockAudioPlay()
    {
        CreateBlock.Play();
    }
}
