﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Rankingcs : MonoBehaviour
{
    public GameObject[] TopRank = new GameObject[5];
    public GameObject MyRank;

    public class Rank
    {
        GameObject GO;
        Text Ranking, Nickname, ClearProblem, Level;
        Rank()
        {
            Ranking = null;
            Nickname = null;
            ClearProblem = null;
            Level = null;
        }
        public Rank(GameObject GO, Text Ranking, Text Nickname,Text ClearProblem, Text Level)
        {
            this.GO = GO;
            this.Ranking = Ranking;
            this.Nickname = Nickname;
            this.ClearProblem = ClearProblem;
            this.Level = Level;
        }
        public void Set_Rank(int Ranking, string Nickname, string ClearProblem, string Level)
        {
            if (Ranking <= 0) this.Ranking.text = "";
            else if (Ranking >= 100) this.Ranking.text = "99+";
            else this.Ranking.text = Ranking.ToString();
            this.Nickname.text = Nickname;
            this.ClearProblem.text = ClearProblem;
            this.Level.text = Level;
        }
    }
    public Rank[] Top5 = new Rank[5];
    public Rank MY;
    private void Awake()
    {
        MY = new Rank(MyRank, MyRank.transform.GetChild(0).GetChild(0).gameObject.GetComponent<Text>(), MyRank.transform.GetChild(1).gameObject.GetComponent<Text>(),
                MyRank.transform.GetChild(2).gameObject.GetComponent<Text>(), MyRank.transform.GetChild(3).gameObject.GetComponent<Text>());
        for (int i = 0; i < 5; i++)
        {
            Top5[i] = new Rank(TopRank[i],TopRank[i].transform.GetChild(0).GetChild(0).gameObject.GetComponent<Text>(), TopRank[i].transform.GetChild(1).gameObject.GetComponent<Text>(),
                TopRank[i].transform.GetChild(2).gameObject.GetComponent<Text>(), TopRank[i].transform.GetChild(3).gameObject.GetComponent<Text>());
        }

    }

    public void Get_Rank()
    {
        GameObject.Find("DontDestroyThis").GetComponent<Manager>().Get_Rank();
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
