﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Help_Manager : MonoBehaviour
{
    public GameObject Help;
    public GameObject[] page = new GameObject[12];
    public GameObject Left,Right;
    public GameObject CloseButton;

    private int nowpage;
    // Start is called before the first frame update

    public void Open_Help()
    {
        Help.SetActive(true);
        nowpage = 0;
        for (int i = 1; i < 12; i++)
            page[i].SetActive(false);
        page[0].SetActive(true);
        Left.SetActive(false);
        Right.SetActive(true);
    }
    public void Left_Button()
    {
        if (nowpage == 11)
            Right.SetActive(true);
        page[nowpage].SetActive(false);
        nowpage--;
        page[nowpage].SetActive(true);
        if (nowpage == 0)
            Left.SetActive(false);
    }
    public void Right_Button()
    {
        if (nowpage == 0)
            Left.SetActive(true);
        page[nowpage].SetActive(false);
        nowpage++;
        page[nowpage].SetActive(true);
        if (nowpage == 11)
            Right.SetActive(false);
    }
   


    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
