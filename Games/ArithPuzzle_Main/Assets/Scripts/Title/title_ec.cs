﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class title_ec : MonoBehaviour {
    public GameObject SoundOn_btn,SoundOff_btn;

    public static int[] StageNum = { 20, 50, 60, 0 };
    public GameObject Easy, Normal, Hard, Extreme;
    public GameObject[] easy_stage = new GameObject[StageNum[0]];
    public GameObject[] normal_stage = new GameObject[StageNum[1]];
    public GameObject[] hard_stage = new GameObject[StageNum[2]];
    public GameObject[] extreme_stage = new GameObject[StageNum[3]];

    public GameObject Stage;

    public GameObject RankBox;
    public GameObject Rank_Loading;
    public GameObject MyRank;
    public GameObject AfterGet;

    public void LevelClose() {
        for (int i = 1; i < Stage.transform.childCount; i++) Stage.transform.GetChild(i).gameObject.SetActive(false);
    }

    public Text EasyNum, EasyPercent, NormalNum, NormalPercent, HardNum, HardPercent, ExtremeNum, ExtremePercent;
    private GameObject DDT;

    public void GameClose() {
        Application.OpenURL(DDT.GetComponent<Manager>().user.closeUrl);
    }

    // Use this for initialization
    void Start () {
        DDT = GameObject.Find("DontDestroyThis");
        DDT.GetComponent<Manager>().Easy = Easy;
        DDT.GetComponent<Manager>().Normal = Normal;
        DDT.GetComponent<Manager>().Hard = Hard;
        DDT.GetComponent<Manager>().Extreme = Extreme;
        DDT.GetComponent<Manager>().easy_stage = (GameObject[])easy_stage.Clone();
        DDT.GetComponent<Manager>().normal_stage = (GameObject[])normal_stage.Clone();
        DDT.GetComponent<Manager>().hard_stage = (GameObject[])hard_stage.Clone();
        DDT.GetComponent<Manager>().extreme_stage = (GameObject[])extreme_stage.Clone();
        DDT.GetComponent<Manager>().EasyNum = EasyNum;
        DDT.GetComponent<Manager>().NormalNum = NormalNum;
        DDT.GetComponent<Manager>().HardNum = HardNum;
        DDT.GetComponent<Manager>().ExtremeNum = ExtremeNum;
        DDT.GetComponent<Manager>().EasyPercent = EasyPercent;
        DDT.GetComponent<Manager>().NormalPercent = NormalPercent;
        DDT.GetComponent<Manager>().HardPercent = HardPercent;
        DDT.GetComponent<Manager>().ExtremePercent = ExtremePercent;
        DDT.GetComponent<Manager>().RankBox = RankBox;
        DDT.GetComponent<Manager>().Rank_Loading = Rank_Loading;
        DDT.GetComponent<Manager>().MyRankBox = MyRank;
        DDT.GetComponent<Manager>().AfterGet = AfterGet;

        if (!PlayerPrefs.HasKey("Sound")) PlayerPrefs.SetFloat("Sound", 1f);
        if (PlayerPrefs.GetFloat("Sound") == 0f) {
            AudioListener.volume = 0f;
            SoundOff_btn.SetActive(false);
            SoundOn_btn.SetActive(true);
        }
        else {
            AudioListener.volume = 1f;
            SoundOff_btn.SetActive(true);
            SoundOn_btn.SetActive(false);
        }

        Destroy(GameObject.Find("PlayAudioManager"));
        DDT.GetComponent<Manager>().GETGETGET();
    }
	
    public void LevelStage(string str) // level : 1-easy 2-normal 3-hard 4-hell
    {
        string[] newstr = str.Split('-');
        PlayerPrefs.SetInt("level", int.Parse(newstr[0]));
        PlayerPrefs.SetInt("stage", int.Parse(newstr[1]));
        SceneManager.LoadScene("play");
    }

    public void gotoMapEditor()
    {
        SceneManager.LoadScene("MapEditor");
    }

    public void SoundOff()
    {
        SoundOn_btn.SetActive(true);
        SoundOff_btn.SetActive(false);
        PlayerPrefs.SetFloat("Sound", 0f);
        AudioListener.volume = 0f;
    }

    public void SoundOn()
    {
        SoundOn_btn.SetActive(false);
        SoundOff_btn.SetActive(true);
        PlayerPrefs.SetFloat("Sound", 1f);
        AudioListener.volume = 1f;
    }
}
