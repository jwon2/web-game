﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class Manager : MonoBehaviour {
    public const string gameName = "ArithPuzzle";

    private static Manager instance = null;

    public static int[] StageNum = { 20, 50, 60, 0 };
    public GameObject Easy, Normal, Hard, Extreme;
    public GameObject[] easy_stage = new GameObject[StageNum[0]];
    public GameObject[] normal_stage = new GameObject[StageNum[1]];
    public GameObject[] hard_stage = new GameObject[StageNum[2]];
    public GameObject[] extreme_stage = new GameObject[StageNum[3]];
    public Text EasyNum, EasyPercent, NormalNum, NormalPercent, HardNum, HardPercent, ExtremeNum, ExtremePercent;

    public GameObject RankBox;
    public GameObject Rank_Loading;
    public GameObject MyRankBox;
    public GameObject AfterGet;

    // User 데이터 저장
    public struct UserData {
        public string host;
        public string userid;
        public string nickname;
        public string token;
        public string closeUrl;
    }

    // index.html을 이용한 토큰 전달
    public UserData user = new UserData();
    public void SetUserData(string data) {
        user = JsonUtility.FromJson<UserData>(data);
    }

    // 데이터 저장할 object (DontDestroyThis) 컨트롤
    public static Manager Instance { get { return instance; } }
    private void Awake() {
        if (instance) {
            DestroyImmediate(gameObject);
            return;
        }
        instance = this;
        DontDestroyOnLoad(this.gameObject);
        Application.ExternalCall("SetUserData");
    }

    // Update is called once per frame
    public void Clear_Stage(int level, int stage) {
        stage--;
        bool isPut = false;
        switch(level) {
            case 1:
                if (!MY_DB.easy_stage[stage]) {
                    MY_DB.Easy++;
                    MY_DB.easy_stage[stage] = true;
                    isPut = true;
                }
                break;
            case 2:
                if (!MY_DB.normal_stage[stage]) {
                    MY_DB.Normal++;
                    MY_DB.normal_stage[stage] = true;
                    isPut = true;
                }
                break;
            case 3:
                if (!MY_DB.hard_stage[stage]) {
                    MY_DB.Hard++;
                    MY_DB.hard_stage[stage] = true;
                    isPut = true;
                }
                break;
            case 4:
                if (!MY_DB.extreme_stage[stage]) {
                    MY_DB.Extreme++;
                    MY_DB.extreme_stage[stage] = true;
                    isPut = true;
                }
                break;
            default:
                // Debug.Log("level value error");
                break;
        }

        if (isPut) {
            StartCoroutine(PutData(user.token, level, stage));
            StartCoroutine(PutRank());
        }
    }

    public void GETGETGET() {
        StartCoroutine(GetData(user.token));
    }

    [System.Serializable]
    class DATA {
        public int Easy, Normal, Hard, Extreme;
        public bool[] easy_stage, normal_stage, hard_stage, extreme_stage;
        public DATA() {
            Easy = 0;
            Normal = 0;
            Hard = 0;
            Extreme = 0;
            easy_stage = new bool[StageNum[0]];
            normal_stage = new bool[StageNum[1]];
            hard_stage = new bool[StageNum[2]];
            extreme_stage = new bool[StageNum[3]];
        }
    }

    [System.Serializable]
    class TF_DATA {
        public int code;
        public string message;
        public string createdTime;
        public string lastModifiedTime;
        public DATA data;
    }

    DATA MY_DB;

    private IEnumerator GetData(string token) {
        string url = user.host + "/user/v1/games/" + gameName+"/users/"+user.userid+"/state";
        
        using (UnityWebRequest w = UnityWebRequest.Get(url)) {
            w.SetRequestHeader("Authorization", "Bearer " + token);
            yield return w.SendWebRequest();
            // Debug.Log("GetData: " + w.downloadHandler.text);

            if (w.isHttpError || w.isNetworkError) {
                //TODO handle error
            }
            else {
                // Debug.Log(w.downloadHandler.text);
                //success
                MY_DB = JsonUtility.FromJson<TF_DATA>(w.downloadHandler.text).data;
                if (MY_DB.easy_stage == null) {
                    MY_DB.easy_stage = new bool[StageNum[0]];
                    MY_DB.Easy = 0;
                }
                if (MY_DB.normal_stage == null) {
                    MY_DB.normal_stage = new bool[StageNum[1]];
                    MY_DB.Normal = 0;
                }
                if (MY_DB.hard_stage == null) {
                    MY_DB.hard_stage = new bool[StageNum[2]];
                    MY_DB.Hard = 0;
                }
                if (MY_DB.extreme_stage == null) {
                    MY_DB.extreme_stage = new bool[StageNum[3]];
                    MY_DB.Extreme = 0;
                }
                // Debug.Log("Easy, normal, hard, extreme: " + MY_DB.Easy + ", " + MY_DB.Normal + ", " + MY_DB.Hard + ", " + MY_DB.Extreme);
                // Debug.Log("easy_stage: " + MY_DB.easy_stage[0].ToString() + ", " + MY_DB.easy_stage[1].ToString() + ", " + MY_DB.easy_stage[2].ToString());

                EasyNum.text = MY_DB.Easy.ToString() + "/" + StageNum[0].ToString();
                EasyPercent.text = (100 * ((float)MY_DB.Easy / StageNum[0])).ToString("#0") + "%";

                if (MY_DB.Easy >= (int)((float)StageNum[0] * 0.6f)) {
                    Normal.GetComponent<Button>().interactable = true;
                    NormalNum.text = MY_DB.Normal.ToString() + "/" + StageNum[1].ToString();
                    NormalPercent.text = (100 * ((float)MY_DB.Normal / StageNum[1])).ToString("#0") + "%";
                }
                if (MY_DB.Normal >= (int)((float)StageNum[1] * 0.7f)) {
                    Hard.GetComponent<Button>().interactable = true;
                    HardNum.text = MY_DB.Hard.ToString() + "/" + StageNum[2].ToString();
                    HardPercent.text = (100 * ((float)MY_DB.Hard / StageNum[2])).ToString("#0") + "%";
                }
                if (MY_DB.Hard >= (int)((float)StageNum[2] * 0.8f)) {/*
                    Extreme.GetComponent<Button>().interactable = true;
                    ExtremeNum.text = MY_DB.Extreme.ToString() + "/" + StageNum[3].ToString();
                    ExtremePercent.text = (100*((float)MY_DB.Extreme / StageNum[3])).ToString("#0")+"%";*/
                }

                for(int i = 0; i < StageNum[0]; i++)
                    if (MY_DB.easy_stage[i]) easy_stage[i].transform.GetChild(1).gameObject.SetActive(MY_DB.easy_stage[i]);
                for(int i = 0; i < StageNum[1]; i++)
                    if (MY_DB.normal_stage[i]) normal_stage[i].transform.GetChild(1).gameObject.SetActive(MY_DB.normal_stage[i]);
                for(int i = 0; i < StageNum[2]; i++)
                    if (MY_DB.hard_stage[i]) hard_stage[i].transform.GetChild(1).gameObject.SetActive(MY_DB.hard_stage[i]);
                for(int i = 0; i < StageNum[3]; i++)
                    if (MY_DB.extreme_stage[i]) extreme_stage[i].transform.GetChild(1).gameObject.SetActive(MY_DB.extreme_stage[i]);
            }
        }
    }

    private IEnumerator PutData(string token, int level, int stage) {
        string url = user.host + "/user/v1/games/" + gameName + "/users/" + user.userid + "/state";
        string data = "{\"data\":" + JsonUtility.ToJson(MY_DB) + "}";
        // Debug.Log("url: " + url + ", data: " + data);

        using (UnityWebRequest w = UnityWebRequest.Put(url, data)) {
            w.SetRequestHeader("Authorization", "Bearer " + token);
            w.SetRequestHeader("Content-Type", "application/json");
            yield return w.SendWebRequest();
            // Debug.Log("PutAndGetData: " + w.downloadHandler.text);

            if (w.isHttpError || w.isNetworkError)
            {
                //TODO handle error
            }
            else
            {
                //sucess
            }
        }
    }

    [System.Serializable]
    struct Badges {
        public Badge winner;
    }

    [System.Serializable]
    struct Badge {
        public int level;
    }

    [System.Serializable]
    struct User {
        public string userId;
        public string nickname;
        public Badges badges;
    }
    struct Ranking {
        public RankData my;
        public List<RankData> ranking;
    }

    // RankData 저장할 구조체
    [System.Serializable]
    struct RankData {
        public User user;
        public int rank;        // 등수
        public int score;       // 점수
        public float time;     // 시간
        public string nickname; // 닉네임
        public int level;       // 레벨 (깨봉홈페이지 레벨)
    }

    // 상위 5등과 자신의 RankData 저장할 구조체
    RankData[] Top5 = new RankData[5];

    RankData MyRank;

    public void Get_Rank() {
        Rank_Loading.SetActive(true);
        AfterGet.SetActive(false);
        StartCoroutine(GetRank(user.token));
    }

    private IEnumerator GetRank(string token) {
        string url = user.host + "/user/v1/games/" + gameName;

        using (UnityWebRequest w = UnityWebRequest.Get(url)) {
            w.SetRequestHeader("Authorization", "Bearer " + token);
            yield return w.SendWebRequest();

            // Debug.Log("GetData: " + w.downloadHandler.text);

            if (w.isHttpError || w.isNetworkError)
            {
                //TODO handle error
            }
            else
            {
                Rank_Loading.SetActive(false);
                AfterGet.SetActive(true);
                Ranking r = JsonUtility.FromJson<Ranking>(w.downloadHandler.text);


                // Debug.Log(w.downloadHandler.text);
                //success
                MyRank = r.my;
                MyRank.nickname = r.my.user.nickname;
                MyRank.level = r.my.user.badges.winner.level;
                
                // Debug.Log("my rank=" + MyRank.rank);

                int size = Math.Min(r.ranking.Count, 5);
                int i = 0;
                for (i = 0; i < size; i++)
                {
                    Top5[i] = r.ranking[i];
                    Top5[i].nickname = r.ranking[i].user.nickname;
                    Top5[i].level = r.ranking[i].user.badges.winner.level;

                    // Debug.Log(i + ": rank=" + Top5[i].rank + ", score=" + Top5[i].score);
                }
                if (i < 5)
                {
                    for (int j = i; j < 5; j++)
                    {
                        //TODO don't show empty data
                        Top5[j] = new RankData();
                    }
                }
                for (i = 0; i < 5; i++)
                    RankBox.GetComponent<Rankingcs>().Top5[i].Set_Rank(Top5[i].rank, Top5[i].nickname,Top5[i].score.ToString(), Top5[i].level.ToString());
                
                if (1 <= MyRank.rank && MyRank.rank <= 5)
                    MyRankBox.SetActive(false);
                else
                {
                    RankBox.GetComponent<Rankingcs>().MY.Set_Rank(MyRank.rank, MyRank.nickname, MyRank.score.ToString(), MyRank.level.ToString());
                    RankBox.SetActive(true);
                }
            }
        }
    }

    private IEnumerator PutRank() {
        string url = user.host + "/user/v1/games/" + gameName + "/users/" + user.userid;
        string data = "{\"score\":" + (MY_DB.Easy + MY_DB.Normal + MY_DB.Hard + MY_DB.Extreme).ToString() + ",\"time\":0}";

        using (UnityWebRequest w = UnityWebRequest.Put(url, data)) {
            w.SetRequestHeader("Authorization", "Bearer " + user.token);
            w.SetRequestHeader("Content-Type", "application/json");
            // Debug.Log(url + "\n\n" + data);
            yield return w.SendWebRequest();

            if (w.isHttpError || w.isNetworkError)
            {
                // TODO handle error
            }
            else
            {

            }
        }
    }
}
