import os

path = "/Git/web-game/Games/ArithPuzzle_Main/Assets"

try:
    f = open("./output.txt", 'w')
    f.write("")
    f.close()
except IOError:
    print("Not Cleared")

try:
    f = open("./output.txt", 'a')
    filelist = os.listdir(path)
    filelist.sort()
    for filename in filelist:
        if len(filename) >= 13 and filename[0:4] == 'hard' and filename[len(filename)-7:len(filename)] == 'ori.txt':
            try:
                problem = open(path+"/"+filename, 'r')
                pro = problem.read().strip()
                f.write("stageDB[3, " + filename[5:len(filename)-8] + "] = " + pro + ";")
                problem.close()
            except IOError:
                print("problem IOError")
    f.close()

except IOError:
    print("output IOError")
