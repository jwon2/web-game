﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EC : MonoBehaviour {
    // 카메라
    public GameObject Camera;

    // 깨다
    public GameObject QuedaPar;
    public GameObject Queda;
    private Animator quedaAni;

    // 코봇
    public GameObject kkobotDown;
    public GameObject kkobotUp;

    // 깨다가 들고 있는 풍선
    public GameObject balloonsPar, flyBalloonsPar;
    private GameObject[] balloons = new GameObject[10];
    private Vector3[] balloonsPos = new Vector3[10];
    private GameObject[] blanks = new GameObject[10];
    private GameObject[] addedBalloons = new GameObject[10];

    // 코봇이 들고 있는 풍선
    private GameObject[] kkobotDownBalloons = new GameObject[10];
    private GameObject[] kkobotUpBalloons = new GameObject[10];

    // 게임오버
    public GameObject clearWindow;

    // 버튼, 버튼 내부 풍선
    public GameObject answers;
    private GameObject[] answer = new GameObject[3];
    private GameObject[,] ansBalloons = new GameObject[3, 8];
    private GameObject[,] ansXBalloons = new GameObject[3, 8];
    

    // 문제 세팅
    private int startBalloonNum, deltaBalloonNum;
    private int ansIdx;
    private int chosenIdx;

    // 이동
    static private float frame = 40f;
    public GameObject quedaInit, quedaUsual, quedaUp;
    private Vector3 quedaInitPos, quedaUsualPos, quedaUpPos;
    public GameObject flyBalloonInit, flyBalloonFinish;
    private Vector3 flyBalloonInitPos, flyBalloonFinishPos;
    public GameObject kkobotDownPosGO, kkobotUpPosGO, kkobotDestGO;
    private Vector3 kkobotDownPos, kkobotUpPos, kkobotDestPos;
    private float qt;
    private bool isMoving;

    private IEnumerator move(GameObject go, Vector3 s, Vector3 f, float time, int n, float qt = 0) {
        go.transform.Translate((f - s) / (time * frame));
        yield return new WaitForSeconds(1f / frame);
        if (qt <= time)
            StartCoroutine(move(go, s, f, time, n, qt + 1f / frame));
        else {
            switch(n) {
                case 0: // 깨다가 시작하자마자 올라오는거 끝나고
                    if (deltaBalloonNum < 0) {
                        for (int i = startBalloonNum - 1; i >= startBalloonNum + deltaBalloonNum; i--)
                            balloons[i].transform.SetParent(flyBalloonsPar.transform);
                        flyBalloonsPar.transform.SetSiblingIndex(0);
                        StartCoroutine(move(flyBalloonsPar, flyBalloonInitPos, flyBalloonFinishPos, 2f, 1));
                    } else {
                        for (int i = 0; i < startBalloonNum; i++)
                            kkobotDownBalloons[i].SetActive(false);
                        for (int i = startBalloonNum; i < startBalloonNum + deltaBalloonNum; i++)
                            kkobotDownBalloons[i].SetActive(true);
                        for (int i = startBalloonNum + deltaBalloonNum; i < 10; i++)
                            kkobotDownBalloons[i].SetActive(false);
                        StartCoroutine(move(kkobotDown, kkobotDownPos, kkobotDestPos, 2f, 1));
                    }
                    break;
                case 1: // 코봇이 풍선을 갖다줬거나, 풍선이 위로 올라가서 문제 생성되자마자
                    if (deltaBalloonNum < 0)
                        kkobotDown.SetActive(false);
                    else {
                        for (int i = startBalloonNum; i < startBalloonNum + deltaBalloonNum; i++)
                            kkobotDownBalloons[i].SetActive(false);
                        kkobotDown.transform.rotation *= Quaternion.Euler(new Vector3(0f, 0f, 180f));
                        StartCoroutine(move(kkobotDown, kkobotDownPos, kkobotDestPos, 2f, -1));
                    }
                    setProblem();
                    break;
                case 2: // 코봇이 풍선을 갖다주려고 카메라가 올라온 후
                    StartCoroutine(move(kkobotUp, kkobotUpPos, kkobotDestPos, 2f, -1));
                    StartCoroutine(move(Camera, new Vector3(0f, 0f, 0f), new Vector3(0f, -9.5f, 0f), 2f, 3));
                    break;
                case 3: // 코봇이 풍선을 갖다준 후
                    for (int i = 0; i < 10; i++)
                        kkobotUpBalloons[i].SetActive(false);

                    for (int i = startBalloonNum + deltaBalloonNum; i < startBalloonNum + chosenIdx - ansIdx; i++)
                        blanks[i].SetActive(false);

                    for (int i = startBalloonNum + deltaBalloonNum; i < startBalloonNum + chosenIdx - ansIdx; i++)
                        balloons[i].SetActive(true);

                    for (int i = startBalloonNum + deltaBalloonNum; i < startBalloonNum + deltaBalloonNum + chosenIdx - ansIdx; i++) {
                        balloons[i].SetActive(true);
                        blanks[i].SetActive(false);
                    }

                    StartCoroutine(move(kkobotUp, kkobotDestPos, kkobotDownPos, 2f, -1));
                    break;
            }
        }
    }

    void Awake() {
        // 이동 경로 설정
        quedaInitPos = quedaInit.transform.position;
        quedaUsualPos = quedaUsual.transform.position;
        quedaUpPos = quedaUp.transform.position;
        flyBalloonInitPos = flyBalloonInit.transform.position;
        flyBalloonFinishPos = flyBalloonFinish.transform.position;
        kkobotDownPos = kkobotDownPosGO.transform.position;
        kkobotUpPos = kkobotUpPosGO.transform.position;
        kkobotDestPos = kkobotDestGO.transform.position;
        qt = 0;

        // 정답버튼 내의 풍선, X풍선들 할당
        for (int i = 0; i < 3; i++) {
            answer[i] = answers.transform.GetChild(i).gameObject;
            for (int j = 0; j < 8; j++)
                ansBalloons[i, j] = answer[i].transform.GetChild(0).GetChild(j).gameObject;
            for (int j = 0; j < 8; j++)
                ansXBalloons[i, j] = answer[i].transform.GetChild(1).GetChild(j).gameObject;
        }

        // 깨다, 코봇이 들고 있는 풍선들 할당
        for (int i = 0; i < 10; i++) {
            balloons[i] = balloonsPar.transform.GetChild(10 - i - 1).GetChild(0).gameObject;
            blanks[i] = balloonsPar.transform.GetChild(10 - i - 1).GetChild(1).gameObject;
            addedBalloons[i] = balloonsPar.transform.GetChild(10 - i - 1).GetChild(2).gameObject;
            kkobotDownBalloons[i] = kkobotDown.transform.GetChild(0).GetChild(10 - i - 1).GetChild(0).gameObject;
            kkobotUpBalloons[i] = kkobotUp.transform.GetChild(0).GetChild(10 - i - 1).GetChild(0).gameObject;
        }

        // 풍선 초기위치 저장
        for (int i = 0; i < 10; i++)
            balloonsPos[i] = balloons[i].transform.position;

        quedaAni = Queda.GetComponent<Animator>();

        // 문제 값 세팅
        startBalloonNum = PlayerPrefs.GetInt("startBalloonNum");
        deltaBalloonNum = PlayerPrefs.GetInt("deltaBalloonNum");

        QuedaPar.transform.position = quedaInitPos;

        for (int i = 0; i < 10; i++) {
            balloons[i].SetActive(false);
            blanks[i].SetActive(false);
            addedBalloons[i].SetActive(false);
        }
        displayPro(balloons, 0, startBalloonNum);
        StartCoroutine(move(QuedaPar, quedaInitPos, quedaUsualPos, 3f, 0));
    }

    private void setProblem() {
        // 풍선 초기위치 저장
        for (int i = 0; i < 10; i++)
            balloons[i].transform.position = balloonsPos[i];

        // 문제 display
        display();

        // 정답 값 세팅
        if (Mathf.Abs(deltaBalloonNum) == 1) ansIdx = 0;
        else if (Mathf.Abs(deltaBalloonNum) == 2) ansIdx = Random.Range(0, 2);
        else if (Mathf.Abs(deltaBalloonNum) == 7) ansIdx = Random.Range(1, 3);
        else if (Mathf.Abs(deltaBalloonNum) == 8) ansIdx = 2;
        else ansIdx = Random.Range(0, 3);

        // 정답 display
        for (int i = 0; i < 3; i++) setAnsBalloons(i, Mathf.Abs(deltaBalloonNum) + i - ansIdx, deltaBalloonNum < 0);
        answers.SetActive(true);
    }

    private void setAnsBalloons(int idx, int n, bool isNotX) {
        if (isNotX) {
            for (int i = 0; i < n; i++)
                ansBalloons[idx, i].SetActive(true);
            for (int i = n; i < 8; i++)
                ansBalloons[idx, i].SetActive(false);
            for (int i = 0; i < 8; i++)
                ansXBalloons[idx, i].SetActive(false);
        } else {
            for (int i = 0; i < n; i++)
                ansXBalloons[idx, i].SetActive(true);
            for (int i = n; i < 8; i++)
                ansXBalloons[idx, i].SetActive(false);
            for (int i = 0; i < 8; i++)
                ansBalloons[idx, i].SetActive(false);
        }
    }

    private void displayPro(GameObject[] go, int s, int f) {
        for (int i = s; i < f; i++)
            go[i].SetActive(true);
    }

    private void display() {
        for (int i = 0; i < 10; i++) {
            balloons[i].SetActive(false);
            blanks[i].SetActive(false);
            addedBalloons[i].SetActive(false);
        }
        if (deltaBalloonNum < 0) {
            displayPro(balloons, 0, startBalloonNum + deltaBalloonNum);
            displayPro(blanks, startBalloonNum + deltaBalloonNum, startBalloonNum);
        } else {
            displayPro(balloons, 0, startBalloonNum);
            displayPro(addedBalloons, startBalloonNum, startBalloonNum + deltaBalloonNum);
        }
    }

    public void choose(int n) {
        chosenIdx = n;
        answers.SetActive(false);
        if (deltaBalloonNum < 0) {
            for (int i = 0; i < 10; i++)
                kkobotUpBalloons[i].SetActive(false);
            for (int i = startBalloonNum + deltaBalloonNum; i < startBalloonNum + chosenIdx - ansIdx; i++)
                kkobotUpBalloons[i].SetActive(true);
            StartCoroutine(move(Camera, new Vector3(0f, 0f, 0f), new Vector3(0f, 9.5f, 0f), 2f, 2));
        } else {

        }
    }

    private void finish(int n) {
        if (n == ansIdx)
            clear();
        else
        {
            if (deltaBalloonNum > 0)
                deltaBalloonNum = ansIdx - n;
            else
                deltaBalloonNum = n - ansIdx;
            // 문제 값 세팅
            PlayerPrefs.SetInt("startBalloonNum", startBalloonNum);
            PlayerPrefs.SetInt("deltaBalloonNum", deltaBalloonNum);
            setProblem();
        }
    }

    private void clear() {
        deltaBalloonNum = 0;
        display();
        clearWindow.SetActive(true);
    }

    public void toTitle() {
        SceneManager.LoadScene("Story");
    }
}
