﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Title : MonoBehaviour {
    public GameObject Queda;
    public GameObject quedaAniGO;
    private Animator quedaAni;

    public Button GameStartButton;

    private bool isMoving;

    static private Vector3 flyDest = new Vector3(4f, 9.5f, 0f);
    static private float frame = 40f;
    static private float time = 2f;

    void Awake() { quedaAni = quedaAniGO.GetComponent<Animator>(); }

    public void GameStart() {
        if (isMoving)
            SceneManager.LoadScene("Story");
        isMoving = true;
        quedaAni.SetBool("isNorm", false);
        StartCoroutine(moveQueda(0f));
    }

    private IEnumerator moveQueda(float t) {
        Queda.transform.Translate(flyDest / (time * frame));
        yield return new WaitForSeconds(1f / frame);
        if (t <= time)
            StartCoroutine(moveQueda(t + 1f / frame));
        else
            SceneManager.LoadScene("Story");
    }
}
