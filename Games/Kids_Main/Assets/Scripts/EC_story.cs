﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EC_story : MonoBehaviour {
    // 깨다, 꼬봇
    public GameObject Queda;
    public GameObject quedaAniGO;
    private Animator quedaAni;
    public GameObject Kkobot;

    // Text display
    public GameObject textPar;
    private Text[] texts;
    private string[] textContents = new string[3];
    private int textsNum;
    private bool isNumDisplay;  // 1개, 2개.. or 한개, 두개..

    private int state;
    private bool isMoving;
    private int balloonNum;

    // 깨다 풍선
    public GameObject qBalloonPar;
    private GameObject[] qBalloons;
    private Vector3[] qBalloonsPos;

    // 꼬봇 풍선
    public GameObject kBalloonPar;
    private GameObject[] kBalloons;

    // 움직이는 시간 조절
    static private float frame = 40f;
    static private float time = 1f;

    // 움직이는 위치들
    public GameObject qUp;
    public GameObject qDown;
    public GameObject bUp;
    public GameObject bDown;
    public GameObject kRight;
    public GameObject kLeft;
    public Vector3 qUpPos;
    public Vector3 qDownPos;
    public Vector3 bUpPos;
    public Vector3 bDownPos;
    public Vector3 kRightPos;

    // 풍선 개수 저장
    private int startBalloonNum, deltaBalloonNum;

    void Awake() {
        quedaAni = quedaAniGO.GetComponent<Animator>();
        quedaAni.SetBool("isNorm", true);

        qUpPos = qUp.transform.position;
        qDownPos = qDown.transform.position;
        bUpPos = bUp.transform.position;
        bDownPos = bDown.transform.position;
        kRightPos = kRight.transform.position;

        textsNum = textPar.transform.childCount;
        texts = new Text[textsNum];
        for (int i = 0; i < textsNum; i++)
            texts[i] = textPar.transform.GetChild(i).GetComponent<Text>();

        balloonNum = qBalloonPar.transform.childCount;
        qBalloons = new GameObject[balloonNum];
        qBalloonsPos = new Vector3[balloonNum];
        kBalloons = new GameObject[balloonNum];
        for (int i = 0; i < balloonNum; i++) {
            qBalloons[i] = qBalloonPar.transform.GetChild(balloonNum - i - 1).gameObject;
            kBalloons[i] = kBalloonPar.transform.GetChild(balloonNum - i - 1).gameObject;
            qBalloonsPos[i] = qBalloons[i].transform.localPosition;
        }
    }

    // 문제 세팅 및 Text 초기화
    void Start() {
        setProblem();
        isNumDisplay = (Random.Range(0, 2) == 0);
        textContents[0] = "어제 " + numOrString(startBalloonNum) + "개의 풍선으로 하늘을 날고 있었어";
        if (deltaBalloonNum < 0) {
            textContents[1] = "그런데 " + numOrString(-deltaBalloonNum) + "개의 풍선" + (-deltaBalloonNum == 1 ? "" : "들") + "을 놓쳐 조금씩 떨어지고 있었는데";
            textContents[2] = "꼬봇이 풍선을 몇 개 가져와서 괜찮아졌어.\n꼬봇이 어떻게 한걸까?";
        }
        else {
            textContents[1] = "그런데 꼬봇이 " + numOrString(deltaBalloonNum) + "개의 풍선" + (deltaBalloonNum == 1 ? "" : "들") + "을 더 가져다 줘서 조금씩 올라가고 있었는데";
            textContents[2] = "풍선을 몇개 놓았더니 괜찮아졌어.\n어떻게 된걸까?";
        }

        for (int i = 0; i < textsNum; i++)
            texts[i].text = textContents[i];

        isMoving = false;
        state = 0;
    }

    void Update() {
        if (!isMoving && Input.GetKeyDown(KeyCode.Mouse0)) {
            isMoving = true;
            switch(state) {
                case 0:
                    texts[0].transform.gameObject.SetActive(false);
                    texts[1].transform.gameObject.SetActive(true);
                    if (deltaBalloonNum < 0) {
                        quedaAni.SetBool("isNorm", false);
                        StartCoroutine(balloonsUpAndQuedaDown());
                    }
                    else
                        StartCoroutine(kkobotComeAndQuedaUp());
                    break;
                case 1:
                    texts[1].transform.gameObject.SetActive(false);
                    texts[2].transform.gameObject.SetActive(true);
                    if (deltaBalloonNum < 0)
                        StartCoroutine(kkobotComeAndQuedaUp());
                    else
                        StartCoroutine(balloonsUpAndQuedaDown());
                    break;
                case 2:
                    SceneManager.LoadScene("Play");
                    break;
            }
            state++;
        }
    }

    private IEnumerator balloonsUpAndQuedaDown(float t = 0f) {
        if (deltaBalloonNum < 0) {
            for (int i = startBalloonNum + deltaBalloonNum; i < startBalloonNum; i++)
                qBalloons[i].transform.Translate((bUpPos - bDownPos) / (time * frame));
            Queda.transform.Translate((qDownPos - qUpPos) / (time * frame));
        } else {
            for (int i = startBalloonNum; i < startBalloonNum + deltaBalloonNum; i++)
                qBalloons[i].transform.Translate((bUpPos - bDownPos) / (time * frame));
            Queda.transform.Translate((qDownPos - qUpPos) / (time * frame));
        }

        yield return new WaitForSeconds(1f / frame);

        if (t <= time)
            StartCoroutine(balloonsUpAndQuedaDown(t + 1f / frame));
        else {
            if (deltaBalloonNum > 0)
                quedaAni.SetTrigger("Clear");
            isMoving = false;
        }
    }

    private IEnumerator kkobotComeAndQuedaUp(float t = 0f) {
        Kkobot.transform.Translate((kLeft.transform.position - kRightPos) / (time * frame));

        yield return new WaitForSeconds(1f / frame);

        if (t <= time)
            StartCoroutine(kkobotComeAndQuedaUp(t + 1f / frame));
        else {
            if (deltaBalloonNum > 0) {
                Kkobot.SetActive(false);
                setBalloon(qBalloons, 0, startBalloonNum + deltaBalloonNum);
                quedaAni.SetBool("isNorm", false);
                StartCoroutine(quedaMoveUp(true));
            } else {
                Kkobot.SetActive(false);
                setBalloon(qBalloons, 0, startBalloonNum);
                setBalloonPos();
                StartCoroutine(quedaMoveUp(false));
            }
        }
    }

    private IEnumerator quedaMoveUp(bool isUp, float t = 0f) {
        Queda.transform.Translate((isUp ? 1f : -1f) * (qUpPos - qDownPos) / (time * frame));

        yield return new WaitForSeconds(1f / frame);

        if (t <= time)
            StartCoroutine(quedaMoveUp(true, t + 1f / frame));
        else {
            if ((isUp && deltaBalloonNum < 0) || (!isUp && deltaBalloonNum > 0))
                quedaAni.SetTrigger("Clear");
            isMoving = false;
        }
    }

    // 문제 초기화
    private void setProblem() {
        startBalloonNum = Random.Range(3, balloonNum - 1);
        PlayerPrefs.SetInt("startBalloonNum", startBalloonNum);
        setBalloon(qBalloons, 0, startBalloonNum);

        do {
            deltaBalloonNum = Random.Range(-startBalloonNum, 11 - startBalloonNum);
        } while (deltaBalloonNum == 0);
        PlayerPrefs.SetInt("deltaBalloonNum", deltaBalloonNum);

        if (deltaBalloonNum > 0)
            setBalloon(kBalloons, startBalloonNum, startBalloonNum + deltaBalloonNum);
        else
            setBalloon(kBalloons, startBalloonNum + deltaBalloonNum, startBalloonNum);
    }

    // 숫자 or 문자로 display
    private string numOrString(int n) {
        if (isNumDisplay)
            return n.ToString();

        string[] ret = { "한", "두", "세", "네", "다섯", "여섯", "일곱", "여덟" };
        return ret[n - 1];
    }

    private void setBalloon(GameObject[] arr, int s, int f) {
        for (int i = 0; i < balloonNum; i++)
            arr[i].SetActive(false);
        for (int i = s; i < f; i++)
            arr[i].SetActive(true);
    }

    private void setBalloonPos() {
        for (int i = 0; i < balloonNum; i++)
            qBalloons[i].transform.localPosition = qBalloonsPos[i];
    }
}
