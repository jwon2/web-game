﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour {
    public GameObject EventController;
    public GameObject FireGO;
    private EventController ec;
    private Animator FireAni;

	void Awake() {
        ec = EventController.GetComponent<EventController>();
        FireAni = FireGO.GetComponent<Animator>();
    }

    public void Fire(int n) {
        FireAni.SetInteger("state", n);
    }

    // Item이랑 충돌시 먹고 ec에 haveBullet, num, oper 값 업데이트
    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.tag == "bullet_default" && ec.haveBullet == 0) { // 캐릭터와 기본 총알 충돌 시
            ec.haveBullet = 1;
            ec.MakeBullet();
            Destroy(collision.gameObject);
        }
        else if (collision.tag == "bullet_eq" && ec.haveBullet == 0) { // 캐릭터와 Oper 총알 충돌 시
            ec.haveBullet = 2;
            ec.num = collision.GetComponent<Bullet_eq>().num;
            ec.oper = collision.GetComponent<Bullet_eq>().oper;
            Destroy(collision.gameObject);
        }
        else if (collision.tag == "time") {
            ec.LeftTime += 5f;
            ec.MakeItem();
            Destroy(collision.gameObject);
        }
        else if (collision.tag == "super" && ec.haveBullet == 0) {
            ec.haveBullet = 3;
            ec.MakeItem();
            Destroy(collision.gameObject);
        } 
        else if (collision.tag == "magnet" && ec.haveBullet == 0) {
            ec.haveBullet = 4;
            ec.MakeItem();
            Destroy(collision.gameObject);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision) {
        if (collision.collider.tag == "MapEdge") {
            Vector3 v = transform.GetComponent<Rigidbody2D>().velocity;
            if (collision.transform.name == "Left") transform.position = new Vector3(18.5f, transform.position.y);
            else if (collision.transform.name == "Right") transform.position = new Vector3(-18.5f, transform.position.y);
            else if (collision.transform.name == "Top") transform.position = new Vector3(transform.position.x, -18.5f);
            else if (collision.transform.name == "Bottom") transform.position = new Vector3(transform.position.x, 18.5f);
            transform.GetComponent<Rigidbody2D>().velocity = -v;
            ec.SetCameraWithCharacterPos();
        }
    }
}
