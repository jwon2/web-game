﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EventController : MonoBehaviour {
    // Sound
    private AudioSources AS;
    public GameObject SoundOnButton;
    public GameObject SoundOffButton;
    
    // Rank
    public GameObject GameOverRankBox;
    public GameObject MyLoading;

    // Prefab 부모. 미니맵에 있는 수식, 그냥 맵에 수식, 맵에 기본총알들 부모가 되는 GameObject
    public GameObject MinimapEquations;
    public GameObject Equations;
    public GameObject Bullets;
    private float MinimapRate;

    // 캐릭터 이동. 위에서부터 캐릭터 오브젝트, 회전속도, 힘, 속도제한, 캐릭터속도, 속도 출력이고 Awake에서 초기화
    public GameObject Character;
    private float CharacterAngularVelocity;
    private float NormalForce;
    private float BoosterForce;
    private float Force;
    private float Velocity;

    // 게임종료화면
    public GameObject GameFinish;

    // 점수 및 시간
    public GameObject TimeBarGauge;
    public Text ScoreText;
    public Text TimeText;
    public int Score;
    public float CurrentTime;
    public float LeftTime;

    public GameObject Line;

    // Bullet. 위에서부터 캐릭터가 줍는 기본총알, Oper총알, 캐릭터가 쏘는 기본총알, Oper총알
    public GameObject DefaultBullet;
    public GameObject OperBullet;
    public GameObject DefaultBullettoshoot;
    public GameObject OperBullettoshoot;

    // Minimap. 위에서부터 메인카메라, 미니맵에 카메라 사각형 (그냥 Object), 미니맵에 수식 (Prefab), 메인카메라 위치벡터
    public GameObject MainCamera;
    public GameObject MinimapCamera;
    public GameObject MinimapEquation;
    private Vector3 MainCameraPosition;

    // BulletBox. BulletBox안에 들어갈 숫자, 연산자, 기본총알. (UI Object) On/Off로 조절 - 위치가 바뀔일없어서
    public GameObject[] NumbersInBox = new GameObject[10];
    public GameObject[] OpersInBox = new GameObject[4];
    public GameObject DefaultBulletInBox;
    public GameObject SuperBulletInBox;
    public GameObject MagnetInBox;

    // Item. super, magnet, time
    public GameObject ItemPar;
    public GameObject[] Items = new GameObject[3];
    public GameObject SuperBulletToShoot;
    public GameObject MagnetEffect;

    // 우측 카메라
    public GameObject MirrorCamera;

    // 시간 얼마 없을 때 깜빡깜빡
    public GameObject Red;
    public GameObject Siren;
    private float rt;
    private bool isEmer;

    //----------------------------------------------------------------------------------------------------------------
    private const float EdgeLength = 10f;
    private Vector3[] EquationPosition = { new Vector3(-EdgeLength, -EdgeLength, 0f), new Vector3(0f, -EdgeLength, 0f), new Vector3(EdgeLength, -EdgeLength, 0f), new Vector3(-EdgeLength, 0f, 0f), new Vector3(0f, 0f, 0f), new Vector3(EdgeLength, 0f, 0f), new Vector3(-EdgeLength, EdgeLength, 0f), new Vector3(0f, EdgeLength, 0f), new Vector3(EdgeLength, EdgeLength, 0f) };
    private int[] NumOfPosition = new int[9];
    public GameObject Equation;

    public int haveBullet; // 0: nothing, 1: default, 2: OperAndNum, 3: super, 4: magnet
    public int num;
    public char oper;
    public bool magnetActive;
    private static float MaxLife = 100f;

    private bool isPlaying;

    private bool isQualyeong;
    private bool isGettingBigger;
    private float characterSize;
    static private float QualyeongTime = 2f;

    private Vector3 preVelocity;

    void GameOver()
    {
        GameFinish.SetActive(true);
        MyLoading.SetActive(true);
        GameOverRankBox.SetActive(false);
        Character.GetComponent<Rigidbody2D>().velocity = new Vector2();
        isPlaying = false;
        transform.GetComponent<RankManager>().PutAndGetRankInfo(Score, CurrentTime);
    }

    void Awake()
    {
        // 초기화
        AS = transform.GetComponent<AudioSources>();
        if (!PlayerPrefs.HasKey("isSoundOn")) PlayerPrefs.SetInt("isSoundOn", 1);
        else if (PlayerPrefs.GetInt("isSoundOn") == 1)
        {
            SoundOnButton.SetActive(false);
            SoundOffButton.SetActive(true);
            AS.SoundOn();
        } else
        {
            SoundOnButton.SetActive(true);
            SoundOffButton.SetActive(false);
            AS.SoundOff();
        }
        preVelocity = new Vector3(0f, 1f, 0f);
        isEmer = false;
        rt = 0f;
        Velocity = 0f;
        isPlaying = true;
        isQualyeong = true;
        isGettingBigger = true;
        characterSize = 1f;
        MinimapRate = 2.9f;
        Force = 200f * Character.GetComponent<Rigidbody2D>().mass;
        NormalForce = 200f * Character.GetComponent<Rigidbody2D>().mass;
        BoosterForce = 300f * Character.GetComponent<Rigidbody2D>().mass;
        CharacterAngularVelocity = 400f;
        Score = 0;
        LeftTime = 50f;
        magnetActive = false;
        
        while (Equations.transform.childCount < 3) MakeEquation();
        StartCoroutine(BulletManage());
        StartCoroutine(ItemManage());

        StartCoroutine(Timer());
        StartCoroutine(tmpRed());

        haveBullet = 0;
    }

    IEnumerator Timer() {
        if (LeftTime < 4f * (2 - 1 / ((CurrentTime + 10f) / 50f + 1f))) {
            Siren.SetActive(true);
            AS.SirenOn();
            if (!isEmer)
                rt = 0f;
            isEmer = true;
        }
        else {
            isEmer = false;
            Siren.SetActive(false);
        }
        yield return new WaitForSeconds(0.01f);
        if (LeftTime > MaxLife) LeftTime = MaxLife;
        LeftTime -= 0.01f * (2 - 1 / ((CurrentTime + 10f) / 50f + 1f));
        CurrentTime += 0.01f;
        TimeBarGauge.GetComponent<Image>().fillAmount = LeftTime / MaxLife;
        if (LeftTime < 0f)
            GameOver();
        else
            StartCoroutine("Timer");
    }

    IEnumerator tmpRed() {
        rt += 0.01f;
        if (rt > 0.5f) rt = 0f;

        if (isEmer && rt < 0.1f)
            Red.SetActive(true);
        else
            Red.SetActive(false);

        yield return new WaitForSeconds(1f / 40f);
        if(isPlaying)
            StartCoroutine(tmpRed());
        else {
            Siren.SetActive(false);
            Red.SetActive(false);
            AS.SirenOff();
        }
    }

    private float dirToTheta(Vector3 direction) {
        if (direction.x == 0 && direction.y == 0) return -1000f;
        else if (direction.x == 0 && direction.y > 0) return 0f;
        else if (direction.x == 0 && direction.y < 0) return 180f;
        else if (direction.x > 0 && direction.y == 0) return 270f;
        else if (direction.x < 0 && direction.y == 0) return 90f;
        else if (direction.x > 0 && direction.y > 0) return Mathf.Atan(-direction.x / direction.y) / Mathf.PI * 180f + 360f;
        else if (direction.x < 0 && direction.y > 0) return Mathf.Atan(-direction.x / direction.y) / Mathf.PI * 180f;
        else return Mathf.Atan(-direction.x / direction.y) / Mathf.PI * 180f + 180f;
    }

    void Update() {
        // 미니맵 카메라 컨트롤
        MainCameraPosition = MainCamera.transform.position;
        MinimapCamera.transform.localPosition = new Vector3(MainCameraPosition.x * MinimapRate, MainCameraPosition.y * MinimapRate - 190f, 0f);

        // BulletBox 컨트롤 (저장된 num, oper따라서 출력만 함
        BulletBoxDelete(); // BulletBox 내 모든거 끔
        if (haveBullet == 1) DefaultBulletInBox.SetActive(true);
        else if (haveBullet == 2) {
            NumbersInBox[num].SetActive(true);
            switch (oper) {
                case '+':
                    OpersInBox[0].SetActive(true);
                    break;
                case '-':
                    OpersInBox[1].SetActive(true);
                    break;
                case '*':
                    OpersInBox[2].SetActive(true);
                    break;
                case '/':
                    OpersInBox[3].SetActive(true);
                    break;
            }
        } else if (haveBullet == 3) {
            SuperBulletInBox.SetActive(true);
        } else if (haveBullet == 4) {
            MagnetInBox.SetActive(true);
        }

        // 캐릭터 이동 및 속도 조절.
        if (Input.GetKey(KeyCode.LeftShift)) {
            Force = BoosterForce;
        } else {
            Force = NormalForce;
        }
        if (isPlaying && (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.RightArrow))) {
            isQualyeong = false;
            Vector3 direction = new Vector3();
            if (Input.GetKey(KeyCode.UpArrow))
                direction += Vector3.up;
            if (Input.GetKey(KeyCode.DownArrow))
                direction += Vector3.down;
            if (Input.GetKey(KeyCode.LeftArrow))
                direction += Vector3.left;
            if (Input.GetKey(KeyCode.RightArrow))
                direction += Vector3.right;
            if (direction.magnitude != 0) preVelocity = direction;

            if (direction.magnitude != 0)
            {
                if (Input.GetKey(KeyCode.LeftShift)) Velocity = 1.5f;
                else Velocity = 1f;
                direction /= direction.magnitude;
                Character.transform.position += direction * Force * Time.deltaTime * 0.005f;
                float theta = dirToTheta(direction) - Character.transform.rotation.eulerAngles.z;
                if (Mathf.Abs(theta) >= 180f) {
                    if (theta > 0) theta -= 360f;
                    else theta += 360f;
                }
                if (theta > 1f) Character.transform.rotation *= Quaternion.Euler(new Vector3(0f, 0f, 15f));
                else if (theta < -1f) Character.transform.rotation *= Quaternion.Euler(new Vector3(0f, 0f, -15f));
            }
            if(direction.magnitude == 0) {
                Character.GetComponent<Character>().Fire(0);
                isQualyeong = true;
            } else {
                if (Input.GetKey(KeyCode.LeftShift))
                    Character.GetComponent<Character>().Fire(2);
                else
                    Character.GetComponent<Character>().Fire(1);
            }
        } else {
            if (Velocity > 1f) Velocity *= 0.95f;
            else if (Velocity > 0.05f) Velocity *= 0.95f;
            else Velocity = 0f;
            Character.transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, dirToTheta(preVelocity)));
            Character.transform.position += preVelocity / preVelocity.magnitude * NormalForce * Time.deltaTime * 0.005f * Velocity;
            Character.GetComponent<Character>().Fire(0);
            isQualyeong = true;
        }

        if (characterSize <= 1f) isGettingBigger = true;
        if (characterSize >= 1.1f) isGettingBigger = false;

        if (isQualyeong) {
            if (isGettingBigger) characterSize += 1 / QualyeongTime * Time.deltaTime / 4;
            else characterSize -= 1 / QualyeongTime * Time.deltaTime / 4;
        }
        else if (characterSize > 1f) characterSize -= 1 / QualyeongTime * Time.deltaTime / 4;
        if (characterSize < 1f) characterSize = 1f;
        if (characterSize > 1.1f) characterSize = 1.1f;
        Character.transform.localScale = new Vector3(characterSize, characterSize, 1f);

        if (Input.GetKey(KeyCode.Space) && haveBullet != 0) Line.SetActive(true);
        else Line.SetActive(false);

        // 총알 발사
        if (isPlaying && Input.GetKeyUp(KeyCode.Space)) {
            if (haveBullet == 1) { // 기본 총알 가지고 있는 경우
                haveBullet = 0;
                Vector3 NewPosition = Character.transform.position;
                Quaternion NewQuaternion = new Quaternion(0f, 0f, 0f, 1f);
                GameObject ShootBullet = Instantiate(DefaultBullettoshoot, NewPosition, NewQuaternion, transform);

                AS.ShootingSound.Play();
                ShootBullet.transform.rotation = Character.transform.rotation;
            }
            else if (haveBullet == 2) { // Oper 총알 가지고 있는 경우
                haveBullet = 0;
                Vector3 NewPosition = Character.transform.position;
                Quaternion NewQuaternion = new Quaternion(0f, 0f, 0f, 1f);
                GameObject ShootBullet = Instantiate(OperBullettoshoot, NewPosition, NewQuaternion, transform);
                ShootBullet.GetComponent<Bullet_eq_shoot>().num = num;
                ShootBullet.GetComponent<Bullet_eq_shoot>().oper = oper;

                AS.ShootingSound.Play();
                ShootBullet.transform.rotation = Character.transform.rotation;
            } else if (haveBullet == 3) {
                haveBullet = 0;
                Vector3 NewPosition = Character.transform.position;
                Quaternion NewQuaternion = new Quaternion(0f, 0f, 0f, 1f);
                GameObject ShootBullet = Instantiate(SuperBulletToShoot, NewPosition, NewQuaternion, transform);

                AS.ShootingSound.Play();
                ShootBullet.transform.rotation = Character.transform.rotation;
            } else if (haveBullet == 4) {
                haveBullet = 0;
                StartCoroutine(StartMagnet());
            }
        }

        // 점수 세팅
        ScoreText.text = Score.ToString();
        TimeText.text = CurrentTime.ToString("#00.00");
    }

    IEnumerator StartMagnet() {
        magnetActive = true;
        MagnetEffect.SetActive(true);
        yield return new WaitForSeconds(10f);
        magnetActive = false;
        MagnetEffect.SetActive(false);
    }

    void BulletBoxDelete() { // BulletBox 내의 모든거 끔
        for (int i = 0; i < 10; i++) NumbersInBox[i].SetActive(false);
        for (int i = 0; i < 4; i++) OpersInBox[i].SetActive(false);
        DefaultBulletInBox.SetActive(false);
        SuperBulletInBox.SetActive(false);
        MagnetInBox.SetActive(false);
    }

    // 캐릭터가 맵 가장자리 충돌해서 반대편으로 갈 때 카메라도 순간이동
    public void SetCameraWithCharacterPos() {
        Vector3 CharacterPos = Character.transform.position;
        MainCamera.transform.position = new Vector3(CharacterPos.x, CharacterPos.y, -10f);
        MirrorCamera.transform.position = new Vector3(CharacterPos.x, CharacterPos.y, 10f);
    }

    //--------------------------------------------------------------------------------------------------------------------------------------------------
    // 수식 만들기 (위치만 잡아주고 Instantiate. 수식 내부 내용은 Equation Script에서 다룸)
    void MakeEquation() {
        int RandomNum;
        do {
            RandomNum = Random.Range(0, 9);
        } while (NumOfPosition[RandomNum] != 0);
        NumOfPosition[RandomNum]++;
        Vector3 Offset = new Vector3(Random.Range(-4f, 4f), Random.Range(-4f, 4f), 0f);
        Vector3 NewPosition = EquationPosition[RandomNum] + Offset;
        Quaternion NewQuaternion = new Quaternion(0f, 0f, 0f, 1f);

        GameObject newEquation = Instantiate(Equation, NewPosition, NewQuaternion, Equations.transform);
        newEquation.GetComponent<Equation>().id = RandomNum;

        GameObject newMinimapEquation = Instantiate(MinimapEquation, NewPosition, NewQuaternion, MinimapEquations.transform);
        newMinimapEquation.GetComponent<MinimapEquation>().SetEquation(newEquation);
    }

    // 인자로 전달된 eq Object를 제거하고 (제거 전에 아이템들은 Equations(수식 부모 Object)를 부모로 바꾼다) 새로운 수식을 Instantiate. 
    public void RemakeEquation(GameObject eq) {
        for(int i = eq.transform.childCount - 1; i >= 0; i--)
            if (eq.transform.GetChild(i).tag == "Item") eq.transform.GetChild(i).parent = eq.transform.parent;
        NumOfPosition[eq.GetComponent<Equation>().id]--;
        Destroy(eq);
        MakeEquation();
    }
    
    IEnumerator BulletManage() { // Bullet 100개 만듦
        while (Bullets.transform.childCount < 50) MakeBullet();
        yield return new WaitForSeconds(1f);
    }
    
    public void MakeBullet() { // Bullet 1개 만듦
        Vector3 NewPosition = new Vector3(Random.Range(-18f, 18f), Random.Range(-18f, 18f), 0f);
        Quaternion NewQuaternion = new Quaternion(0f, 0f, 0f, 1f);
        GameObject newBullet = Instantiate(DefaultBullet, NewPosition, NewQuaternion);
        newBullet.GetComponent<Magnet>().ec = transform.GetComponent<EventController>();
        newBullet.transform.localEulerAngles = new Vector3(0, 0, Random.Range(-180, 180));
        newBullet.transform.parent = Bullets.transform;
    }

    IEnumerator ItemManage() {
        while (ItemPar.transform.childCount < Mathf.Min((int)(4.0f * Mathf.Pow(1.2f, CurrentTime / 10)), 16)) MakeItem();
        yield return new WaitForSeconds(1f);
    }

    public void MakeItem() {    // Item 1개 만듦
        Vector3 NewPosition = new Vector3(Random.Range(-18f, 18f), Random.Range(-18f, 18f), 0f);
        Quaternion NewQuaternion = new Quaternion(0f, 0f, 0f, 1f);
        GameObject newItem = Instantiate(Items[(int)Random.Range(0f, 3f)], NewPosition, NewQuaternion);
        newItem.transform.parent = ItemPar.transform;
        newItem.transform.GetComponent<Magnet>().ec = transform.GetComponent<EventController>();
    }
    //-------------------------------------------------------------------------------------------------------------------------------------------
}
