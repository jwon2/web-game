﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioSources : MonoBehaviour
{
    public AudioSource PlayBGM;
    public AudioSource ShootingSound;
    public AudioSource Siren;
    private bool isSiren;

    void Start() {
        isSiren = false;
    }

    public void SoundOn()
    {
        AudioListener.volume = 1f;
        PlayerPrefs.SetInt("isSoundOn", 1);
    }

    public void SoundOff() {
        AudioListener.volume = 0f;
        PlayerPrefs.SetInt("isSoundOn", 0);
    }

    public void SirenOn() {
        if (!isSiren) {
            isSiren = true;
            Siren.Play();
        }
    }

    public void SirenOff() {
        isSiren = false;
        Siren.Stop();
    }
}
