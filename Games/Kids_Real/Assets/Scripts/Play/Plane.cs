﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plane : MonoBehaviour {
    private BackGround GM;

    float left_limit, right_limit;

    float direction;
    float velocity;

    private void Awake() {
        GM = GameObject.Find("GameManager").GetComponent<BackGround>();
        left_limit = GM.LeftBottom.transform.position.x - 2f;
        right_limit = GM.RightBottom.transform.position.x + 2f;

        if (Random.value < 0.5f)
            transform.rotation = Quaternion.Euler(0f, 180f, 0f);

        velocity = 2f;
    }
    
    void Update() {
        transform.Translate(new Vector3(-1f, 0f, 0f) * Time.deltaTime * velocity);
        if (transform.position.x < left_limit || transform.position.x > right_limit)
            Destroy_This();
    }

    void Destroy_This() {
        GM.MakePlane(1, false);
        Destroy(gameObject);
    }
}
