﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Answer : MonoBehaviour {
    private GameObject[] BallonPos = new GameObject[7];
    public GameObject[] BalloonPrefabs = new GameObject[10];
    public GameObject BalloonXPrefab;

    private Stack<GameObject> CreatedBallon = new Stack<GameObject>();

    public int n;

    public void Set_ballons(GameManager.problem Problem) {
        // 보기의 풍선들 다 없엠
        while (CreatedBallon.Count != 0)
            Destroy(CreatedBallon.Pop());

        if (Problem.isAdd) { //빼야됨
            for (int i = 0; i < Problem.Ans[n]; i++) {
                if (i < Problem.Delta)
                    CreatedBallon.Push(Instantiate(BalloonXPrefab, BallonPos[i].transform));
                else
                    CreatedBallon.Push(Instantiate(BalloonPrefabs[Problem.Init + Problem.Delta - i - 1], BallonPos[i].transform));
            }
        }
        else { // 더해야됨
            for (int i = 0; i < Problem.Ans[n]; i++) {
                if (Problem.Init - Problem.Delta + Problem.Ans[n] - i - 1 >= Problem.Init)
                    CreatedBallon.Push(Instantiate(BalloonXPrefab, BallonPos[i].transform));
                else
                    CreatedBallon.Push(Instantiate(BalloonPrefabs[Problem.Init - Problem.Delta + Problem.Ans[n] - i - 1], BallonPos[i].transform));
            }
        }
    }

    // Start is called before the first frame update
    private void Awake() {
        for(int i = 0; i < 7; i++)
            BallonPos[i] = transform.GetChild(i).gameObject;
    }
}
