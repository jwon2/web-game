﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackGround : MonoBehaviour {
    // 배경
    public GameObject[] Clouds = new GameObject[4];
    public GameObject[] Planes = new GameObject[2];
    public GameObject Bird;

    // Points
    public GameObject LeftBottom, LeftTop, RightBottom, RightTop;
    public GameObject Objects;

    void Start() {
        // 배경 물체들 - 구름, 비행기, 새
        MakeBackObject();
    }

    public void MakeBackObject() {
        MakeCloud(15, true);
        MakePlane(2, true);
        MakeBird(3, true);
    }

    public void MakeCloud(int n, bool isInit) {
        float pos_x, pos_y;
        for (int i = 0; i < n; i++) {
            if (isInit)
                pos_x = LeftBottom.transform.position.x + (RightBottom.transform.position.x - LeftBottom.transform.position.x) * Random.Range(-0.1f, 1.1f);
            else {
                if (Random.value < 0.5f)
                    pos_x = LeftBottom.transform.position.x - 2f;
                else
                    pos_x = RightBottom.transform.position.x + 2f;
            }
            pos_y = LeftBottom.transform.position.y + (RightTop.transform.position.y - LeftBottom.transform.position.y) * Random.Range(-0.02f, 0.9f);
            GameObject newCloud = Instantiate(Clouds[Random.Range(0, 4)], new Vector3(pos_x, pos_y, 0f), new Quaternion(0f, 0f, 0f, 1f), Objects.transform);
        }
    }

    public void MakePlane(int n, bool isInit) {
        float pos_x, pos_y;
        for (int i = 0; i < n; i++) {
            if (isInit)
                pos_x = LeftBottom.transform.position.x + (RightBottom.transform.position.x - LeftBottom.transform.position.x) * Random.Range(-0.1f, 1.1f);
            else {
                if (Random.value < 0.5f)
                    pos_x = LeftBottom.transform.position.x - 2f;
                else
                    pos_x = RightBottom.transform.position.x + 2f;
            }
            pos_y = LeftBottom.transform.position.y + (RightTop.transform.position.y - LeftBottom.transform.position.y) * Random.Range(-0.02f, 0.9f);
            GameObject newPlane = Instantiate(Planes[Random.Range(0, 2)], new Vector3(pos_x, pos_y, 0f), new Quaternion(0f, 0f, 0f, 1f), Objects.transform);
        }
    }

    public void MakeBird(int n, bool isInit) {
        float pos_x, pos_y;
        for (int i = 0; i < n; i++) {
            if (isInit)
                pos_x = LeftBottom.transform.position.x + (RightBottom.transform.position.x - LeftBottom.transform.position.x) * Random.Range(-0.1f, 1.1f);
            else {
                if (Random.value < 0.5f)
                    pos_x = LeftBottom.transform.position.x - 2f;
                else
                    pos_x = RightBottom.transform.position.x + 2f;
            }
            pos_y = LeftBottom.transform.position.y + (RightTop.transform.position.y - LeftBottom.transform.position.y) * Random.Range(-0.02f, 0.9f);
            GameObject newPlane = Instantiate(Bird, new Vector3(pos_x, pos_y, 0f), new Quaternion(0f, 0f, 0f, 1f), Objects.transform);
        }
    }
}
