﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectMove : MonoBehaviour {

    // 움직인 후에 움직임
    public delegate void moveCallBack(GameObject[] Dest, int idx, moveCallBack callback);

    public void StartMove(GameObject[] Dest, int idx, moveCallBack callback) {
        StartCoroutine(Moving(Dest, idx, callback));
    }

    IEnumerator Moving(GameObject[] Dest, int idx, moveCallBack callback) {
        float Velocity = 10f;
        Vector3 Direction = (Dest[idx].transform.position - transform.position);
        float time = Direction.magnitude / Velocity;
        float Frame = 80f;
        for (int i = 0; i < time * Frame; i++) {
            transform.Translate(Direction / (time * Frame));
            yield return new WaitForSeconds(1f / Frame);
        }
        transform.position = Dest[idx].transform.position;
        if (callback != null && Dest.Length > idx + 1)
            callback(Dest, idx + 1, callback);
    }

    // 움직이고 난 후 다른 함수 실행
    public delegate void CallBack();

    public void StartMove(GameObject Dest, CallBack callback) {
        StartCoroutine(Moving(Dest, callback));
    }

    IEnumerator Moving(GameObject Dest, CallBack callback, float timeScale = 1f) {
        float Velocity = 10f;
        Vector3 Direction = (Dest.transform.position - transform.position);
        float time = Direction.magnitude / Velocity;
        float Frame = 80f;

        for (int i = 0; i < time * Frame; i++) {
            transform.Translate(Direction / (time * Frame));
            yield return new WaitForSeconds(1f / (Frame * timeScale));
        }
        transform.position = Dest.transform.position;
        callback?.Invoke();
    }
}