﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {
    // 캐싱
    public GameObject MainCamera;

    // 풍선들
    public GameObject[] Balloons = new GameObject[10];

    public GameObject Character;
    public GameObject Queda;

    public GameObject[] GGobots = new GameObject[10];

    public GameObject Clear_GO;

    // Points
    public GameObject Character_Dest;

    public GameObject[] Balloonsblank = new GameObject[10];
    public GameObject[] Dest_long = new GameObject[5];
    public GameObject[] Dest_short = new GameObject[5];

    public GameObject XballoonStartPos;

    public GameObject[] GGobotCenters = new GameObject[10];

    public GameObject GGobot_default_pos;

    public GameObject CameraUp, CameraDown;
    
    // Problem UIs
    public GameObject[] AnswerUI = new GameObject[3];
    public GameObject ProblemUI;

    // SoundButton
    public GameObject SoundOff, SoundOn;

    // value들
    int BalloonNum;
    int chosenIdx;
    problem Problem;
    bool isUIActivated = false;
    bool isChecked = false;

    public struct problem {
        public bool isAdd;
        public int Init, Delta, AnsIndex;
        public int[] Ans;
        public problem(int Init, bool isAdd, int Delta, int[] Ans, int AnsIndex) {
            this.Init = Init;
            this.isAdd = isAdd;
            this.Delta = Delta;
            this.Ans = Ans;
            this.AnsIndex = AnsIndex;
        }
        public problem Clone() { return new problem(Init, isAdd, Delta, Ans, AnsIndex); }
        override
        public string ToString() {
            string str="Init : " +Init.ToString()+"\nisAdd : "+isAdd.ToString()+"\nDelta : "+Delta.ToString()+"\nAns : "+Ans[0].ToString() + Ans[1].ToString() + Ans[2].ToString() + "\nAnsIndex : "+AnsIndex.ToString();

            return str;
        }
    }

    void Awake() {
        if (PlayerPrefs.GetFloat("isSoundOn") == 0f) {
            SoundOff.SetActive(false);
            SoundOn.SetActive(true);
        } else {
            SoundOff.SetActive(true);
            SoundOn.SetActive(false);
        }
        // 문제 받음
        Problem = new problem(PlayerPrefs.GetInt("Init"), PlayerPrefs.GetInt("isAdd") == 1, PlayerPrefs.GetInt("Delta"), new int[] { PlayerPrefs.GetInt("Ans0"), PlayerPrefs.GetInt("Ans1"), PlayerPrefs.GetInt("Ans2") }, PlayerPrefs.GetInt("AnsIdx"));
    }

    problem imsi = new problem(3, true, 2, new int[] {2, 3, 4}, 0);
    
    void Start() {
        BalloonNum = Problem.Init + (Problem.isAdd ? Problem.Delta : -Problem.Delta);

        // 열기구에 있는 풍선 맞추기
        SetInitBallons();

        // 깨다 움
        Queda.GetComponent<Animator>().SetTrigger("Cry");

        // 열기구 올라옴
        Character.GetComponent<ObjectMove>().StartMove(Character_Dest, StartProblemAni);
    }
    
    // 초기 문제나오기 전 풍선 세팅
    void SetInitBallons() {
        for (int i = 0; i < 10; i++) {
            if (i < Problem.Init)
                Balloons[i].GetComponent<Animator>().SetBool("isOriginal", true);
            else
                Balloons[i].transform.position = XballoonStartPos.transform.position;
        }
    }

    // 버튼 세팅
    void SetUI() {
        for (int i = 0; i < 3; i++) {
            AnswerUI[i].GetComponent<Answer>().n = i;
            AnswerUI[i].GetComponent<Answer>().Set_ballons(Problem);
        }
    }

    void StartProblemAni() {
        // 처음에 풍선이 더해질 때
        if (Problem.isAdd) {
            for (int i = Problem.Init; i < Problem.Init + Problem.Delta; i++) {
                GGobots[i].transform.position = XballoonStartPos.transform.position;
                Balloons[i].transform.position = XballoonStartPos.transform.position;
                GGobots[i].GetComponent<ObjectMove>().StartMove(new GameObject[] { Balloonsblank[i], GGobot_default_pos }, 0, GGobots[i].GetComponent<ObjectMove>().StartMove);
                Balloons[i].GetComponent<ObjectMove>().StartMove(Balloonsblank[i], OnProblemUI);
            }
        } else { // 처음에 풍선이 날아갈 때
            for (int i = Problem.Init - Problem.Delta; i < Problem.Init; i++) {
                if (i < 5)
                    Balloons[i].GetComponent<ObjectMove>().StartMove(Dest_short[i], OnProblemUI);
                else
                    Balloons[i].GetComponent<ObjectMove>().StartMove(Dest_long[i - 5], OnProblemUI);
                Balloonsblank[i].SetActive(true);
            }
        }
    }

    // 정답 클릭 시
    public void ClickAnswerButton(int n) {
        isChecked = false;
        chosenIdx = n;

        OffProblemUI();

        for (int i = 0; i < 10; i++)
            GGobots[i].transform.position = GGobot_default_pos.transform.position;

        if (Problem.isAdd) { // 풍선을 빼야하는 경우: 풍선이 날아가는 애니메이션 + 점선
            for (int i = BalloonNum - Problem.Ans[n]; i < BalloonNum; i++) {
                if (i < 5)
                    Balloons[i].GetComponent<ObjectMove>().StartMove(Dest_short[i], CheckAnswer);
                else
                    Balloons[i].GetComponent<ObjectMove>().StartMove(Dest_long[i - 5], CheckAnswer);
                if (i < Problem.Init)
                    Balloonsblank[i].SetActive(true);
            }
        } else { // 풍선을 꼬봇이 가져와야하는 경우: 꼬봇이 나타나고, 그 후 CameraMoveUp 실행
            for (int i = Problem.Init; i < 10; i++)
                Balloons[i].transform.position = XballoonStartPos.transform.position;
            for (int i = 0; i < 10; i++)
                GGobots[i].transform.position = GGobot_default_pos.transform.position;
            for (int i = BalloonNum; i < BalloonNum + Problem.Ans[n]; i++)
                GGobots[i].GetComponent<ObjectMove>().StartMove(GGobotCenters[i], CameraMoveUp);
        }
    }

    // 중복실행 방지
    bool isCameraMovingUp = false;
    void CameraMoveUp() {
        if (!isCameraMovingUp) {
            isCameraMovingUp = true;
            StartCoroutine(cameraMoveUp());
        }
    }

    // 카메라, 꼬봇 올라가고 이후 동작들
    IEnumerator cameraMoveUp() {
        yield return new WaitForSeconds(1f);
        // 꼬봇 위로
        for (int i = BalloonNum; i < BalloonNum + Problem.Ans[chosenIdx]; i++) {
            if (i < 5)
                GGobots[i].GetComponent<ObjectMove>().StartMove(Dest_short[i], null);
            else
                GGobots[i].GetComponent<ObjectMove>().StartMove(Dest_long[i - 5], null);
        }
        // 카메라 위로
        float Velocity = 8f;
        float Distance = Mathf.Abs(CameraUp.transform.position.y - MainCamera.transform.position.y);
        Vector3 Direction = (CameraUp.transform.position - MainCamera.transform.position);
        float time = Distance / Velocity;
        float timeScale = 0.01f;
        float Frame = time / timeScale;
        for (int i = 0; i < Frame; i++) {
            MainCamera.transform.Translate(Direction / Frame);
            yield return new WaitForSeconds(timeScale);
        }
        MainCamera.transform.position = CameraUp.transform.position;
        isCameraMovingUp = false;
        yield return new WaitForSeconds(1f);
        // 이후 동작
        StartCoroutine(AfterCameraMoveUp());
    }

    IEnumerator AfterCameraMoveUp() {
        if (Problem.AnsIndex >= chosenIdx) { // ? 꼬봇이 안나오는 경우
            // 꼬봇은 풍선가지고 오고 다시 원래자리로, 풍선은 내려와서 CheckAnswer 실행
            for (int i = BalloonNum; i < Problem.Init - (Problem.AnsIndex - chosenIdx); i++) {
                GGobots[i].GetComponent<ObjectMove>().StartMove(new GameObject[] { Balloonsblank[i], GGobot_default_pos }, 0, GGobots[i].GetComponent<ObjectMove>().StartMove);
                Balloons[i].GetComponent<ObjectMove>().StartMove(Balloonsblank[i], CheckAnswer);
            }
            // 카메라 내려옴
            float Velocity = 8f;
            float Distance = Mathf.Abs(CameraDown.transform.position.y - MainCamera.transform.position.y);
            Vector3 Direction = (CameraDown.transform.position - MainCamera.transform.position);
            float time = Distance / Velocity;
            float timeScale = 0.01f;
            float Frame = time / timeScale;
            for (int i = 0; i < Frame; i++) {
                MainCamera.transform.Translate(Direction / Frame);
                yield return new WaitForSeconds(timeScale);
            }
            MainCamera.transform.position = CameraDown.transform.position;
        } else {
            for (int i = Problem.Init; i < Problem.Init + chosenIdx - Problem.AnsIndex; i++) {
                GGobots[i].transform.GetChild(0).gameObject.SetActive(false);
                GGobots[i].transform.GetChild(1).gameObject.SetActive(true);
            }
            yield return new WaitForSeconds(1f);
            float max = 0f;
            for (int i = Problem.Init; i < Problem.Init + chosenIdx - Problem.AnsIndex; i++) {
                GGobots[i].transform.GetChild(0).gameObject.SetActive(true);
                GGobots[i].transform.GetChild(1).gameObject.SetActive(false);
                ObjectMove.moveCallBack callback = GGobots[i].GetComponent<ObjectMove>().StartMove;
                callback += Balloons[i].GetComponent<ObjectMove>().StartMove;
                if (i < 5) {
                    if (max < (XballoonStartPos.transform.position - GGobots[i].transform.position).magnitude / 10f * 2f)
                        max = (XballoonStartPos.transform.position - GGobots[i].transform.position).magnitude / 10f * 2f;
                    GGobots[i].GetComponent<ObjectMove>().StartMove(new GameObject[] { XballoonStartPos, Dest_short[i] }, 0, callback);
                }
                else {
                    if (max < (XballoonStartPos.transform.position - GGobots[i].transform.position).magnitude / 10f * 2f)
                        max = (XballoonStartPos.transform.position - GGobots[i].transform.position).magnitude / 10f * 2f;
                    GGobots[i].GetComponent<ObjectMove>().StartMove(new GameObject[] { XballoonStartPos, Dest_long[i - 5] }, 0, callback);
                }
            }
            StartCoroutine(takeBalloon(max + 0.5f));
        }
    }

    IEnumerator takeBalloon(float time) {
        yield return new WaitForSeconds(time);
        TakeBalloonsToQueda();
    }

    bool isTakingBalloonsToQueda = false;
    void TakeBalloonsToQueda() {
        if (!isTakingBalloonsToQueda) {
            isTakingBalloonsToQueda = true;
            StartCoroutine(takeBalloonsToQueda());
        }
    }

    IEnumerator takeBalloonsToQueda() {
        yield return new WaitForSeconds(2f);
        isTakingBalloonsToQueda = false;
        if (!isTakingBalloonsToQueda) {
            for (int i = Problem.Init - Problem.Delta; i < Problem.Init + chosenIdx - Problem.AnsIndex; i++) {
                GGobots[i].GetComponent<ObjectMove>().StartMove(new GameObject[] { Balloonsblank[i], GGobot_default_pos }, 0, GGobots[i].GetComponent<ObjectMove>().StartMove);
                Balloons[i].GetComponent<ObjectMove>().StartMove(Balloonsblank[i], null);
            }
        }
        StartCoroutine(cameraMoveDown());
    }

    IEnumerator cameraMoveDown() {
        float Velocity = 8f;
        float Distance = Mathf.Abs(CameraDown.transform.position.y - MainCamera.transform.position.y);
        Vector3 Direction = (CameraDown.transform.position - MainCamera.transform.position);
        float time = Distance / Velocity;
        float timeScale = 0.01f;
        float Frame = time / timeScale;
        for (int i = 0; i < Frame; i++) {
            MainCamera.transform.Translate(Direction / Frame);
            yield return new WaitForSeconds(timeScale);
        }
        MainCamera.transform.position = CameraDown.transform.position;
        for (int i = 0; i < 10; i++)
            Balloonsblank[i].SetActive(false);
        isTakingBalloonsToQueda = false;
        CheckAnswer();
    }

    void CheckAnswer() {
        if (!isChecked) {
            isChecked = true;
            if (Problem.AnsIndex == chosenIdx)
                Clear();
            else {
                Problem = MakeProblem.UpdateProblem(Problem, chosenIdx);
                BalloonNum = Problem.Init + (Problem.isAdd ? Problem.Delta : -Problem.Delta);
                OnProblemUI();
            }
            if (!Problem.isAdd) {
                for (int i = Problem.Init; i < 10; i++)
                    Balloons[i].transform.position = XballoonStartPos.transform.position;
            }
        }
    }

    void OnProblemUI() {
        if (!isUIActivated) {
            isUIActivated = true;
            foreach (GameObject Answer in AnswerUI)
                Answer.SetActive(true);

            ProblemUI.SetActive(true);
            SetUI();
        }
    }

    void OffProblemUI() {
        if (isUIActivated) {
            isUIActivated = false;
            foreach (GameObject Answer in AnswerUI)
                Answer.SetActive(false);
            ProblemUI.SetActive(false);

            StartCoroutine(delay_off_activated());
        }
    }

    IEnumerator delay_off_activated() {
        yield return new WaitForSeconds(1f);
        isUIActivated = false;
    }

    void Clear() {
        StartCoroutine(clear());
    }

    IEnumerator clear() {
        Queda.GetComponent<Animator>().SetTrigger("Clear");
        yield return new WaitForSeconds(1.5f);
        Clear_GO.SetActive(true);
    }
}
