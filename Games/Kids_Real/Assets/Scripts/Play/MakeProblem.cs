﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MakeProblem : GameManager
{
   

    public static problem SetProblem()
    {
        // 초기 풍선 수
        int Init = Random.Range(3, 9);

        // 나중 풍선 - 초기 풍선
        int Delta;
        do { Delta = Random.Range(-Init + 1, 11 - Init); } while (Delta == 0);

        // isAdd, Delta 세팅
        bool isAdd = Delta > 0;
        Delta = Mathf.Abs(Delta);

        // 정답 index 세팅
        int ansIdx;
        if (Delta == 1)
            ansIdx = 0;
        else if (Delta == 2)
            ansIdx = Random.Range(0, 2);
        else if (Delta == 7)
            ansIdx = 2;
        else if (Delta == 6)
            ansIdx = Random.Range(1, 3);
        else
            ansIdx = Random.Range(0, 3);

        // 보기 세팅
        int[] Ans = new int[3];
        for (int i = 0; i < 3; i++) Ans[i] = Delta + i - ansIdx;

        problem ret = new problem(Init, isAdd, Delta, Ans, ansIdx);
        Debug.Log(ret.ToString());
        return ret;
    }

    public static problem UpdateProblem(problem prePro, int chosenIdx) {
        // isAdd
        bool isAdd = (prePro.AnsIndex < chosenIdx) ? !prePro.isAdd : prePro.isAdd;

        // Delta
        int Delta = Mathf.Abs(prePro.AnsIndex - chosenIdx);

        // 정답 index 세팅
        int ansIdx;
        if (Delta == 1)
            ansIdx = 0;
        else if (Delta == 2)
            ansIdx = Random.Range(0, 2);
        else if (Delta == 7)
            ansIdx = 2;
        else if (Delta == 6)
            ansIdx = Random.Range(1, 3);
        else
            ansIdx = Random.Range(0, 3);

        // 보기 세팅
        int[] Ans = new int[3];
        for (int i = 0; i < 3; i++) Ans[i] = Delta + i - ansIdx;
        problem ret = new problem(prePro.Init, isAdd, Delta, Ans, ansIdx);
        Debug.Log(ret.ToString());
        return ret;
    }
}