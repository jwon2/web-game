﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonManager : MonoBehaviour {
    public void GameClose() {
        Application.OpenURL("");
    }

    public void SoundOn() {
        PlayerPrefs.SetFloat("isSoundOn", 1f);
        AudioListener.volume = 1f;
    }

    public void SoundOff() {
        PlayerPrefs.SetFloat("isSoundOn", 0f);
        AudioListener.volume = 0f;
    }

    public void Restart() {
        SceneManager.LoadScene("Story");
    }

    public void ToTitle() {
        SceneManager.LoadScene("Title");
    }
}
