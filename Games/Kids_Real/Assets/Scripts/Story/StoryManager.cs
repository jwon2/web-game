﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StoryManager : MonoBehaviour {
    private GameManager.problem Problem;
    public GameObject[] Balloons = new GameObject[10];
    public GameObject[] Balloonsblank = new GameObject[10];
    public GameObject[] GGobots = new GameObject[10];
    public GameObject Character, Queda;

    public GameObject GGobotLeft, GGobotUp, CharacterPos;
    public GameObject[] GGobotCenterPos = new GameObject[10];

    public GameObject[] TextPar = new GameObject[2];
    private GameObject[] Texts = new GameObject[7];

    bool isMoving;
    int state;
    float timeScale;
    static float frame = 60f;

    void Awake() {
        // 문제 만듦
        Problem = MakeProblem.SetProblem();
        PlayerPrefs.SetInt("isAdd", Problem.isAdd ? 1 : 0);
        PlayerPrefs.SetInt("Init", Problem.Init);
        PlayerPrefs.SetInt("Delta", Problem.Delta);
        PlayerPrefs.SetInt("Ans0", Problem.Ans[0]);
        PlayerPrefs.SetInt("Ans1", Problem.Ans[1]);
        PlayerPrefs.SetInt("Ans2", Problem.Ans[2]);
        PlayerPrefs.SetInt("AnsIdx", Problem.AnsIndex);
        for (int i = 0; i < Texts.Length; i++)
            Texts[i] = TextPar[Problem.isAdd ? 0 : 1].transform.GetChild(i).gameObject;
        for (int i = 0; i < Texts.Length; i++)
            Texts[i].SetActive(false);
        isMoving = false;
        state = 0;
        timeScale = 1f;
    }

    void Start() {
        // 풍선 세팅
        SetInitBallons();

        // 시작
        StartCoroutine(story(0));
    }

    // 초기 문제나오기 전 풍선 세팅
    void SetInitBallons() {
        for (int i = 0; i < 10; i++) {
            if (i < Problem.Init)
                Balloons[i].GetComponent<Animator>().SetBool("isOriginal", true);
            else
                Balloons[i].transform.position = GGobotLeft.transform.position;
        }
    }

    IEnumerator story(int i) {
        switch (i) {
            case 0: // 따미: 어제 뭐했
                yield return new WaitForSeconds(1f / timeScale);
                Texts[0].SetActive(true);
                yield return new WaitForSeconds(4f / timeScale);
                Texts[0].SetActive(false);
                break;
            case 1: // 깨다: 하늘 날고있: 열기구 날아옴
                Texts[1].SetActive(true);
                StartCoroutine(BalloonsCome());
                yield return new WaitForSeconds(6f / timeScale);
                Texts[1].SetActive(false);
                break;
            case 2: // 따미: 우와 재밌었겠
                Texts[2].SetActive(true);
                yield return new WaitForSeconds(4f / timeScale);
                Texts[2].SetActive(false);
                break;
            case 3: // 깨다: 그런데... 문제생김
                Texts[3].SetActive(true);
                GetProblem();
                yield return new WaitForSeconds(6f / timeScale);
                Texts[3].SetActive(false);
                break;
            case 4: // 따미: 어떻게?
                Texts[4].SetActive(true);
                yield return new WaitForSeconds(3f / timeScale);
                Texts[4].SetActive(false);
                break;
            case 5: // 깨다: ~해결
                Texts[5].SetActive(true);
                SolveProblem();
                yield return new WaitForSeconds(6f / timeScale);
                Texts[5].SetActive(false);
                break;
            case 6: // 따미: 우와
                Texts[6].SetActive(true);
                yield return new WaitForSeconds(3f / timeScale);
                Texts[6].SetActive(false);
                break;
            case 7:
                SceneManager.LoadScene("Play");
                break;
        }
        if (i < 7)
            StartCoroutine(story(i + 1));
    }

    // 열기구 날아옴
    IEnumerator BalloonsCome() {
        float time = 2f;
        Vector3 Dest = CharacterPos.transform.position - Character.transform.position;
        for (int i = 0; i < time * frame; i++) {
            Character.transform.Translate(Dest / (time * frame));
            yield return new WaitForSeconds(1f / (frame * timeScale));
        }
        Character.transform.position = CharacterPos.transform.position;
    }

    // 문제 생김
    void GetProblem() {
        if (Problem.isAdd) { // 처음에 풍선이 더해질 때
            for (int i = Problem.Init; i < Problem.Init + Problem.Delta; i++) {
                GGobots[i].transform.position = GGobotUp.transform.position;
                Balloons[i].transform.position = GGobotUp.transform.position;
                GGobots[i].GetComponent<ObjectMove>().StartMove(new GameObject[] { Balloonsblank[i], GGobotLeft }, 0, GGobots[i].GetComponent<ObjectMove>().StartMove);
                Balloons[i].GetComponent<ObjectMove>().StartMove(Balloonsblank[i], QuedaUp);
            }
        } else { // 처음에 풍선이 날아갈 때
            QuedaDown();
            for (int i = Problem.Init - Problem.Delta; i < Problem.Init; i++)
                StartCoroutine(balloonUp(i));
        }
    }

    // 열기구 올라감
    bool isQuedaUp = false;
    void QuedaUp() {
        if (!isQuedaUp) {
            isQuedaUp = true;
            StartCoroutine(quedaUp());
        }
    }

    IEnumerator quedaUp() {
        yield return new WaitForSeconds(0.5f);
        if (Problem.isAdd)
            Queda.GetComponent<Animator>().SetTrigger("Cry");
        float time = 1f;
        for (int i = 0; i < time * frame; i++) {
            Character.transform.Translate(Vector3.up / 2f / frame);
            yield return new WaitForSeconds(1f / frame / timeScale);
        }
        isQuedaUp = false;
        if (!Problem.isAdd)
        {
            Debug.Log("aa");
            Queda.GetComponent<Animator>().SetTrigger("Clear");
        }
    }

    // 열기구 내려감
    void QuedaDown() {
        StartCoroutine(quedaDown());
    }

    IEnumerator quedaDown() {
        if (!Problem.isAdd)
            Queda.GetComponent<Animator>().SetTrigger("Cry");
        float time = 1f;
        for (int i = 0; i < time * frame; i++) {
            Character.transform.Translate(Vector3.down / 2f / frame);
            yield return new WaitForSeconds(1f / frame / timeScale);
        }
        if (Problem.isAdd)
        {
            Debug.Log("AA");
            Queda.GetComponent<Animator>().SetTrigger("Clear");
        }
    }

    // 풍선 위로 날아감
    IEnumerator balloonUp(int n) {
        float time = 2f;
        float distance = 4f;
        for (int i = 0; i < time * frame; i++) {
            Balloons[n].transform.Translate(Vector3.up * distance / frame);
            yield return new WaitForSeconds(1f / frame / timeScale);
        }
    }

    // 풍선 위로 날아감
    IEnumerator balloonDown(int n) {
        float time = 2f;
        float distance = 4f;
        for (int i = 0; i < time * frame; i++) {
            Balloons[n].transform.Translate(Vector3.down * distance / frame);
            yield return new WaitForSeconds(1f / frame / timeScale);
        }
    }

    void SolveProblem() {
        if (Problem.isAdd) { // 더해진 경우 날려야댐
            QuedaDown();
            for (int i = Problem.Init; i < Problem.Init + Problem.Delta; i++)
                StartCoroutine(balloonUp(i));
        } else { // 날아간 경우 가져와야댐, 꼬봇들 나오고 GoGetBalloon
            for (int i = Problem.Init - Problem.Delta; i < Problem.Init; i++) {
                GGobots[i].transform.position = GGobotLeft.transform.position;
                GGobots[i].GetComponent<ObjectMove>().StartMove(GGobotCenterPos[i], GoGetBalloon);
            }
        }
    }

    bool isGoGetBalloon = false;
    void GoGetBalloon() {
        if (!isGoGetBalloon) {
            isGoGetBalloon = true;
            StartCoroutine(goGetBalloon());
        }
    }

    IEnumerator goGetBalloon() {
        yield return new WaitForSeconds(1f / timeScale);
        for (int i = Problem.Init - Problem.Delta; i < Problem.Init; i++)
            StartCoroutine(GGobotGoGetBalloon(i));
    }

    IEnumerator GGobotGoGetBalloon(int n) {
        float time = 1.5f;
        Vector3 Dest = Balloons[n].transform.position - GGobots[n].transform.position;
        for (int i = 0; i < time * frame; i++) {
            GGobots[n].transform.Translate(Dest / (time * frame));
            yield return new WaitForSeconds(1f / frame / timeScale);
        }
        Dest = Balloonsblank[n].transform.position - Balloons[n].transform.position;
        for (int i = 0; i < time * frame; i++) {
            GGobots[n].transform.Translate(Dest / (time * frame));
            Balloons[n].transform.Translate(Dest / (time * frame));
            yield return new WaitForSeconds(1f / frame / timeScale);
        }
        Dest = GGobotLeft.transform.position - GGobots[n].transform.position;
        for (int i = 0; i < time * frame; i++) {
            GGobots[n].transform.Translate(Dest / (time * frame));
            yield return new WaitForSeconds(1f / frame / timeScale);
        }
        QuedaUp();
    }
}
