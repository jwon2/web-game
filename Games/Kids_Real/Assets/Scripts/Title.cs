﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Title : MonoBehaviour {
    public GameObject DestGO;
    public GameObject BalloonsPar;
    private bool isGameStarting = false;

    void Awake() {
        if (!PlayerPrefs.HasKey("isSoundOn"))
            PlayerPrefs.SetFloat("isSoundOn", 1f);
        for (int i = 0; i < 10; i++)
            BalloonsPar.transform.GetChild(i).GetComponent<Animator>().SetBool("isOriginal", true);
    }

    public void GameStart() {
        if (!isGameStarting) {
            isGameStarting = true;
            StartCoroutine(CharacterMove());
        }
        else
            SceneManager.LoadScene("Story");
    }

    IEnumerator CharacterMove() {
        float time = 2f;
        float frame = 80f;
        Vector3 Dest = (DestGO.transform.position - transform.position);
        for (int i = 0; i < time * frame; i++) {
            transform.Translate(Dest / (time * frame));
            yield return new WaitForSeconds(1f / frame);
        }
        SceneManager.LoadScene("Story");
    }
}
