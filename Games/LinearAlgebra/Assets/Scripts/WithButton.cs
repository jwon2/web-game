﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class WithButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public GameObject Texts;

    public void OnPointerEnter(PointerEventData eventData)
    {
        Texts.transform.GetChild(transform.GetSiblingIndex()).gameObject.SetActive(true);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        Texts.transform.GetChild(transform.GetSiblingIndex()).gameObject.SetActive(false);
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
