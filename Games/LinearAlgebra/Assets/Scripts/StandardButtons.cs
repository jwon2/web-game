﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StandardButtons : MonoBehaviour
{
    public GameObject SoundOn_Btn, SoundOff_Btn;

    private void Awake()
    {
        if (PlayerPrefs.HasKey("Sound"))
        {
            if (PlayerPrefs.GetFloat("Sound") == 1)
            {
                SoundOn();
            }
            else if (PlayerPrefs.GetFloat("Sound") == 0)
            {
                SoundOff();
            }
        }
        else
        {
            SoundOn();
        }
    }

    public void StartNextStage() {
        DDOL ddol = GameObject.Find("DDOL").GetComponent<DDOL>();
        if (ddol.Mode == 16)
            SceneManager.LoadScene("Title");
        else {
            ddol.Mode++;
            SceneManager.LoadScene("Play");
        }
    }

    public void toTitle() {
        SceneManager.LoadScene("Title");
    }

    public void Restart() {
        SceneManager.LoadScene("Play");
    }

    public void SoundOn()
    {
        PlayerPrefs.SetFloat("Sound", 1f);
        AudioListener.volume = 1f;
        SoundOn_Btn.SetActive(false);
        SoundOff_Btn.SetActive(true);
    }

    public void SoundOff()
    {
        PlayerPrefs.SetFloat("Sound", 0f);
        AudioListener.volume = 0f;
        SoundOn_Btn.SetActive(true);
        SoundOff_Btn.SetActive(false);

    }

    public void SceneChange(string str)
    {
        SceneManager.LoadScene(str);
    }
}
