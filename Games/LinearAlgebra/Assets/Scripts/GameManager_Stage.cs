﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager_Stage : GameManager
{
    GameManager GM;

    public int stageIndex;

    private float similarity;

    public GameObject[] constellation;
    public GameObject[] constellation_Answer;
    public Vector3[] initPos = {new Vector3(),new Vector3(2f,0f,0f), new Vector3(), new Vector3(2f,0f,0f), new Vector3(),
        new Vector3(), new Vector3(), new Vector3(), new Vector3(2f,0f,0f), new Vector3(0f,-2f,0f), new Vector3(-1f,-1f,0f), new Vector3(-2f,0f,0f), new Vector3(2f,0f,0f), new Vector3(), new Vector3(2f,-2f,0f), new Vector3(-2f,0f,0f) };

    GameObject ori, ans;

    public GameObject ClearWindow, Contents;

    public GameObject ReStartButton;

    private void Awake()
    {
        GM = transform.parent.GetComponent<GameManager>();
        DDOL DDOL = GameObject.Find("DDOL").GetComponent<DDOL>();
        stageIndex = DDOL.Mode-1;
        Debug.Log("Stage Mode : " + stageIndex);

        ReStartButton.SetActive(true);
        ori =Instantiate(constellation[stageIndex], GM.Dot_Parent.transform);
        ans =Instantiate(constellation_Answer[stageIndex], GM.Answer_DotParent.transform);

        ori.transform.position = initPos[stageIndex];

        ori.transform.position = TwoDimensionlize(ori.transform.position);
        ans.transform.position = TwoDimensionlize(ans.transform.position);

        GM.Answer_DotParent = GM.Answer_DotParent.transform.GetChild(0).gameObject;
    }

    bool isCleared;

    // Start is called before the first frame update
    void Start()
    {
        isCleared = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Get_Similarity() < 0.05f)
        {
            if (!isCleared) {
                isCleared = true;
                //Clear
                StageClear();
            }
        }
    }

    void StageClear()
    {
        ClearWindow.SetActive(true);
        Contents.transform.GetChild(stageIndex).gameObject.SetActive(true);
        transform.parent.GetComponent<RankManager>().PutAndGetStageInfo(stageIndex, 0f);
    }

    float Get_Similarity()
    {
        similarity = 0;
        for (int i = 0; i < GM.Dot_Parent.transform.childCount; i++)
        {
            float error = Vector3.Distance(GM.Dot_Parent.transform.GetChild(i).position, GM.Answer_DotParent.transform.GetChild(i).position);
            error = error * error;
            similarity += error;
        }

        similarity /= GM.Dot_Parent.transform.childCount;
        return similarity;
        //Similarity_Display.text = similarity.ToString();
    }
}
