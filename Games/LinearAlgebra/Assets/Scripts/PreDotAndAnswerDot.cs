﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PreDotAndAnswerDot : MonoBehaviour
{
    public GameObject Line_Prefab;
    public GameObject[] Linked_Dot;

    public void Draw_Line()
    {
        for (int i = 0; i < Linked_Dot.Length; i++)
        {
            GameObject newLine = Instantiate(Line_Prefab, transform);
            newLine.GetComponent<DrawLine>().GO = new GameObject[2];
            newLine.GetComponent<DrawLine>().GO[0] = gameObject;
            newLine.GetComponent<DrawLine>().GO[1] = Linked_Dot[i];
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
