﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class GameManager : MonoBehaviour
{

    /* 캐싱 */
    public GameObject StartMarkPrefab, MarkLinePrefab;
    public GameObject[] PreviewPrefabs;


    public GameObject Dot_Parent, Pre_DotParent, Answer_DotParent;

    public GameObject GM_P, GM_S, GM_I;

    //UI
    public Toggle Toggle_X, Toggle_Y;
    public GameObject Size_Back;
    public Slider Size_Scaler;

    public Button[] Buttons;


    /* Values */

    float SpaceScale = 5f;
    float preFrame_anglez=float.MaxValue;

    int segmentArc = 40;

    /* Setting */

    bool isX = true;
    Operation_enum Operation_State;

    Vector3 CharacterOriPosition, MouseOriPosition, CameraOriPosition, CharacterOriScale;
    Quaternion CharacterOriRotation;
    bool isClockwiseRotate;

    GameObject StartMark, MarkLine, QuedaPreViewObject, Rotate_StandardLine, ArkLine;

    enum Operation_enum
    {
        Default, Move, Rotate, Size, Reverse, Tilt 
    }

    private void Awake()
    {
        try
        {
            DDOL DDOL = GameObject.Find("DDOL").GetComponent<DDOL>();
            Debug.Log(DDOL.Mode);
            if (DDOL.Mode == 0)
            {
                GM_P.SetActive(true);
            }
            else if (DDOL.Mode < 0)
            {
                GM_I.SetActive(true);
            }
            else
            {
                GM_S.SetActive(true);
            }
        }
        catch
        {
            Debug.LogError("It is start on Play Scene");
        }

        Operation_State = Operation_enum.Default;
    }

    // Start is called before the first frame update
    void Start()
    {
        isGetKeyDown = false;
        Dot_Parent = Dot_Parent.transform.GetChild(0).gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            Initialize_Settings();
            Operation_State = Operation_enum.Default;
        }
        CameraSizeScaling();
        
        if ((0.2074074f < Camera.main.ScreenToViewportPoint(Input.mousePosition).x && 0.7916667f> Camera.main.ScreenToViewportPoint(Input.mousePosition).x
            && 0.0503f < Camera.main.ScreenToViewportPoint(Input.mousePosition).y &&  0.8681481f> Camera.main.ScreenToViewportPoint(Input.mousePosition).y)
            ||(Operation_State==Operation_enum.Size&& 0.8305289f < Camera.main.ScreenToViewportPoint(Input.mousePosition).x && 0.9627404f > Camera.main.ScreenToViewportPoint(Input.mousePosition).x
            && 0.2074074f < Camera.main.ScreenToViewportPoint(Input.mousePosition).y && 0.8216667f > Camera.main.ScreenToViewportPoint(Input.mousePosition).y))
            SwitchOperation();

        if (MarkLine != null)
        {
            for(int i=1;i<MarkLine.GetComponent<LineRenderer>().positionCount; i++)
                MarkLine.GetComponent<LineRenderer>().SetPosition(i, GetMouseWorldPosition());
        }
        
    }

    void Initialize_Settings()
    {
        //Operation_State = Operation_enum.Default;
        Size_Back.SetActive(false);
        for(int i = 0; i < Pre_DotParent.transform.childCount; i++)
        {
            Destroy(Pre_DotParent.transform.GetChild(i).gameObject);
        }

        foreach (Button Btn in Buttons)
            Btn.interactable = true;
        if(StartMark!=null)
            Destroy(StartMark);
        if(MarkLine!=null)
            Destroy(MarkLine);
        if (Rotate_StandardLine != null)
            Destroy(Rotate_StandardLine);
        if (ArkLine != null)
            Destroy(ArkLine);
    }

    public void MoveButton()
    {
        Initialize_Settings();
        Buttons[0].interactable = false;
        Operation_State = Operation_enum.Move;
    }
    public void RotateButton()
    {
        Initialize_Settings();
        Buttons[1].interactable = false;
        Operation_State = Operation_enum.Rotate;
    }
    public void SizeButton()
    {
        Initialize_Settings();
        Buttons[2].interactable = false;

        Operation_State = Operation_enum.Size;

        for (int i = 0; i < Dot_Parent.transform.childCount; i++)
        {
            GameObject newGO = Instantiate(Dot_Parent.transform.GetChild(i).GetComponent<Dot>().Pre_Prefab, Dot_Parent.transform.GetChild(i).transform.position, new Quaternion());
            newGO.transform.SetParent(Pre_DotParent.transform, false);
            Size_Back.SetActive(true);
        }

        Draw_Preparent_Line();

    }
    public void SizeOKButton()
    {
        Hide_Image();
        for (int i = 0; i < Dot_Parent.transform.childCount; i++)
        {
            if (isX)
                Dot_Parent.transform.GetChild(i).transform.position = new Vector3(Dot_Parent.transform.GetChild(i).transform.position.x * Size_Scaler.value, Dot_Parent.transform.GetChild(i).transform.position.y);
            else
                Dot_Parent.transform.GetChild(i).transform.position = new Vector3(Dot_Parent.transform.GetChild(i).transform.position.x, Dot_Parent.transform.GetChild(i).transform.position.y * Size_Scaler.value);
        }

        Operation_State = Operation_enum.Default;
        Size_Scaler.value = 1;
        Initialize_Settings();
    }
    public void ReverseButton()
    {
        Hide_Image();
        Initialize_Settings();
        for (int i = 0; i < Dot_Parent.transform.childCount; i++)
        {
            Dot_Parent.transform.GetChild(i).transform.position = new Vector3(Dot_Parent.transform.GetChild(i).transform.position.y, Dot_Parent.transform.GetChild(i).transform.position.x);
        }
    }
    public void TiltButton()
    {
        Initialize_Settings();
        Buttons[4].interactable = false;
        Operation_State = Operation_enum.Tilt;
    }


    void SwitchOperation()
    {
        switch (Operation_State)
        {
            case Operation_enum.Default:

                OperationState_Default();
                break;

            case Operation_enum.Move:

                OperationState_Move();

                break;
            case Operation_enum.Rotate:
                OperationState_Rotate();

                break;
            case Operation_enum.Size:

                OperationState_Size();

                break;
            case Operation_enum.Reverse:
                break;

            case Operation_enum.Tilt:


                break;
        }
    }
    bool isGetKeyDown;
    void OperationState_Default()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            isGetKeyDown = true;
            CameraOriPosition = Camera.main.transform.position;
            MouseOriPosition = Camera.main.ScreenToViewportPoint(Input.mousePosition);
        }
        else if (Input.GetKey(KeyCode.Mouse0))
        {
            if(isGetKeyDown)
            {
                Vector3 DestinationPosition = CameraOriPosition - TwoDimensionlize(Camera.main.ScreenToViewportPoint(Input.mousePosition) - MouseOriPosition) * 10f;
                if (DestinationPosition.x > -SpaceScale && DestinationPosition.x < SpaceScale && DestinationPosition.y < SpaceScale && DestinationPosition.y > -SpaceScale)
                    Camera.main.transform.position = DestinationPosition;
            }
        }
        else if (Input.GetKeyUp(KeyCode.Mouse0))
        {
            isGetKeyDown = false;
        }
    }
    void OperationState_Move()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            for (int i = 0; i < Dot_Parent.transform.childCount; i++)
            {
                GameObject newGO = Instantiate(Dot_Parent.transform.GetChild(i).GetComponent<Dot>().Pre_Prefab, Dot_Parent.transform.GetChild(i).transform.position, new Quaternion());
                newGO.transform.SetParent(Pre_DotParent.transform, false);

            }
            Draw_Preparent_Line();
            StartMark = Instantiate(StartMarkPrefab, TwoDimensionlize(GetMouseWorldPosition()), new Quaternion());
            MarkLine = Instantiate(MarkLinePrefab, Vector3.zero, new Quaternion());
            MarkLine.GetComponent<LineRenderer>().SetPosition(0, GetMouseWorldPosition());
        }
        else if (Input.GetKey(KeyCode.Mouse0))
        {
            Vector3 displacement = TwoDimensionlize(Camera.main.ScreenToWorldPoint(Input.mousePosition) - StartMark.transform.position);


            for (int i = 0; i < Pre_DotParent.transform.childCount; i++)
            {
                Pre_DotParent.transform.GetChild(i).transform.position = Dot_Parent.transform.GetChild(i).transform.position + displacement;
            }


        }
        else if (Input.GetKeyUp(KeyCode.Mouse0) && StartMark != null)
        {
            Hide_Image();
            Vector3 displacement = TwoDimensionlize(GetMouseWorldPosition() - StartMark.transform.position);
            for (int i = 0; i < Dot_Parent.transform.childCount; i++)
                Dot_Parent.transform.GetChild(i).transform.position += displacement;
            Initialize_Settings();
            Operation_State = Operation_enum.Default;
        }
    }
    void OperationState_Rotate()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            for (int i = 0; i < Dot_Parent.transform.childCount; i++)
            {
                GameObject newGO = Instantiate(Dot_Parent.transform.GetChild(i).GetComponent<Dot>().Pre_Prefab, Dot_Parent.transform.GetChild(i).transform.position, new Quaternion());
                newGO.transform.SetParent(Pre_DotParent.transform, false);
            }

            Draw_Preparent_Line();
            StartMark = Instantiate(StartMarkPrefab, GetMouseWorldPosition(), new Quaternion());
            Rotate_StandardLine = Instantiate(MarkLinePrefab, Vector3.zero, new Quaternion());
            Rotate_StandardLine.GetComponent<LineRenderer>().SetPosition(0, Vector3.zero);
            Rotate_StandardLine.GetComponent<LineRenderer>().SetPosition(1, GetMouseWorldPosition());
            MarkLine = Instantiate(MarkLinePrefab, Vector3.zero, new Quaternion());
            MarkLine.GetComponent<LineRenderer>().SetPosition(0, Vector3.zero);


            ArkLine = Instantiate(MarkLinePrefab, Vector3.zero, new Quaternion());
            ArkLine.name = "Arc";
        }
        else if (Input.GetKey(KeyCode.Mouse0))
        {
            Quaternion angle = Quaternion.FromToRotation(StartMark.transform.position, GetMouseWorldPosition());

            for (int i = 0; i < Pre_DotParent.transform.childCount; i++)
            {
                Pre_DotParent.transform.GetChild(i).transform.position = angle * Dot_Parent.transform.GetChild(i).transform.position;
            }

            float anglez = angle.eulerAngles.z;
            Vector3[] arcpoint = new Vector3[segmentArc + 1];

            if (anglez > 180f)
                anglez = anglez - 360f;


            if (preFrame_anglez != float.MaxValue)
            {
                if (Mathf.Abs(preFrame_anglez) < 90f)
                {
                    if (preFrame_anglez * anglez < 0)
                    {
                        if (anglez > 0)
                            isClockwiseRotate = false;
                        else
                            isClockwiseRotate = true;
                    }
                    else if(preFrame_anglez==0)
                    {
                        if (anglez > 0)
                            isClockwiseRotate = false;
                        else
                            isClockwiseRotate = true;
                    }
                }
                
            }
            else
            {
                if (anglez > 0)
                    isClockwiseRotate = false;
                else
                    isClockwiseRotate = true;
            }
            preFrame_anglez = anglez;

            float arcsize = Mathf.Min(StartMark.transform.position.magnitude, GetMouseWorldPosition().magnitude) / 2f;
            if (arcsize > 1f)
                arcsize = 1f;

            if (!isClockwiseRotate)
            {
                if (anglez < 0)
                    anglez += 360f;
                for (int i = 0; i <= segmentArc; i++)
                {
                    arcpoint[i] = (Quaternion.Euler(new Vector3(0f, 0f, +anglez * i / segmentArc)) * StartMark.transform.position).normalized * arcsize;
                }
            }
            else
            {
                if (anglez > 0)
                    anglez -= 360f;
                for (int i = 0; i <= segmentArc; i++)
                {

                    arcpoint[i] = (Quaternion.Euler(new Vector3(0f, 0f, +anglez * i / segmentArc)) * StartMark.transform.position).normalized * arcsize;
                }
            }

            ArkLine.GetComponent<LineRenderer>().SetPositions(arcpoint);


        }
        else if (Input.GetKeyUp(KeyCode.Mouse0) && StartMark != null)
        {
            Hide_Image();
            Quaternion angle = Quaternion.FromToRotation(StartMark.transform.position, GetMouseWorldPosition());
            for (int i = 0; i < Dot_Parent.transform.childCount; i++)
                Dot_Parent.transform.GetChild(i).transform.position = angle * Dot_Parent.transform.GetChild(i).transform.position;
            Initialize_Settings();
            Operation_State = Operation_enum.Default;
            preFrame_anglez = float.MaxValue;
        }
    }
    void OperationState_Size()
    {
        if (!Input.GetKeyDown(KeyCode.Mouse0))
        {
            for (int i = 0; i < Pre_DotParent.transform.childCount; i++)
            {
                if (isX)
                    Pre_DotParent.transform.GetChild(i).transform.position = new Vector3(Dot_Parent.transform.GetChild(i).transform.position.x * Size_Scaler.value, Dot_Parent.transform.GetChild(i).transform.position.y);
                else
                    Pre_DotParent.transform.GetChild(i).transform.position = new Vector3(Dot_Parent.transform.GetChild(i).transform.position.x, Dot_Parent.transform.GetChild(i).transform.position.y * Size_Scaler.value);
            }
        }
    }
    void OperationState_Reverse()
    {

    }

    void Draw_Preparent_Line()
    {
        for (int i = 0; i < Pre_DotParent.transform.childCount; i++)
        {
            Pre_DotParent.transform.GetChild(i).GetComponent<PreDotAndAnswerDot>().Linked_Dot = new GameObject[Dot_Parent.transform.GetChild(i).GetComponent<Dot>().Linked_Dot.Length];
            for (int j=0;j < Dot_Parent.transform.GetChild(i).GetComponent<Dot>().Linked_Dot.Length; j++)
            {
                Pre_DotParent.transform.GetChild(i).GetComponent<PreDotAndAnswerDot>().Linked_Dot[j] = Pre_DotParent.transform.GetChild(Dot_Parent.transform.GetChild(i).GetComponent<Dot>().Linked_Dot[j].transform.GetSiblingIndex()).gameObject;
            }
            Pre_DotParent.transform.GetChild(i).GetComponent<PreDotAndAnswerDot>().Draw_Line();
        }
    }

    void Hide_Image()
    {
        Dot_Parent.GetComponent<SpriteRenderer>().enabled = false;
    }

    void CameraSizeScaling()
    {
        if (Input.GetAxis("Mouse ScrollWheel") < 0f && Camera.main.orthographicSize < 10)
            Camera.main.orthographicSize *= 1.05f;
        if (Input.GetAxis("Mouse ScrollWheel") > 0f && Camera.main.orthographicSize > 2.5f)
            Camera.main.orthographicSize *= 0.96f;
    }

    public void SizeToggleX()
    {
        if (!Toggle_X.isOn)
        {
            if (Toggle_Y.isOn)
            {

            }
            else
            {
                Toggle_X.isOn = true;
            }
        }
        else
        {
            if (isX)
            {
                Toggle_X.isOn = true;
            }
            else
            {
                if (Toggle_Y.isOn)
                {
                    Toggle_Y.isOn = false;
                    isX = true;
                }
            }
        }
    }
    public void SizeToggleY()
    {
        if (!Toggle_Y.isOn)
        {
            if (Toggle_X.isOn)
            {

            }
            else
            {
                Toggle_Y.isOn = true;
            }
        }
        else
        {
            if (!isX)
            {
                Toggle_Y.isOn = true;
            }
            else
            {
                if (Toggle_X.isOn)
                {
                    Toggle_X.isOn = false;
                    isX = false;
                }
            }
        }

    }
    public void SizeScaler()
    {
        if (Size_Scaler.value > 1.5f)
            Size_Scaler.value = 2f;
        else if (Size_Scaler.value > 0.75f)
            Size_Scaler.value = 1f;
        else if (Size_Scaler.value > 0.25f)
            Size_Scaler.value = 0.5f;
        else if (Size_Scaler.value > -0.25f)
            Size_Scaler.value = 0f;
        else if (Size_Scaler.value > -0.75f)
            Size_Scaler.value = -0.5f;
        else if (Size_Scaler.value > -1.5f)
            Size_Scaler.value = -1f;
        else
            Size_Scaler.value = -2f;
    }
    
    /* 블랙박스 함수들*/
    public Vector3 TwoDimensionlize(Vector3 ori)
    {
        return new Vector3(ori.x, ori.y);
    }
    
    Vector3 GetMouseWorldPosition()
    {
        return TwoDimensionlize(Camera.main.ScreenToWorldPoint(Input.mousePosition));
    }

    
}
