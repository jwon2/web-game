﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawLine : MonoBehaviour
{

    public GameObject[] GO;
    LineRenderer lr;

    // Start is called before the first frame update
    void Awake()
    {
        lr = GetComponent<LineRenderer>();
        lr.startWidth = 0.1f;
        lr.endWidth = 0.1f;
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < GO.Length; i++)
            lr.SetPosition(i, GO[i].transform.position);
    }
}
