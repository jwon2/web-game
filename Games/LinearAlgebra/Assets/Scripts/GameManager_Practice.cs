﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager_Practice : GameManager
{
    public GameObject ReStartButton;

    public GameObject[] constellation;

    GameManager GM;

    private void Awake()
    {
        Debug.Log("Practice Mode");

        GM = transform.parent.GetComponent<GameManager>();

        ReStartButton.SetActive(true);


        Instantiate(constellation[PlayerPrefs.GetInt("PracticeIndex")], GM.Dot_Parent.transform);
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void ReStart()
    {
        SceneManager.LoadScene("Play");
    }
}
