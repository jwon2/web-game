﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StageSelect : MonoBehaviour
{
    public GameObject StageWindow;
    public GameObject LeftButton;
    public GameObject RightButton;
    public GameObject Page1,Page2;
    
    enum State
    {
        page1, page2
    }
    State pageState;

    public void OpenWindow()
    {
        StageWindow.SetActive(true);
        LeftButton.SetActive(false);
        RightButton.SetActive(true);
        pageState = State.page1;
        Page1.SetActive(true);
        Page2.SetActive(false);
    }
    public void Right()
    {
        pageState = State.page2;
        Page1.SetActive(false);
        Page2.SetActive(true);
        LeftButton.SetActive(true);
        RightButton.SetActive(false);
    }
    public void Left()
    {
        pageState = State.page1;
        Page1.SetActive(true);
        Page2.SetActive(false);
        LeftButton.SetActive(false);
        RightButton.SetActive(true);
    }

    

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
