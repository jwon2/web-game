﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DDOL : MonoBehaviour
{
    public int Mode=-1;

    private static DDOL _instance = null;
    public AudioSource click;

    public static DDOL Instance {
        get {
            if (_instance == null)
                Debug.LogError("DDOL == null");
            return _instance;
        }
    }

    private void Awake() {
        _instance = null;
    }

    void Start() {
        DontDestroyOnLoad(gameObject);
    }
}
