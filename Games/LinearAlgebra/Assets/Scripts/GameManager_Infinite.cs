﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager_Infinite : GameManager
{
    GameManager GM;
    private float similarity;

    int infiniteIndex;

    public GameObject GameOverWindow;

    public GameObject ScoreUI, TimeUI;

    public GameObject[] constellation;
    public GameObject WhiteStarPrefab;

    public Text[] TimeDisplay = new Text[6];
    public Text ScoreDisplay;

    int Score;
    float remain_Time;
    int level;

    float[] level_diag = { 20f, 40f, 60f, 120f, 180f, 300f, 500f, 1000f };
    int[] count = { 1, 1, 2, 2, 2, 3, 3, 3,3 };
    float[] discount_velocity = { 1f, 1.1f, 1.2f, 1.4f, 1.6f, 1.8f, 2.0f, 2.5f, 3f};

    float timer;

    GameObject ori, ans;

    private void Awake()
    {
        Debug.Log("Infinite Mode");
        Score = 0;
        remain_Time = 40f;
        level = 0;

        TimeUI.SetActive(true);
        ScoreUI.SetActive(true);

        DDOL DDOL = GameObject.Find("DDOL").GetComponent<DDOL>();
        infiniteIndex = (-DDOL.Mode) - 1;
        GM = transform.parent.GetComponent<GameManager>();

        ori = Instantiate(constellation[infiniteIndex], GM.Dot_Parent.transform);
        ori.transform.position = new Vector3(Random.Range(0, 2) == 0 ? Random.Range(0.5f, 1.5f) : Random.Range(-0.5f, -1.5f), Random.Range(0, 2) == 0 ? Random.Range(0.5f, 1.5f) : Random.Range(-0.5f, -1.5f) );
    }
    // Start is called before the first frame update
    void Start()
    {
        isGameOver = false;
        ori.transform.position = TwoDimensionlize(ori.transform.position);

        StartCoroutine(Timer());
        for (int i = 0; i < GM.Dot_Parent.transform.childCount; i++)
        {
            Instantiate(WhiteStarPrefab, GM.Answer_DotParent.transform);
        }
        Make_AnswerStars();
    }
    IEnumerator Timer()
    {
        float timescale = 0.09f;

        yield return new WaitForSeconds(timescale);
        timer += timescale;
        for (int i = 0; i < level_diag.Length; i++)
        {
            if (timer > level_diag[i])
            {
                level = i + 1;
            }
               
        }
        remain_Time -= timescale * discount_velocity[level];
        if (remain_Time <= 0f)
        {
            remain_Time = 0f;
            GameOver();
        }
        TimeDisplay[0].text = ((((int)remain_Time / 60) / 10)).ToString();
        TimeDisplay[1].text = ((((int)remain_Time / 60) % 10)).ToString();
        TimeDisplay[2].text = (((((int)remain_Time % 60) / 10))%10).ToString();
        TimeDisplay[3].text = (((((int)remain_Time % 60) % 10))%10).ToString();
        TimeDisplay[4].text = (((((int)(remain_Time * 100) % 100) / 10))%10).ToString();
        TimeDisplay[5].text = ((((int)(remain_Time * 100) % 100) %10)).ToString();

        StartCoroutine(Timer());
    }

    // Update is called once per frame
    void Update()
    {
        if (Get_Similarity() < 0.001f)
        {
            Make_AnswerStars();
            remain_Time += 8f;
            Score += 10 * (level + 1);
        }
        else if(Get_Similarity() < 0.01f)
        {
            Make_AnswerStars();
            remain_Time += 6f;
            Score += 8 * (level + 1);
        }
        else if(Get_Similarity() < 0.05f)
        {
            Make_AnswerStars();
            remain_Time += 4f;
            Score += 6 * (level + 1);
        }

        ScoreDisplay.text = Score.ToString() + " 점";

    }

    void Make_AnswerStars()
    {
       
        for(int i = 0; i < GM.Dot_Parent.transform.childCount; i++)
        {
            WhiteStarPrefab.transform.position = GM.Dot_Parent.transform.GetChild(i).position;
        }
        int cnt = 0;
        while(cnt < count[level])
        {
            Vector3[] imsipos = new Vector3[GM.Dot_Parent.transform.childCount];
            for(int i = 0; i < GM.Dot_Parent.transform.childCount; i++)
            {
                imsipos[i] = GM.Dot_Parent.transform.GetChild(i).position;
            }

            int Operation_ran = Random.Range(0,4);
            
            switch(Operation_ran)
            {
                case 0:
                    imsipos = Move(imsipos);
                    break;
                case 1:
                    imsipos = Rotate(imsipos);
                    break;
                case 2:
                    imsipos = Size(imsipos); 
                    break;
                case 3:
                    imsipos = Reverse(imsipos);
                    break;

            }

            if (CheckInRange(imsipos))
            {
                for(int i = 0; i < GM.Dot_Parent.transform.childCount; i++)
                {
                    GM.Answer_DotParent.transform.GetChild(i).position = imsipos[i];
                }
                cnt++;
            }
        }
    }
    Vector3[] Move(Vector3[] pos)
    {
        Vector3[] ret = new Vector3[pos.Length];
        float ran_x, ran_y;
        ran_x = Random.Range(0,2)==0 ? Random.Range(1f, 3f) : Random.Range(-1f, -3f);
        ran_y = Random.Range(0, 2) == 0 ? Random.Range(1f, 3f) : Random.Range(-1f, -3f);

        for (int i = 0; i < pos.Length; i++)
        {
            ret[i] = pos[i] + new Vector3(ran_x, ran_y);
        }

        return ret;
    }
    Vector3[] Rotate(Vector3[] pos)
    {
        Vector3[] ret = new Vector3[pos.Length];
        int[] anglez = { 30, 45, 60, 90, 120, 135, 150, 180, 210, 225, 240, 270, 300, 315, 330 };
        int ran_idx = Random.Range(0, 15);

        Quaternion angle = Quaternion.Euler(new Vector3(0f, 0f, anglez[ran_idx]));

        for (int i = 0; i < pos.Length; i++)
        {
            ret[i] = angle * pos[i];
        }

        return ret;
    }
    Vector3[] Size(Vector3[] pos)
    {
        Vector3[] ret = new Vector3[pos.Length];

        bool isX = (Random.Range(0, 2) ==0);
        float[] size = { -2, -1, -0.5f, 0.5f, 2f };
        int ran_idx = Random.Range(0, 5);

        for(int i = 0; i < pos.Length; i++)
        {
            if (isX)
                ret[i] = new Vector3(size[ran_idx] * pos[i].x, pos[i].y);
            else
                ret[i] = new Vector3(pos[i].x, size[ran_idx] * pos[i].y);
        }

        return ret;
    }
    Vector3[] Reverse(Vector3[] pos)
    {
        Vector3[] ret = new Vector3[pos.Length];
        for (int i = 0; i < pos.Length; i++)
        {
            ret[i] = new Vector3(pos[i].y, pos[i].x);
        }
        return ret;

    }
    bool CheckInRange(Vector3[] pos)
    {
        bool ret = true;

        for(int i = 0; i < pos.Length; i++)
        {
            if (pos[i].x < -14 || pos[i].x > 14 || pos[i].y < -12 || pos[i].y > 12)
                ret = false;
        }

        return ret;
    }

    bool isGameOver;
    void GameOver()
    {
        if (!isGameOver) {
            isGameOver = true;
            StopCoroutine(Timer());
            GameOverWindow.SetActive(true);
            transform.parent.GetComponent<RankManager>().PutAndGetInfiniteRankInfo(Score, timer);
        }
    }

    float Get_Similarity()
    {
        similarity = 0;
        for (int i = 0; i < GM.Dot_Parent.transform.childCount; i++)
        {
            float error = Vector3.Distance(GM.Dot_Parent.transform.GetChild(i).position, GM.Answer_DotParent.transform.GetChild(i).position);
            error = error * error;
            similarity += error;
        }

        similarity /= GM.Dot_Parent.transform.childCount;
        return similarity;
        //Similarity_Display.text = similarity.ToString();
    }
}
