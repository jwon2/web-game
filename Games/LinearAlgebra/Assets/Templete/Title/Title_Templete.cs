﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Title_Templete : MonoBehaviour
{
    public GameObject SoundOn_Btn, SoundOff_Btn;

    public GameObject HelpWindow;
    public GameObject HelpContentsPar;
    private GameObject[] HelpContents;
    public GameObject LeftButton, RightButton;
    private int HelpContentsNum;

    int Help_state;

    private void Awake()
    {
        HelpContentsNum = HelpContentsPar.transform.childCount;
        HelpContents = new GameObject[HelpContentsNum];
        for (int i = 0; i < HelpContentsNum; i++)
            HelpContents[i] = HelpContentsPar.transform.GetChild(i).gameObject;
        if (PlayerPrefs.HasKey("Sound"))
        {
            if (PlayerPrefs.GetFloat("Sound") == 1)
            {
                SoundOn();
            }
            else if(PlayerPrefs.GetFloat("Sound")==0)
            {
                SoundOff();
            }
        }
        else
        {
            SoundOn();
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SoundOn()
    {
        PlayerPrefs.SetFloat("Sound", 1f);
        AudioListener.volume = 1f;
        SoundOn_Btn.SetActive(false);
        SoundOff_Btn.SetActive(true);
    }

    public void SoundOff()
    {
        PlayerPrefs.SetFloat("Sound", 0f);
        AudioListener.volume = 0f;
        SoundOn_Btn.SetActive(true);
        SoundOff_Btn.SetActive(false);

    }

    public void Help_Open()
    {
        HelpWindow.SetActive(true);
        for (int i = 0; i < HelpContents.Length; i++)
        {
            HelpContents[i].SetActive(false);
        }
        Help_state = 0;
        HelpContents[0].SetActive(true);
        LeftButton.SetActive(false);
        RightButton.SetActive(true);
    }
    public void Left()
    {
        HelpContents[Help_state--].SetActive(false);
        HelpContents[Help_state].SetActive(true);
        if (Help_state == 0)
            LeftButton.SetActive(false);
        else
            LeftButton.SetActive(true);

        if (Help_state == HelpContents.Length - 1)
            RightButton.SetActive(false);
        else
            RightButton.SetActive(true);
    }
    public void Right()
    {
        HelpContents[Help_state++].SetActive(false);
        HelpContents[Help_state].SetActive(true);
        if (Help_state == HelpContents.Length - 1)
            RightButton.SetActive(false);
        else
            RightButton.SetActive(true);

        if (Help_state == 0)
            LeftButton.SetActive(false);
        else
            LeftButton.SetActive(true);
    }
    public void GameStart(int n)
    {
        DDOL DDOL = GameObject.Find("DDOL").GetComponent<DDOL>();
        if (n < 0)
            DDOL.Mode = n;
        else if (n == 0)
        {
            DDOL.Mode = 0;

        }
        else
            DDOL.Mode = n;
        SceneManager.LoadScene("Play");
    }
    public void PracticeStart(int n)
    {
        PlayerPrefs.SetInt("PracticeIndex", n);

        DDOL DDOL = GameObject.Find("DDOL").GetComponent<DDOL>();
        DDOL.Mode = 0;
        SceneManager.LoadScene("Play");
    }
}
