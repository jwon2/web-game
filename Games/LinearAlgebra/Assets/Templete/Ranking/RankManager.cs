﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System;
using UnityEngine.UI;

public class RankManager : MonoBehaviour {
    public const string gameName = "StarRoad";
    
    public GameObject GameOverRankBox;
    public GameObject RankWindow;
    public GameObject RankDataWindow;
    public GameObject[] RankBoxTop5 = new GameObject[5];
    public GameObject MyRankBoxWithTop5;
    public GameObject DDD;
    public GameObject Loading;
    public GameObject MyLoading;
    public GameObject[] checkStage = new GameObject[16];
    public Button[] PracticeSelectButton = new Button[16];
    public Button[] InfinitySelectButton = new Button[16];

    private bool[] isClearedStage = new bool[16];

    // UserData 저장용 구조체
    struct UserData {
        public string host;
        public string userid;
        public string nickname;
        public string token;
        public string closeUrl;
    }

    public void SetUserData(string data) {
        UserJsonData = data;

        user = JsonUtility.FromJson<UserData>(UserJsonData);
    }

    // UserData 받아올 JSON과 구조체
    public string UserJsonData;
    UserData user = new UserData();

    // 시작하면서 UserData 받아오고 저장
    void Start() {
        LoadData();
        GetStageRankInfo();
    }

    void LoadData() {
        Application.ExternalCall("SetUserData");

        // JSON Parsing
        user = JsonUtility.FromJson<UserData>(UserJsonData);
    }

    public void GameClose() {
        Application.OpenURL(user.closeUrl);
    }

    [System.Serializable]
    struct Badges
    {
        public Badge winner;
    }

    [System.Serializable]
    struct Badge
    {
        public int level;
    }

    [System.Serializable]
    struct User
    {
        public string userId;
        public string nickname;
        public Badges badges;
    }

    [System.Serializable]
    struct Ranking
    {
        public RankData my;
        public List<RankData> ranking;
    }

    // RankData 저장할 구조체
    [System.Serializable]
    struct RankData
    {
        public User user;
        public int rank;        // 등수
        public int score;       // 점수
        public float time;     // 시간
        public string nickname; // 닉네임
        public int level;       // 레벨 (깨봉홈페이지 레벨)
    }

    // 상위 5등과 자신의 RankData 저장할 구조체
    RankData[] Top5 = new RankData[5];

    RankData MyRank;

    public void GetStageRankInfo() {
        if (string.IsNullOrEmpty(user.token))
            LoadData();

        StartCoroutine(GetStageRanking(user.token));
    }

    private IEnumerator GetStageRanking(string token) {
        string url = user.host + "/user/v1/games/" + gameName + "_stage";

        using (UnityWebRequest w = UnityWebRequest.Get(url)) {
            w.SetRequestHeader("Authorization", "Bearer " + token);

            yield return w.SendWebRequest();

            if (w.isHttpError || w.isNetworkError) {
                //TODO handle error
            } else {
                //success
                Ranking r = JsonUtility.FromJson<Ranking>(w.downloadHandler.text);

                PlayerPrefs.SetInt("stageInt", r.my.score);
                isClearedStage = intToBoolArr(r.my.score);
                for (int i = 0; i < 16; i++) {
                    checkStage[i].SetActive(isClearedStage[i]);
                    if (i != 0) {
                        PracticeSelectButton[i].interactable = isClearedStage[i];
                        InfinitySelectButton[i].interactable = isClearedStage[i];
                    }
                }
            }
        }
    }
    
    public void PutAndGetStageInfo(int clearIdx, float time) {
        isClearedStage = intToBoolArr(PlayerPrefs.GetInt("stageInt"));
        isClearedStage[clearIdx] = true;
        if (string.IsNullOrEmpty(user.token))
            LoadData();

        StartCoroutine(_PutAndGetStageInfo(user.token, boolArrToInt(isClearedStage), time));
    }

    private IEnumerator _PutAndGetStageInfo(string token, int stageInt, float time) {
        string url = user.host + "/user/v1/games/" + gameName + "_stage/users/" + user.userid;
        string data = "{\"score\":" + stageInt + ",\"time\":" + time + "}";
        
        using (UnityWebRequest w = UnityWebRequest.Put(url, data)) {
            w.SetRequestHeader("Authorization", "Bearer " + token);
            w.SetRequestHeader("Content-Type", "application/json");
            // Debug.Log(url + "\n\n" + data);
            yield return w.SendWebRequest();

            if (w.isHttpError || w.isNetworkError) {
                //TODO handle error
            } else {
                //sucess
                MyRank = JsonUtility.FromJson<RankData>(w.downloadHandler.text);

                isClearedStage = intToBoolArr(MyRank.score);
            }
        }
    }

    private int boolArrToInt(bool[] stageArr) {
        int stageInt = 0;
        for (int i = 0; i < 16; i++)
            if (stageArr[i])
                stageInt += 1 << i;
        return stageInt;
    }

    private bool[] intToBoolArr(int stageInt) {
        bool[] stageArr = new bool[16];
        for (int i = 0; i < 16; i++)
            stageArr[i] = ((stageInt >> i) & 1) == 1;
        return stageArr;
    }
    
    public void GetInfiniteRankInfo() {
        Loading.SetActive(true);
        RankDataWindow.SetActive(false);

        if (string.IsNullOrEmpty(user.token))
            LoadData();

        StartCoroutine(GetInfiniteRanking(user.token));
    }

    private IEnumerator GetInfiniteRanking(string token) {
        string url = user.host + "/user/v1/games/" + gameName;

        using (UnityWebRequest w = UnityWebRequest.Get(url)) {
            w.SetRequestHeader("Authorization", "Bearer " + token);
            
            yield return w.SendWebRequest();

            if (w.isHttpError || w.isNetworkError) {
                //TODO handle error
            } else {
                //success
                Ranking r = JsonUtility.FromJson<Ranking>(w.downloadHandler.text);
                
                MyRank = r.my;
                MyRank.nickname = r.my.user.nickname;
                MyRank.level = r.my.user.badges.winner.level;

                Loading.SetActive(false);
                RankDataWindow.SetActive(true);

                int size = Math.Min(r.ranking.Count, 5);
                int i = 0;
                for (i = 0; i < size; i++) {
                    Top5[i] = r.ranking[i];
                    Top5[i].nickname = r.ranking[i].user.nickname;
                    Top5[i].level = r.ranking[i].user.badges.winner.level;
                }
                if (i < 5) {
                    for (int j = i; j < 5; j++) {
                        Top5[j] = new RankData();
                    }
                }
                for (i = 0; i < 5; i++) {
                    RankBoxTop5[i].GetComponent<RankBox>().SetRankBox(Top5[i].rank, Top5[i].score, Top5[i].time, Top5[i].nickname, Top5[i].level);
                    RankBoxTop5[i].transform.GetChild(0).gameObject.SetActive(false);
                }

                if (1 <= MyRank.rank && MyRank.rank <= 5) {
                    RankBoxTop5[MyRank.rank - 1].transform.GetChild(0).gameObject.SetActive(true);
                    MyRankBoxWithTop5.SetActive(false);
                    DDD.SetActive(false);
                } else {
                    MyRankBoxWithTop5.GetComponent<RankBox>().SetRankBox(MyRank.rank, MyRank.score, MyRank.time, MyRank.nickname, MyRank.level);
                    MyRankBoxWithTop5.SetActive(true);
                    DDD.SetActive(true);
                }
            }
        }
    }
    
    public void PutAndGetInfiniteRankInfo(int score, float time) {
        MyLoading.SetActive(true);
        GameOverRankBox.SetActive(false);

        if (string.IsNullOrEmpty(user.token)) {
            LoadData();

            //not authorized
            return;
        }

        StartCoroutine(PutAndGetInfiniteRanking(user.token, score, time));
    }

    private IEnumerator PutAndGetInfiniteRanking(string token, int score, float time) {
        string url = user.host + "/user/v1/games/" + gameName + "/users/" + user.userid;
        string data = "{\"score\":" + score + ",\"time\":" + time + "}";
        
        using (UnityWebRequest w = UnityWebRequest.Put(url, data)) {
            w.SetRequestHeader("Authorization", "Bearer " + token);
            w.SetRequestHeader("Content-Type", "application/json");
            // Debug.Log(url + "\n\n" + data);
            yield return w.SendWebRequest();

            if (w.isHttpError || w.isNetworkError) {
                //TODO handle error
            } else {
                //sucess
                MyRank = JsonUtility.FromJson<RankData>(w.downloadHandler.text);

                //success
                RankData r = JsonUtility.FromJson<RankData>(w.downloadHandler.text);
                
                MyRank.nickname = r.user.nickname;
                MyRank.level = r.user.badges.winner.level;

                MyLoading.SetActive(false);
                GameOverRankBox.SetActive(true);

                if (MyRank.score <= score)
                    GameOverRankBox.GetComponent<RankBox>().SetRankBox(MyRank.rank, score, time, MyRank.nickname, MyRank.level);
                else
                    GameOverRankBox.GetComponent<RankBox>().SetRankBox(0, score, time, MyRank.nickname, MyRank.level);
            }
        }
    }
}