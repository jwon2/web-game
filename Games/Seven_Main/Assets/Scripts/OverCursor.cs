﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OverCursor : MonoBehaviour
{
    int count;

    private void Awake()
    {
        count = 0;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {   if (count++ == 1)
        {
            if(FindObjectsOfType<OverCursor>().Length==1)
                Cursor.visible = true;
            Destroy(gameObject);
        }
    }
}
