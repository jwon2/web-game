﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : MonoBehaviour
{
    public int label;
    public int index;
    int color;

    public GameObject InterruptBlock;

    public GameObject[] Line_Prefab;
    public GameObject[] ColorBlocks;

    public GameObject Shield;

    private GameManager GM;

    public GameObject Cursor_Image;

    public GameObject Pop_Effect;

    Block B;

    private void Awake()
    {
        GM = GameObject.Find("GameManager").GetComponent<GameManager>();
        color = Random.Range(0, 4);
        ColorBlocks[color].SetActive(true);
        StartCoroutine(Destroy_Shield_Delay());
    }

    public void Destroy_Shield()
    {
        StopCoroutine(Destroy_Shield_Delay());
        Destroy(Shield);
        tag = "Block";
    }
    IEnumerator Destroy_Shield_Delay()
    {
        yield return new WaitForSeconds(5f);
        Destroy_Shield();
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        Make_Block(collision.gameObject);
    }
    public void Make_Block(GameObject other)
    {
        if (tag == "Block" && other.tag == "Block")
        {
            B = other.GetComponent<Block>();
            if (label < B.label)
            {
                Debug.Log(index + "  " + B.index + " " + GM.Get_GoalNum());
                if (index + B.index < GM.Get_GoalNum())
                {
                    Make_CombineBlock(other);
                }
                else if (B.index + index == GM.Get_GoalNum())
                {

                    Make_GoalBlock(other);
                }
                else
                {
                    Make_InterruptBlock(other);
                }
            }
        }
    }
    void Make_CombineBlock(GameObject other)
    {
        GameObject NewBlock = Instantiate(GM.Block_Prefabs[index + B.index - 1], GetMidPositionOfTwoObjects(gameObject, other), new Quaternion());
        NewBlock.transform.SetParent(GM.Blocks_Parent.transform, false);
        NewBlock.GetComponent<Block>().label = GM.Get_Label();
        NewBlock.GetComponent<Block>().Destroy_Shield();
        NewBlock.GetComponent<Rigidbody2D>().velocity = GetAverageVelocity(gameObject, other);
        Destroy(other);
        Destroy(gameObject);
    }
    void Make_GoalBlock(GameObject other)
    {
        /*
         * 애니메이션 추가 필요 
         */
        Instantiate(Pop_Effect, GetMidPositionOfTwoObjects(gameObject, other), new Quaternion());

        GM.Block_pop_AS.Stop();
        GM.Block_pop_AS.Play();

        GM.Gain_Score(1);
        Destroy(other);
        Destroy(gameObject);
    }
    void Make_InterruptBlock(GameObject other)
    {
        GameObject NewBlock = Instantiate(InterruptBlock, GetMidPositionOfTwoObjects(gameObject, other), new Quaternion());
        NewBlock.transform.SetParent(GM.Blocks_Parent.transform, false);
        NewBlock.GetComponent<Rigidbody2D>().velocity = GetAverageVelocity(gameObject, other);
        NewBlock.GetComponent<InterruptBlock>().label = GM.Get_Label();
        Destroy(other);
        Destroy(gameObject);
    }

    public static Vector2 GetMidPositionOfTwoObjects(GameObject GO1, GameObject GO2)
    {
        return (GO1.transform.position + GO2.transform.position) / 2f;
    }
    public static Vector2 GetAverageVelocity(GameObject GO1, GameObject GO2)
    {
        return (GO1.GetComponent<Rigidbody2D>().velocity + GO2.GetComponent<Rigidbody2D>().velocity) / 2f;   
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Wall_v")
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(-GetComponent<Rigidbody2D>().velocity.x, GetComponent<Rigidbody2D>().velocity.y);
        }
        if (collision.tag == "Wall_h")
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x, -GetComponent<Rigidbody2D>().velocity.y);
        }
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Wall_v" || collision.gameObject.tag == "Wall_h")
        {

            GetComponent<Rigidbody2D>().AddForce(-transform.position);
        }
    }

    /*
    private void OnMouseOver()
    {
        Cursor.visible = false;
        Instantiate(Cursor_Image, Camera.main.ScreenToWorldPoint(Input.mousePosition)+new Vector3(0f,0f,10f), new Quaternion());
        
    }
    */

    GameObject Line;
    private void OnMouseDown()
    {
        Destroy_Shield();
        Line = Instantiate(Line_Prefab[color], transform.position, new Quaternion());
        Line.transform.position = Vector3.zero;
        Vector3 From, To;
        From = new Vector3(1f, 0f, 0f);
        To = new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x,
            Camera.main.ScreenToWorldPoint(Input.mousePosition).y,0f) - transform.position;
        Line.transform.rotation = Quaternion.FromToRotation(From, To);
        float Dis = Vector3.Distance(new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x,
            Camera.main.ScreenToWorldPoint(Input.mousePosition).y, 0f), transform.position);
        Vector3 Scale = new Vector3(Dis, 6f, 1f);
        Line.transform.localScale = Scale;
        Line.transform.SetParent(transform, false);
    }
    private void OnMouseDrag()
    {
        if (Line != null)
        {
            Line.transform.position = transform.position;
            Vector3 From, To;
            From = new Vector3(1f, 0f, 0f);
            To = new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x,
                Camera.main.ScreenToWorldPoint(Input.mousePosition).y, 0f) - transform.position;
            Line.transform.rotation = Quaternion.FromToRotation(From, To);
            float Dis = Vector3.Distance(new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x,
                Camera.main.ScreenToWorldPoint(Input.mousePosition).y, 0f), transform.position);
            Vector3 Scale = new Vector3(Dis, 6f, 1f);
            Line.transform.localScale = Scale;
        }
    }
    public void OnMouseUp()
    {
        if (Line != null)
            Destroy(Line.gameObject);
    }

}
