﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InterruptBlock : MonoBehaviour
{
    public int label;
    public int index;

    public GameObject Line_Prefab;

    public GameObject[] InterruptBlock_Prefabs;

    private GameManager GM;

    InterruptBlock IB;

    private void Awake()
    {
        GM = GameObject.Find("GameManager").GetComponent<GameManager>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(tag == "InterruptBlock" && collision.gameObject.tag == "InterruptBlock")
        {
            IB = collision.gameObject.GetComponent<InterruptBlock>();
            if(label < IB.label)
            {
                if(index ==0 && IB.index == 0)
                {
                    Make_StrongInterruptBlock(collision.gameObject);
                }
                else
                {
                    Make_GameOverInterruptBlock(collision.gameObject);
                }
            }
        }
    }
    void Make_StrongInterruptBlock(GameObject other)
    {
        GameObject newIBlock = Instantiate(InterruptBlock_Prefabs[0], 
            Block.GetMidPositionOfTwoObjects(gameObject,other), new Quaternion());
        newIBlock.transform.SetParent(GM.Blocks_Parent.transform, false);
        newIBlock.GetComponent<Rigidbody2D>().velocity = Block.GetAverageVelocity(gameObject,other);
        newIBlock.GetComponent<InterruptBlock>().label = GM.Get_Label();
        Destroy(other);
        Destroy(gameObject);
    }
    void Make_GameOverInterruptBlock(GameObject other)
    {
        GameObject newIBlock = Instantiate(InterruptBlock_Prefabs[1],
            Block.GetMidPositionOfTwoObjects(gameObject, other), new Quaternion());
        //newIBlock.transform.SetParent(GM.Blocks_Parent.transform, false);
        GM.GameOver_AS.Play();
        GM.GameOver();
        Destroy(other);
        Destroy(gameObject);
    }

    GameObject line;

    GameObject Line;
    private void OnMouseDown()
    {
        Line = Instantiate(Line_Prefab, transform.position, new Quaternion());
        Line.transform.position = Vector3.zero;
        Vector3 From, To;
        From = new Vector3(1f, 0f, 0f);
        To = new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x,
            Camera.main.ScreenToWorldPoint(Input.mousePosition).y, 0f) - transform.position;
        Line.transform.rotation = Quaternion.FromToRotation(From, To);
        float Dis = Vector3.Distance(new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x,
            Camera.main.ScreenToWorldPoint(Input.mousePosition).y, 0f), transform.position);
        Vector3 Scale = new Vector3(Dis, 6f, 1f);
        Line.transform.localScale = Scale;
        Line.transform.SetParent(transform, false);
    }
    private void OnMouseDrag()
    {
        if (Line != null)
        {
            Line.transform.position = transform.position;
            Vector3 From, To;
            From = new Vector3(1f, 0f, 0f);
            To = new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x,
                Camera.main.ScreenToWorldPoint(Input.mousePosition).y, 0f) - transform.position;
            Line.transform.rotation = Quaternion.FromToRotation(From, To);
            float Dis = Vector3.Distance(new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x,
                Camera.main.ScreenToWorldPoint(Input.mousePosition).y, 0f), transform.position);
            Vector3 Scale = new Vector3(Dis, 6f, 1f);
            Line.transform.localScale = Scale;
        }
    }
    public void OnMouseUp()
    {
        if (Line != null)
            Destroy(Line.gameObject);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Wall_v")
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(-GetComponent<Rigidbody2D>().velocity.x, GetComponent<Rigidbody2D>().velocity.y);
        }
        if (collision.tag == "Wall_h")
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x, -GetComponent<Rigidbody2D>().velocity.y);
        }
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Wall_v" || collision.gameObject.tag == "Wall_h")
        {

            GetComponent<Rigidbody2D>().AddForce(-transform.position);
        }
    }
}
