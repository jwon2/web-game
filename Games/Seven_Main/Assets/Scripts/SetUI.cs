﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SetUI : GameManager
{
    public Text Score_Text,Level_Text;
    public GameObject SoundOn_Btn, SoundOff_Btn;

    private void Awake()
    {
        Score_Text.text = "0 점";
        Level_Text.text = "LEVEL 1";
        Set_Sound();
    }
    void Set_Sound()
    {
        if (PlayerPrefs.GetFloat("Sound") == 1f)
        {
            SoundOn();
        }
        else
        {
            SoundOff();
        }
    }

    public void Set_Score()
    {
        Score_Text.text = GetComponent<GameManager>().Get_Score().ToString() + " 점";
    }
    public void Set_Level()
    {
        if(GetComponent<GameManager>().level>0)
            Level_Text.text = "LEVEL "+GetComponent<GameManager>().level.ToString();
    }

    public void SoundOn()
    {
        PlayerPrefs.SetFloat("Sound", 1f);
        AudioListener.volume = 1f;
        SoundOff_Btn.SetActive(true);
        SoundOn_Btn.SetActive(false);
    }
    public void SoundOff()
    {

        PlayerPrefs.SetFloat("Sound", 0f);
        AudioListener.volume = 0f;
        SoundOff_Btn.SetActive(false);
        SoundOn_Btn.SetActive(true);
    }


    public void Restart()
    {
        SceneManager.LoadScene("Play");
    }
    public void ToTitle()
    {

        SceneManager.LoadScene("Title");
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
