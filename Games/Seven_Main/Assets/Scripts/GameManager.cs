﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    /* 캐싱 */
    public GameObject[] Block_Prefabs;

    public GameObject Block_Outline;
    public GameObject Blocks_Parent;

    public GameObject RankWindow;

    public AudioSource Block_pop_AS;
    public AudioSource GameOver_AS;

    public GameObject Goal_Num_Display7, Goal_Num_Display10;
    
    /* 값 */
    int score;
    int goalNum;
    public int latestLabel;
    public int level;
    int createdBlock;


    /* 읽기전용 */
    readonly int[] set_7 = { 4, 3, 2, 1, 1, 1 }, set_10 = {5,4,3,2,1,1,1,1,1};
    List<int> set;
    readonly int[] level_stair = {5,10,15,20,30,40,50,70,100,150,200,300,400,500,1000,1500,2000,2500,3000 };
    readonly float[] regen_time = {0.5f,2.5f,2.0f,1.8f,1.6f,1.4f,1.3f,1.2f,1.1f,1.0f,0.9f,0.8f,0.7f,0.6f,0.5f,0.4f,0.3f,0.25f,0.2f };

    private void Awake()
    {
        goalNum = PlayerPrefs.GetInt("Goal");
        level = 0;
        score = 0;
        latestLabel = 0;
        createdBlock = 0;
        Set_Init();

        if (goalNum == 7)
            Goal_Num_Display7.SetActive(true);
        else
            Goal_Num_Display10.SetActive(true);
    }
    void Start()
    {
        current_time = 0f;
        StartCoroutine(Timer());
        StartCoroutine(Make_Block());
    }
    void Update()
    {
    }

    IEnumerator Make_Block()
    {

        int ran = Get_ran_AboutSet();

        Vector3 Make_Position = new Vector3(Random.Range(-2f, 2f), Random.Range(-2f, 2f), 0f);
        yield return new WaitForSeconds(regen_time[level] - 0.5f);
        GameObject outline = Instantiate(Block_Outline, Make_Position, new Quaternion());
        yield return new WaitForSeconds(0.5f);
        Destroy(outline.gameObject);
        GameObject newBlock = Instantiate(Block_Prefabs[ran], Make_Position, new Quaternion(0f, 0f, 0f, 1f));
        float x = Random.value;
        float y = Mathf.Sqrt(1 - x * x);
        newBlock.GetComponent<Rigidbody2D>().velocity = new Vector2(Random.Range(0, 2) == 0 ? x : -x, Random.Range(0, 2) == 0 ? y : -y) * 0.2f; 
        newBlock.GetComponent<Block>().label = Get_Label();
        newBlock.transform.SetParent(Blocks_Parent.transform, false);

        createdBlock++;
        Check_Level();
        StartCoroutine(Make_Block());
        
    }
    int Get_ran_AboutSet()
    {
        int ran;
        while (true)
        {
            ran = set[Random.Range(0, set.Count)];
            if (set.Contains(ran))
            {
                set.Remove(ran);
                if (set.Count == 0)
                    Set_Init();
                break;
            }
        }
        return ran;
    }

    public int Get_Label()
    {
        return latestLabel++;
    }
    public int Get_Score()
    {
        return score;
    }
    public int Get_GoalNum()
    {
        return goalNum;
    }
    public void Gain_Score(int n)
    {
        score += n;
        GetComponent<SetUI>().Set_Score();
    }
    void Check_Level()
    {
        for(int i = 0; i < level_stair.Length; i++)
        {
            if (createdBlock > level_stair[i])
            {
                level = i+1;
                GetComponent<SetUI>().Set_Level();
            }
        }
    }
    void Set_Init()
    {
        set = new List<int>();
        int[] Set_templete;

        if (goalNum == 7)
            Set_templete = set_7;
        else
            Set_templete = set_10;

            for (int j = 0; j < Set_templete.Length; j++)
            {
                for (int i = 0; i < Set_templete[j]; i++)
                {
                    set.Add(j);
                }
            }

    }

    public void GameOver()
    {
        Destroy(Blocks_Parent);
        StopAllCoroutines();

        StartCoroutine(GameOver_Delay());
    }

    IEnumerator GameOver_Delay()
    {
        yield return new WaitForSeconds(1f);

        RankWindow.SetActive(true);
        if (goalNum == 7)
            GetComponent<RankManager>().PutAndGetRankInfo(score, current_time);
        else
            GetComponent<RankManager>().PutAndGetRankInfo_2(score, current_time);
    }

    float current_time;
    IEnumerator Timer() {
        current_time += 0.02f;
        yield return new WaitForSeconds(0.02f);
        StartCoroutine(Timer());
    }
}
