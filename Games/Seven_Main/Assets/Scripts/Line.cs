﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Line : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Shield"){
            collision.GetComponent<Block>().Destroy_Shield();
        }


        if (collision.GetComponent<CircleCollider2D>().radius + transform.parent.GetComponent<CircleCollider2D>().radius > Vector2.Distance(transform.parent.position, collision.transform.position))
        {
            collision.GetComponent<Block>().Make_Block(transform.parent.gameObject);
            transform.parent.GetComponent<Block>().Make_Block(collision.gameObject);
        }
        else
        {
            collision.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            transform.parent.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            collision.GetComponent<Rigidbody2D>().AddForce((transform.parent.position - collision.transform.position).normalized * 150f);
            transform.parent.GetComponent<Rigidbody2D>().AddForce((collision.transform.position - transform.parent.position).normalized * 150f);

            Destroy(gameObject);
        }
    }

}
