﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InterruptBlock : MonoBehaviour
{

    public GameObject Line_Prefab;
    public int index;
    public int label;

    public GameObject GrayBlock;
    public GameObject BlackBlock;
    private GameManager GM;
    private void Awake()
    {
        GM = GameObject.FindObjectOfType<GameManager>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {

        if (collision.gameObject.tag == "BadBlock")
        {
            InterruptBlock BB = collision.gameObject.GetComponent<InterruptBlock>();
            if (label < BB.label)
            {
                if (BB.index == 0 && index == 0)
                {
                    GameObject newBlock = Instantiate(GrayBlock, (transform.position + collision.transform.position) / 2, new Quaternion());
                    newBlock.tag = "BadBlock";
                    newBlock.transform.SetParent(GM.Blocks.transform, false);
                    newBlock.GetComponent<InterruptBlock>().label = GM.Now_label++;
                    newBlock.GetComponent<Rigidbody2D>().velocity = (collision.gameObject.GetComponent<Rigidbody2D>().velocity + GetComponent<Rigidbody2D>().velocity) / 2f;
                    Destroy(collision.gameObject);
                    Destroy(gameObject);
                }
                else
                {
                    GameObject newBlock = Instantiate(BlackBlock, (transform.position + collision.transform.position) / 2, new Quaternion());
                    newBlock.tag = "BadBlock";
                    newBlock.transform.SetParent(GM.Blocks.transform, false);
                    newBlock.GetComponent<InterruptBlock>().label = GM.Now_label++;
                    newBlock.GetComponent<Rigidbody2D>().velocity = (collision.gameObject.GetComponent<Rigidbody2D>().velocity + GetComponent<Rigidbody2D>().velocity) / 2f;
                    Destroy(collision.gameObject);
                    Destroy(gameObject);
                    GM.Gameover();

                    newBlock.transform.SetParent(GM.Blocks.transform, false);
                }
            }
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Wall_v")
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(-GetComponent<Rigidbody2D>().velocity.x, GetComponent<Rigidbody2D>().velocity.y);
        }
        if (collision.gameObject.tag == "Wall_h")
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x, -GetComponent<Rigidbody2D>().velocity.y);
        }
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Wall_v" || collision.gameObject.tag == "Wall_h")
        {

            GetComponent<Rigidbody2D>().AddForce(-transform.position);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    

    GameObject line;

    private void OnMouseDown()
    {
        line = Instantiate(Line_Prefab, transform.position, new Quaternion());

        line.transform.rotation = Quaternion.FromToRotation(new Vector3(1f, 0f, 0f), new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y, 0f) - transform.position);
        line.transform.localScale = new Vector3(Vector3.Distance(new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y, 0f), transform.position), 1f, 1f);
        line.transform.SetParent(transform, false);

    }

    private void OnMouseDrag()
    {
        if (line != null)
        {
            line.transform.position = transform.position;
            line.transform.rotation = Quaternion.FromToRotation(new Vector3(1f, 0f, 0f), new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y, 0f) - transform.position);
            line.transform.localScale = new Vector3(Vector3.Distance(new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y, 0f), transform.position), 1f, 1f);
        }
    }
    private void OnMouseUp()
    {
        Destroy(line.gameObject);

    }

}
