﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public int Score;
    public int Now_label;

    List<int> Set;

    public Text Score_Text,Level_Text;

    public GameObject[] Block_Prefabs = new GameObject[6];

    public GameObject Blocks;

    public GameObject Block_Outline;

    public Dropdown goal_num;
    

    int level;
    struct Compostion
    {
        public int[] a;
        public Compostion(int[] a)
        {
            this.a = a;
        }
    }

    readonly Compostion[] Set_composition = { new Compostion(new int[]{ 2, 1 }), new Compostion(new int[] { 3, 2, 1 }), new Compostion(new int[] { 3, 2, 1, 1 }), new Compostion(new int[] { 3, 2, 1, 1, 1 })
            ,new Compostion(new int[] { 4, 3, 2, 1, 1, 1 }), new Compostion(new int[]{ 4, 3, 2, 1, 1, 1, 1 }), new Compostion(new int[]{ 5, 4, 3, 2, 1, 1, 1, 1 }), new Compostion(new int[]{ 5, 4, 3, 2, 1, 1, 1, 1, 1 })
            , new Compostion(new int[]{ 6, 5, 4, 3, 2, 1, 1, 1, 1, 1 }), new Compostion(new int[]{ 6, 5, 4, 3, 2, 1, 1, 1, 1, 1, 1 } )};
    readonly int[] level_stair = { 0, 5, 10, 15, 20, 30, 40, 60, 80, 100 };
    readonly float[] regen_time = { 2f, 1.8f, 1.6f, 1.4f, 1.2f, 1.1f, 1.0f, 0.9f, 0.8f,0.7f };

    private void Awake()
    {
        Debug.Log(goal_num.value);
        if (PlayerPrefs.HasKey("Goal"))
            goal_num.value = PlayerPrefs.GetInt("Goal");
        else
            PlayerPrefs.SetInt("Goal",goal_num.value);
        Set = new List<int>();
        Set_Init();
    }

    // Start is called before the first frame update
    void Start()
    {
        Score = 0;
        Now_label = 0;
        StartCoroutine(Make_Block());
        level = 0;
    }

    IEnumerator Make_Block()
    {
        int ran;
        while (true)
        {
            ran = Set[Random.Range(0, Set.Count)];
            if (Set.Contains(ran))
            {
                Set.Remove(ran);
                if (Set.Count == 0)
                    Set_Init();
                break;
            }
        }
        Vector3 Make_Position = new Vector3(Random.Range(-3f, 3f), Random.Range(-3f, 3f), 0f);
        yield return new WaitForSeconds(0.5f);
        GameObject outline =  Instantiate(Block_Outline, Make_Position, new Quaternion());
        yield return new WaitForSeconds(regen_time[level]-0.5f);
        Destroy(outline.gameObject);
        GameObject newBlock = Instantiate(Block_Prefabs[ran], Make_Position, new Quaternion(0f, 0f, 0f, 1f));
        newBlock.GetComponent<Block>().label = Now_label++;
        newBlock.transform.SetParent(Blocks.transform, false);
        StartCoroutine(Make_Block());
    }
    // Update is called once per frame
    void Update()
    {
    }
    void Set_Init()
    {

        for(int j = 0; j < Set_composition[PlayerPrefs.GetInt("Goal")].a.Length; j++)
        {
            for(int i=0;i < Set_composition[PlayerPrefs.GetInt("Goal")].a[j]; i++)
            {
                Set.Add(j);
            }
        }

    }
    public void Gameover()
    {
        StopAllCoroutines();
        for(int i=0; i<Blocks.transform.childCount; i++)
        {
            Blocks.transform.GetChild(i).GetComponent<Rigidbody2D>().velocity = new Vector2();
        }
    }
    public void Gain_Score()
    {
        Score++;
        for (int i = 0; i < level_stair.Length; i++) {
            if (Score > level_stair[i])
                level = i;
        }

        Score_Text.text = Score.ToString();
        Level_Text.text = "Level : " + (level+1).ToString("#0");
    }
    public void Change_Goal()
    {
        PlayerPrefs.SetInt("Goal", goal_num.value);
    }

    public void StartRestart()
    {

        SceneManager.LoadScene("SampleScene");
    }
    
}
