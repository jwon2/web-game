﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shield : MonoBehaviour
{
    private void Awake()
    {

        transform.parent.gameObject.GetComponent<CircleCollider2D>().radius = 0.39f;
        transform.parent.gameObject.tag = "Shield";
        StartCoroutine(DestroyThis());
    }

    IEnumerator DestroyThis()
    {
        yield return new WaitForSeconds(5f);
        transform.parent.gameObject.GetComponent<CircleCollider2D>().radius = 0.34f;
        transform.parent.gameObject.tag = "Block";
        Destroy(gameObject);
    }
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
    }
}
