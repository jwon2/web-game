﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Line : MonoBehaviour
{/*
    List<Collider2D> collidedObjects = new List<Collider2D>();

    private void OnTriggerStay2D (Collider2D col)
    {
        if (!collidedObjects.Contains(col) && (col.tag == "Block" || col.tag == "BadBlock"))
        {
            collidedObjects.Add(col);
        }
        if (!collidedObjects.Contains(col) && col.tag == "Shield")
        {
            col.GetComponent<Block>().Destroy_Shield();
            collidedObjects.Add(col);
        }
    }*/

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Shield")
            collision.GetComponent<Block>().Destroy_Shield();
        collision.GetComponent<Rigidbody2D>().velocity = new Vector2();
        transform.parent.GetComponent<Rigidbody2D>().velocity = new Vector2();
        collision.transform.Translate((collision.transform.position - transform.parent.position).normalized*0.02f);
        transform.Translate((transform.parent.position - collision.transform.position).normalized * 0.02f);
        collision.GetComponent<Rigidbody2D>().AddForce((transform.parent.position - collision.transform.position).normalized * 100f);
        transform.parent.GetComponent<Rigidbody2D>().AddForce((collision.transform.position - transform.parent.position).normalized * 100f);
        Destroy(gameObject);
    }

    void FixedUpdate()
    {
       // collidedObjects.Clear(); //clear the list of all tracked objects.
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {/*
        if (collidedObjects.Count == 1)
        {
            Collider2D col = collidedObjects[0];
            collidedObjects.Clear();

            Debug.Log("Add_Force!"+col.tag +transform.parent.tag);
            col.GetComponent<Rigidbody2D>().velocity = new Vector2();
            transform.parent.GetComponent<Rigidbody2D>().velocity = new Vector2();
            Debug.Log("Collisioned go :" + ((transform.parent.position - col.transform.position).normalized * 100f));
            col.GetComponent<Rigidbody2D>().AddForce((transform.parent.position - col.transform.position).normalized * 100f);

            Debug.Log("Block go :" + ((col.transform.position - transform.parent.position).normalized * 100f));
            transform.parent.GetComponent<Rigidbody2D>().AddForce((col.transform.position - transform.parent.position).normalized * 100f);
            Destroy(gameObject);
        }*/
    }
}
