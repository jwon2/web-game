﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : MonoBehaviour
{
    public int index;
    public int label;

    public GameObject[] Block_Prefabs;
    public GameObject WhiteBlock;

    public GameObject Line_Prefab;

    private GameManager GM;

    private void Awake()
    {
        GetComponent<Rigidbody2D>().AddForce(new Vector2(Random.Range(-1f,1f), Random.Range(-1f, 1f)) * 30f);
        GM = GameObject.FindObjectOfType<GameManager>();
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
    }
    List<Collider2D> collidedObjects = new List<Collider2D>();
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Block") {
            Block B = collision.gameObject.GetComponent<Block>();
            if (label < B.label)
            {
                if (B.index + index < GM.goal_num.value+3)
                {
                    GameObject newBlock = Instantiate(Block_Prefabs[B.index + index - 1], (transform.position + collision.transform.position) / 2, new Quaternion());
                    newBlock.transform.SetParent(GM.Blocks.transform,false);
                    newBlock.GetComponent<Block>().label = GM.Now_label++;
                    newBlock.GetComponent<Block>().Destroy_Shield();
                    newBlock.GetComponent<Rigidbody2D>().velocity = (collision.gameObject.GetComponent<Rigidbody2D>().velocity + GetComponent<Rigidbody2D>().velocity) / 2f;
                    Destroy(collision.gameObject);
                    Destroy(gameObject);
                }
                else if(B.index + index == GM.goal_num.value+3)
                {
                    GM.Gain_Score();
                    Destroy(collision.gameObject);
                    Destroy(gameObject);
                }
                else
                {
                    GameObject newBlock= Instantiate(WhiteBlock, (transform.position + collision.transform.position) / 2, new Quaternion());
                    newBlock.tag = "BadBlock";
                    newBlock.transform.SetParent(GM.Blocks.transform, false);
                    newBlock.GetComponent<Rigidbody2D>().velocity = (collision.gameObject.GetComponent<Rigidbody2D>().velocity + GetComponent<Rigidbody2D>().velocity) / 2f;
                    newBlock.GetComponent<InterruptBlock>().label = GM.Now_label++;
                    Destroy(collision.gameObject);
                    Destroy(gameObject);
                }
            }
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Wall_v")
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(-GetComponent<Rigidbody2D>().velocity.x, GetComponent<Rigidbody2D>().velocity.y);
        }
        if (collision.gameObject.tag == "Wall_h")
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x, -GetComponent<Rigidbody2D>().velocity.y);
        }
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Wall_v" || collision.gameObject.tag == "Wall_h")
        {

            GetComponent<Rigidbody2D>().AddForce(-transform.position);
        }
    }

    GameObject line;

    private void OnMouseDown()
    {
        Destroy_Shield();
        line = Instantiate(Line_Prefab, transform.position, new Quaternion());
        line.transform.rotation = Quaternion.FromToRotation(new Vector3(1f, 0f, 0f), new Vector3( Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y,0f) - transform.position);
        line.transform.localScale = new Vector3(Vector3.Distance(new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y, 0f), transform.position), 1f, 1f);
        line.transform.SetParent(transform, false);
        
    }

    private void OnMouseDrag()
    {
        if (line != null)
        {
            line.transform.position = transform.position;
            line.transform.rotation = Quaternion.FromToRotation(new Vector3(1f, 0f, 0f), new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y, 0f) - transform.position);
            line.transform.localScale = new Vector3(Vector3.Distance( new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y, 0f), transform.position), 1f, 1f);
        }
    }
    private void OnMouseUp()
    {
        if(line!=null)
            Destroy(line.gameObject);

    }

    public void Destroy_Shield()
    {

        tag = "Block";
        GetComponent<CircleCollider2D>().radius = 0.39f;
        if (transform.childCount != 0)
            Destroy(transform.GetChild(0).gameObject);
    }
}
