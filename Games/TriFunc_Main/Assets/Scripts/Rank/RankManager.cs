﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System;
using UnityEngine.UI;

public class RankManager : MonoBehaviour
{

    public const string gameName = "TriFunc";

    public GameObject EC;

    public GameObject GameOverRankBox;
    public GameObject MyRankData;
    public GameObject RankWindow;
    public GameObject RankDataWindow;
    public GameObject[] RankBoxTop5 = new GameObject[5];
    public GameObject[] RankActiveImage = new GameObject[3];
    public GameObject MyRankBoxWithTop5;
    public GameObject WaitPlz;
    public GameObject MyWaitPlz;

    private const int INFINITY_PRAC = 1;
    private const int INFINITY_REAL = 2;
    private const int HELL = 3;

    private Vector3 RankDataDownPos, RankDataPos;

    // UserData 저장용 구조체
    struct UserData {
        public string host;
        public string userid;
        public string nickname;
        public string token;
        public string closeUrl;
        public UserData(string host, string userid, string nickname, string token, string closeUrl) {
            this.host = host;
            this.userid = userid;
            this.nickname = nickname;
            this.token = token;
            this.closeUrl = closeUrl;
        } 
    }

    public void SetUserData(string data)
    {
        UserJsonData = data;
        // Debug.Log("Set: " + UserJsonData);

        user = JsonUtility.FromJson<UserData>(UserJsonData);
    }

    // UserData 받아올 JSON과 구조체
    public string UserJsonData;
    UserData user = new UserData();

    void Awake() {
        RankDataPos = RankDataWindow.transform.position;
        RankDataDownPos = new Vector3(RankDataPos.x, RankDataPos.y - 500f, 0);
    }

    // 시작하면서 UserData 받아오고 저장
    void Start() {
        LoadData();
    }

    void LoadData()
    {
        Application.ExternalCall("SetUserData");
        // Debug.Log("Get: " + UserJsonData);

        // JSON Parsing
        user = JsonUtility.FromJson<UserData>(UserJsonData);
    }

    public void GameClose()
    {
        Application.OpenURL(user.closeUrl);
    }

    [System.Serializable]
    struct Badges
    {
        public Badge winner;
    }

    [System.Serializable]
    struct Badge
    {
        public int level;
    }

    [System.Serializable]
    struct User
    {
        public string userId;
        public string nickname;
        public Badges badges;
    }

    [System.Serializable]
    struct Ranking
    {
        public RankData my;
        public List<RankData> ranking;
    }

    // RankData 저장할 구조체
    [System.Serializable]
    struct RankData
    {
        public User user;
        public int rank;        // 등수
        public int score;       // 점수
        public float time;     // 시간
        public string nickname; // 닉네임
        public int level;       // 레벨 (깨봉홈페이지 레벨)
    }

    // 상위 5등과 자신의 RankData 저장할 구조체
    RankData[] Top5 = new RankData[5];
    RankData MyRank;

    // DB에 정보 전송, 점수-시간-userid 를 보낸다
    public void PutAndGetRankInfo(int score, float time) {
        if (string.IsNullOrEmpty(user.token)) {
            LoadData();
            //not authorized
            return;
        }

        StartCoroutine(PutAndGetRanking(user.token, score, time));
    }

    private IEnumerator PutAndGetRanking(string token, int score, float time) {
        string mode = "";
        switch (PlayerPrefs.GetInt("Mode")) {
            case INFINITY_REAL:
                mode = "InfReal";
                break;
            case HELL:
                mode = "Hell";
                break;
        }

        string url = user.host + "/user/v1/games/" + gameName + mode + "/users/" + user.userid;
        string data = "{\"score\":" + score + ",\"time\":" + time + "}";

        using (UnityWebRequest w = UnityWebRequest.Put(url, data))
        {
            w.SetRequestHeader("Authorization", "Bearer " + token);
            w.SetRequestHeader("Content-Type", "application/json");
            // Debug.Log(url + "\n\n" + data);
            yield return w.SendWebRequest();

            if (w.isHttpError || w.isNetworkError)
            {
                //TODO handle error
            }
            else
            {
                //sucess
                MyRank = JsonUtility.FromJson<RankData>(w.downloadHandler.text);
                
                //success
                RankData r = JsonUtility.FromJson<RankData>(w.downloadHandler.text);

                MyRank.nickname = r.user.nickname;
                MyRank.level = r.user.badges.winner.level;

                MyWaitPlz.SetActive(false);
                MyRankData.SetActive(true);

                if (MyRank.score <= score) EC.transform.GetComponent<EventController>().GameOverRankBox.GetComponent<RankBox>().SetRankBox(MyRank.rank, score, time, MyRank.nickname, MyRank.level, false);
                else EC.transform.GetComponent<EventController>().GameOverRankBox.GetComponent<RankBox>().SetRankBox(0, score, time, MyRank.nickname, MyRank.level, true);
            }
        }
    }

    // DB에서 Top5와 자신의 정보 받아온다.
    // token은 (string) user.token에 저장되어 있다.
    // 받아오는 데이터는 각각의 등수, 점수, 시간, 닉네임, 레벨
    public void GetRankInfo(int difficulty) {
        if (difficulty == -1)
            difficulty = PlayerPrefs.GetInt("Mode");

        SetRankActiveImage(difficulty);
        RankDataWindow.SetActive(false);
        WaitPlz.SetActive(true);
        
        string mode = "";
        switch (difficulty) {
            case INFINITY_REAL:
                mode = "InfReal";
                break;
            case HELL:
                mode = "Hell";
                break;
        }

        if (string.IsNullOrEmpty(user.token)) {
            // Debug.Log("token is empty.");
            LoadData();
            //not authorized
            return;
        }
        StartCoroutine(GetRanking(user.token, mode));
    }

    private void SetRankActiveImage(int mode) {
        for (int i = 0; i < 3; i++)
            RankActiveImage[i].SetActive(false);
        RankActiveImage[mode - 1].SetActive(true);
    }

    private IEnumerator GetRanking(string token, string mode) {
        string url = user.host + "/user/v1/games/" + gameName + mode;

        using (UnityWebRequest w = UnityWebRequest.Get(url)) {
            w.SetRequestHeader("Authorization", "Bearer " + token);
            yield return w.SendWebRequest();

            if (w.isHttpError || w.isNetworkError) {
                //TODO handle error
            }
            else {
                // Debug.Log(w.downloadHandler.text);
                //success
                Ranking r = JsonUtility.FromJson<Ranking>(w.downloadHandler.text);

                MyRank = r.my;
                MyRank.nickname = r.my.user.nickname;
                MyRank.level = r.my.user.badges.winner.level;

                WaitPlz.SetActive(false);
                RankDataWindow.SetActive(true);

                int size = Math.Min(r.ranking.Count, 5);
                int i = 0;
                for (i = 0; i < size; i++) {
                    Top5[i] = r.ranking[i];
                    Top5[i].nickname = r.ranking[i].user.nickname;
                    Top5[i].level = r.ranking[i].user.badges.winner.level;
                }
                if (i < 5) {
                    for (int j = i; j < 5; j++) {
                        //TODO don't show empty data
                        Top5[j] = new RankData();
                    }
                }

                for (i = 0; i < 5; i++)
                    RankBoxTop5[i].GetComponent<RankBox>().SetRankBox(Top5[i].rank, Top5[i].score, Top5[i].time, Top5[i].nickname, Top5[i].level, false);

                if (0 <= MyRank.rank && MyRank.rank <= 5)
                    MyRankBoxWithTop5.SetActive(false);
                else {
                    MyRankBoxWithTop5.GetComponent<RankBox>().SetRankBox(MyRank.rank, MyRank.score, MyRank.time, MyRank.nickname, MyRank.level, false);
                    MyRankBoxWithTop5.SetActive(true);
                }
            }
        }
    }
}