﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonController_Title : MonoBehaviour {
    public GameObject[] HelpContents = new GameObject[10];
    public GameObject LeftButton;
    public GameObject RightButton;
    public GameObject SoundOnButton;
    public GameObject SoundOffButton;
    private int Page;

    public void Awake() {
        PlayerPrefs.SetInt("isMonsterTypeOn", 1);
        if (!PlayerPrefs.HasKey("isSoundOn"))
            SoundOn();

        if (PlayerPrefs.GetFloat("isSoundOn") == 0f) {
            SoundOnButton.SetActive(true);
            SoundOffButton.SetActive(false);
        }
        else {
            SoundOnButton.SetActive(false);
            SoundOffButton.SetActive(true);
        }
    }

    public void StoryModeStart() {
        PlayerPrefs.SetInt("Mode", 0);
        SceneManager.LoadScene("Play");
    }
    
    public void InfinityPracModeStart() {
        PlayerPrefs.SetInt("Mode", 1);
        SceneManager.LoadScene("Play");
    }

    public void InfinityRealModeStart() {
        PlayerPrefs.SetInt("Mode", 2);
        SceneManager.LoadScene("Play");
    }

    public void HellModeStart() {
        PlayerPrefs.SetInt("Mode", 3);
        SceneManager.LoadScene("Play");
    }

    public void SoundOn() {
        PlayerPrefs.SetFloat("isSoundOn", 1f);
        AudioListener.volume = 1f;
    }

    public void SoundOff() {
        PlayerPrefs.SetFloat("isSoundOn", 0f);
        AudioListener.volume = 0f;
    }

    public void SetFirstPage() {
        Page = 0;
        HelpContents[Page].SetActive(true);
        for (int i = 1; i < 10; i++) HelpContents[i].SetActive(false);
        LeftButton.SetActive(false);
        RightButton.SetActive(true);
    }

    public void RightPage() {
        if (Page == 0) LeftButton.SetActive(true);
        HelpContents[Page].SetActive(false);
        Page++;
        HelpContents[Page].SetActive(true);
        if (Page == 9) RightButton.SetActive(false);
    }

    public void LeftPage() {
        if (Page == 9) RightButton.SetActive(true);
        HelpContents[Page].SetActive(false);
        Page--;
        HelpContents[Page].SetActive(true);
        if (Page == 0) LeftButton.SetActive(false);
    }
}
