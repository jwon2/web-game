﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonController_Play : MonoBehaviour {
    
    private EventController ec;
    public GameObject GameOverRankBox;
    public GameObject GameCloseButton;
    public GameObject Triangle;
    public GameObject CollisionCircle;
    public GameObject Character;
    public GameObject SoundOnButton;
    public GameObject SoundOffButton;

    void Awake() {
        ec = transform.GetComponent<EventController>();
        AudioListener.volume = PlayerPrefs.GetFloat("isSoundOn");

        if (PlayerPrefs.GetFloat("isSoundOn") == 0f)
            SoundOnButton.SetActive(true);
        else
            SoundOffButton.SetActive(true);
    }

    public void MonsterTypeOn() {
        PlayerPrefs.SetInt("isMonsterTypeOn", 1);
        ec.isMonsterInfoOn = true;
    }

    public void MonsterTypeOff() {
        PlayerPrefs.SetInt("isMonsterTypeOn", 0);
        ec.isMonsterInfoOn = false;
    }

    public void Totitle() {
        SceneManager.LoadScene("Title");
    }

    public void Restart() {
        SceneManager.LoadScene("Play");
    }

    public void InfinityModeStart() {
        PlayerPrefs.SetInt("Mode", 1);
        SceneManager.LoadScene("Play");
    }

    public void SoundOn() {
        PlayerPrefs.SetFloat("isSoundOn", 1f);
        AudioListener.volume = 1f;
    }

    public void SoundOff() {
        PlayerPrefs.SetFloat("isSoundOn", 0f);
        AudioListener.volume = 0f;
    }
}
