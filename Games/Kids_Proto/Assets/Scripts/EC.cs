﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EC : MonoBehaviour {
    public Image clock;
    public GameObject clear, gameover;
    public GameObject bird;
    public GameObject quedaPar;
    public GameObject amqueda;
    public GameObject goqueda;
    public GameObject clqueda;
    public GameObject balloonsPar;
    public GameObject[] tmpBalloons = new GameObject[2];
    public GameObject[] ans = new GameObject[3];
    private GameObject[,] balloons = new GameObject[3, 6];
    private int n, ansIdx;
    private float time;
    private float finishTime;
    private bool isFinishing;
    private bool isProcessing;

    void Start() {
        isProcessing = true;
        isFinishing = false;
        time = 0;
        finishTime = 5f;
        for (int i = 0; i < 3; i++)
            for (int j = 0; j < 6; j++)
                balloons[i, j] = ans[i].transform.GetChild(j).gameObject;

        StartCoroutine(delayStart());
    }

    public void tt() {
        isProcessing = false;
        StartCoroutine(Timer());
    }

    private IEnumerator Timer() {
        time += 0.05f;
        yield return new WaitForSeconds(0.05f);
        clock.fillAmount = time / finishTime;
        if (time < finishTime) StartCoroutine(Timer());
        else {
            gameover.SetActive(true);
            StartCoroutine(DOWN());
        }
    }

    private IEnumerator delayStart() {
        yield return new WaitForSeconds(0.5f);
        ShowProblem();
    }

    private void ShowProblem() {
        n = Random.Range(0, 6);
        if (n == 0) ansIdx = 0;
        else if (n == 1) ansIdx = Random.Range(0, 2);
        else if (n == 5) ansIdx = 2;
        else if (n == 4) ansIdx = Random.Range(1, 3);
        else ansIdx = Random.Range(0, 3);
        for (int i = 0; i < 3; i++) SetAB(i, 1 + n + i - ansIdx);
        bird.GetComponent<Bird>().Fly(n);
    }

    private void SetAB(int bn, int n) {
        for (int i = n; i < 6; i++) ans[bn].transform.GetChild(i).gameObject.SetActive(false);
        for (int i = 0; i < n; i++) ans[bn].transform.GetChild(i).gameObject.SetActive(true);
    }

    public void GetAns(int n) {
        if(!isProcessing) {
            bird.SetActive(false);
            if (ansIdx == n)
                Clear();
            else GameOver(n);
        }
    }

    private void Clear() {
        if (!isFinishing) {
            isFinishing = true;
            amqueda.SetActive(false);
            clqueda.SetActive(true);
            clear.SetActive(true);
            bird.GetComponent<Bird>().OnBalloons();
            StartCoroutine(Restart(1.3f));
        }
    }

    private IEnumerator Restart(float time) {
        yield return new WaitForSeconds(time);
        SceneManager.LoadScene("SampleScene");
    }

    private void GameOver(int tn) {
        if (!isFinishing) {
            isFinishing = true;
            amqueda.SetActive(false);
            goqueda.SetActive(true);
            amqueda.SetActive(true);
            if (ansIdx < tn) {
                bird.GetComponent<Bird>().OnBalloons();
                for (int i = 0; i < tn - ansIdx; i++) tmpBalloons[i].SetActive(true);
                StartCoroutine(UP());
            }
            else {
                for (int i = n; i >= ansIdx - tn; i--) {
                    balloonsPar.transform.GetChild(2 * i).gameObject.SetActive(true);
                    balloonsPar.transform.GetChild(2 * i + 1).gameObject.SetActive(false);
                }
                StartCoroutine(DOWN());
            }
        }
    }

    private IEnumerator UP() {
        for (int i = 0; i < 70; i++) {
            quedaPar.transform.Translate(new Vector3(0f, 0.05f, 0f));
            yield return new WaitForSeconds(1f/60f);
        }
        gameover.SetActive(true);
        StartCoroutine(Restart(1f));
    }

    private IEnumerator DOWN() {
        for (int i = 0; i < 70; i++) {
            quedaPar.transform.Translate(new Vector3(0f, -0.05f, 0f));
            yield return new WaitForSeconds(1f / 60f);
        }
        gameover.SetActive(true);
        StartCoroutine(Restart(1f));
    }
}
