﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bird : MonoBehaviour {
    public GameObject queda;
    public GameObject amqueda;
    public GameObject balloonPar;
    public GameObject EC;
    private GameObject[] balloons, blanks;

    private static readonly float[] bx = { -4.47f + 0.75f, -2.95f + 0.75f, -1.47f + 0.75f, -4.33f + 0.75f, -2.94f + 0.75f, -1.7f + 0.75f };
    private static readonly float[] by = { 3.81f + 0.75f, 3.91f + 0.75f, 3.91f + 0.75f, 2.15f + 0.75f, 2.31f + 0.75f, 2.28f + 0.75f };

    private Vector3 startPosition;
    static private Vector3 tmpSp = new Vector3(-0.9f + 0.75f, 4.72f + 0.75f);
    static private float flyVelocity = 0.1f;
    static private int howManyFrame = 50;
    static private int howManyFrame2 = 20;
    private bool isFly;
    private float x0, y0, x1, y1;
    private int Fstate;
    private int state;
    private int cnt;

    void Awake() {
        balloons = new GameObject[balloonPar.transform.childCount / 2];
        blanks = new GameObject[balloonPar.transform.childCount / 2];
        for (int i = 0; i < balloonPar.transform.childCount / 2; i++) {
            balloons[i] = balloonPar.transform.GetChild(i * 2).gameObject;
            blanks[i] = balloonPar.transform.GetChild(i * 2 + 1).gameObject;
        }
        
        startPosition = transform.position;
        isFly = false;
        cnt = 0;
    }

    public void OnBalloons() {
        for (int i = 0; i < 6; i++) {
            balloons[i].SetActive(true);
            blanks[i].SetActive(false);
        }
    }

    public void Fly(int num) {
        Fstate = num + 6;
        state = 0;
        cnt = 0;
        isFly = true;
    }

    void Update() {
        if (isFly) {
            if (state == 100) {
                x0 = bx[Fstate - 6];
                y0 = by[Fstate - 6];
                x1 = -10f;
                y1 = 8f;
            }
            else if (state == 0) {
                x0 = startPosition.x;
                y0 = startPosition.y;
                x1 = bx[0];
                y1 = by[0];
            } else if (state < 6) {
                x0 = tmpSp.x;
                y0 = tmpSp.y;
                x1 = bx[state];
                y1 = by[state];
            } else if (6 <= state && state < 12){
                x0 = bx[state - 6];
                y0 = by[state - 6];
                x1 = tmpSp.x;
                y1 = tmpSp.y;
            } else {
                x0 = startPosition.x;
                y0 = startPosition.y;
                x1 = startPosition.x;
                y1 = startPosition.y;
            }
            float x;
            if (state == 0 || state == 100) x = x0 + (float)cnt / (float)howManyFrame * (x1 - x0);
            else x = x0 + (float)cnt / (float)howManyFrame2 * (x1 - x0);
            transform.position = new Vector3(x, func(x), 0f);
            cnt++;
            if (state == 0 && cnt == howManyFrame) {
                if(Fstate == 6) {
                    queda.SetActive(false);
                    amqueda.SetActive(true);
                    cnt = 0;
                    balloons[0].SetActive(false);
                    blanks[0].SetActive(true);
                    state = 100;
                } else {
                    queda.SetActive(false);
                    amqueda.SetActive(true);
                    state = 6;
                    cnt = 0;
                    balloons[0].SetActive(false);
                    blanks[0].SetActive(true);
                }
            }
            if (state == 100 && cnt == howManyFrame) {
                cnt = 0;
                Fstate = -1;
                isFly = false;
                EC.GetComponent<EC>().tt();
                transform.position = startPosition;
            }
            if (cnt == howManyFrame2 && state != 0 && state != 100) {
                cnt = 0;
                if (state == Fstate - 6) {
                    balloons[state].SetActive(false);
                    blanks[state].SetActive(true);
                    state = 100;
                } else if (state < 6) {
                    balloons[state].SetActive(false);
                    blanks[state].SetActive(true);
                    state += 6;
                } else if (state != 100) state -= 5;
            }
        }
    }

    private float func(float x) {
        return (y0 - y1) * Mathf.Pow((x - x1) / (x0 - x1), 2) + y1;
    }
}
